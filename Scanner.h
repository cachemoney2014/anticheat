//========================================================================================
#pragma once
//========================================================================================
#include "NT.h"
#include "macros.h"
///========================================================================================
extern DWORD g_dwGlobalStatus;
extern DWORD g_dwStoredNtStatus;
extern DWORD g_dwEIP;
///========================================================================================
extern NTSTATUS g_NtStatus;
///========================================================================================
#define CODE_FOUND				( 1 << 0 )
#define STRINGS_FOUND			( 1 << 1 )
#define IMPORTS_FOUND			( 1 << 2 )
#define HANDLE_TO_GAME_PROCESS  ( 1 << 3 )
#define UNLINKED_MODULE			( 1 << 4 )
#define HIDDEN_PROCESS			( 1 << 5 )
#define VTABLE_MISMATCH			( 1 << 6 )
//========================================================================================
#define PROCESS_ENTRY_32BIT		( 1 << 0 )
#define PROCESS_ENTRY_64BIT		( 1 << 1 )
#define PROCESS_ENTRY_INVALID	( 1 << 2 )
#define PROCESS_ENTRY_UNKNOWN	( 1 << 3 )
//========================================================================================
#define STATUS_DONE 0
#define STATUS_WAITING 1
//========================================================================================
//#define CREATE_INTERFACE_HASH 0x583C88C2
//========================================================================================
#define SECTION_NAME_HASH_TEXT 0x3C2FBC6B // hash of ASCII string .text
#define SECTION_NAME_HASH_RDATA 0x9C05F2C7 // hash of ASCII string .rdata
#define SECTION_NAME_HASH_DATA 0x72EE80DE // hash of ASCII string .data
#define SECTION_NAME_HASH_RELOC 0x6846AF33 // hash of ASCII string .reloc
#define SECTION_NAME_HASH_PDATA 0x8D2C9A88 // hash of ASCII string .pdata
#define SECTION_NAME_HASH_RSRC 0x81473A03 // hash of ASCII string .rsrc
//========================================================================================
typedef struct
{
	DWORD m_dwSectionNameHash;
	DWORD m_dwCharacteristics;

	DWORD m_dwVirtualAddress;
	DWORD m_dwSectionAlignment;
	DWORD m_dwSize;
}Section_Information_t;
//========================================================================================
typedef struct
{
	BOOL m_bIsDEPCompatible;

	DWORD m_dwModuleNameCRC;

	DWORD m_dwTimeDateStamp;
	DWORD m_dwCRCOfFilePEHeader;
	DWORD m_dwCRCOfPEHeader;
	
	DWORD m_dwImageBase;
	DWORD m_dwModuleBase;
	DWORD m_dwTotalSize;
	DWORD m_dwVirtualSize;

	DWORD m_dwFileAlignment;
	DWORD m_dwSizeOfHeaders;

	DWORD m_dwDataBegin;
	DWORD m_dwDataEnd;
	DWORD m_dwRDataBegin;
	DWORD m_dwRDataEnd;
	DWORD m_dwTextBegin;
	DWORD m_dwTextEnd;

	HANDLE m_hFile; // this is only set access denied was encountered

	Section_Information_t Section_Information [ 128 ];

	DWORD m_dwModuleNameHash [ 4 ];
	
	wchar_t m_wszModuleName [ MAX_PATH ];

	BYTE m_bName [ 96 ];

	WORD m_wNumberOfSections;
}Module_t;
//========================================================================================
extern Module_t Module;
//========================================================================================
int __stdcall GetModuleInformation ( HANDLE hProcessToScan, DWORD dwProcessID, DWORD dwModuleBase, DWORD dwModuleSize, Module_t& Module );
//===================================================
HMODULE __stdcall GetModuleHandleEx ( HANDLE hCurrentProcess, HANDLE hProcessToScan, DWORD dwModuleNameHash );
//===================================================s=====================================
extern DWORD Initialize [ 8192 ];
//========================================================================================
DLL_EXPORT DWORD __stdcall RunScan ( PDWORD pdwBufferIn, PDWORD pdwBufferOut );
//========================================================================================
void __stdcall ProcessBufferMD5 ( MD5Context_t& MD5Context, const PBYTE pbBuffer, UINT _Size );
//========================================================================================
void __stdcall SetStatus ( DWORD dwStatus, NTSTATUS status );
//========================================================================================
DWORD __stdcall GetStatus ( void );
//========================================================================================
void __stdcall RemoveStatus ( DWORD dwStatus );
//========================================================================================
extern PVOID g_pvMemoryAllocatedBase;
extern PVOID g_pvFileAllocatedBase;
//========================================================================================
extern BOOLEAN CharIsPrintable [ 256 ];
//========================================================================================
extern DWORD g_dwSizeOfPage;
//========================================================================================
bool __stdcall AsciiToUnicode(const char * szAscii, wchar_t * szUnicode);
//========================================================================================