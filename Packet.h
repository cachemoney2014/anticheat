//========================================================================================
#pragma once
//========================================================================================
#include "NT.h"
#include "macros.h"
//========================================================================================
// Scan Type(s)
//========================================================================================
enum MainScan
{
	SCAN_ALL_USERMODE_PROCESSES_AND_THREADS = ( 1 << 16 ),
	SCAN_ALL_HANDLES = ( 1 << 17 ),
	LIST_ALL_USERMODE_PROCESSES = ( 1 << 18 )
};
//========================================================================================
enum ProcessScan
{
	SCAN_PROCESS_MODULES =				( 1 << 0 ),
	SCAN_SPECIFIC_MODULE =				( 1 << 1 ),
	SCAN_UNLISTED_PROCESS =				( 1 << 2 )
};
///========================================================================================
enum HandleScan
{
	SCAN_THREADS_BY_HANDLE =			( 1 << 0 ),
	SCAN_FILES =						( 1 << 1 ),
	SCAN_PROCESSES =					( 1 << 2 ),
	SCAN_FOR_HANDLE_TO_GAME =			( 1 << 3 )
};
///========================================================================================
#define SUB_SCAN_4 ( 1 << 3 ) // scan for threads by handle
#define SUB_SCAN_5 ( 1 << 4 ) // scan for handles to game process
#define SUB_SCAN_6 ( 1 << 5 ) // scan for any open file, with a handle, this includes script files
#define SUB_SCAN_7 ( 1 << 6 ) // scan for unlisted processes by handle
///========================================================================================
#define SUB_SCAN_8 ( 1 << 7 ) // scan for vtable modifications, very specific
///========================================================================================
/* http://securityxploded.com/hidden-process-detection.php */
///========================================================================================
#define SUB_SCAN_9 ( 1 << 8 )// scan for unlisted processes by NtOpenProcess bruteforce
#define SUB_SCAN_10 ( 1 << 9 ) // scan for unlisted processes by window enumeration
//========================================================================================me
// Server -> Client packet structure
//========================================================================================
typedef struct 
{
	DWORD m_dwCACVersion;
	DWORD m_dwChecksum;
	DWORD m_dwScanGate;							// we perform an and on the high bits to determine which scan to run, because one function supports
												// multiple types of scans
	DWORD m_dwScanGateDecryptKey [ 8 ];			// 256 bit AES key

	DWORD m_dwSize;								// total size of all the post pended packets

	PVOID pvStartOfScanPacketData;
}Scan_Packet_t;
//========================================================================================
typedef struct
{
	DWORD m_dwCRCHashes;						// this gets used to scan for certain sequences of known cheat code
	DWORD m_dwCRCMasks;
}Scan_Packet_CRC_t;
//========================================================================================
typedef struct
{
	PDWORD m_pdwVMCode;							// this gets filled by the DecryptScan
}Scan_Packet_VM_t;
//========================================================================================
typedef struct
{
	DWORD m_dwProcessID;
	DWORD m_dwProcessNameHash;

	DWORD m_dwModuleNameHash;
	DWORD m_dwModuleBase;
}Scan_Packet_Specific_Module_t;
//========================================================================================
typedef struct
{
	DWORD m_dwProcessID;
}Scan_Packet_Process_t;
//========================================================================================
// Client -> Server packet structures
//========================================================================================
typedef struct
{
	DWORD m_dwCACStatus;
	DWORD m_dwCRCOfPacket;
	DWORD m_dwOSVersion;
	DWORD m_dwPacketSize;
}ScanHeader_t;
//========================================================================================
typedef struct
{
	DWORD m_dwFlags;
	DWORD m_dwNumberOfProcessAndModuleEntries; // tells the number of Process_and_Module_ScanEntry_t to expect
	DWORD m_dwNumberOfManualMapEntries; // tells the number of M_M_M_ScanEntry_t to expect
	DWORD m_dwNumberOfThreadEntries; // tells us the number of Thread_Scan_t to expect
}Mega_Scan_Header_t;
//========================================================================================
typedef struct 
{
	DWORD m_dwIATCRC;
	DWORD m_dwMD5HashOfSection [ 4 ];
}Validation_Response_Packet_t;
//========================================================================================
typedef struct
{
	DWORD m_dwTimeDateStamp;
	DWORD m_dwCRCOfFilePEHeader;
	DWORD m_dwCRCOfPEHeader;
	DWORD m_dwCRCOfMemoryPEHeader;
	
	DWORD m_dwImageBase;
	DWORD m_dwModuleBase;
	DWORD m_dwProcessID;
	DWORD m_dwTotalSize;
	DWORD m_dwVirtualSize;

	DWORD m_dwFlags;

	DWORD m_dwMemoryMD5 [ 4 ];
	DWORD m_dwFileMD5 [ 4 ];

	BYTE m_bName [ 96 ];
}Generic_Scan_Information_t;
//========================================================================================
typedef struct // used for Game_Scan_Response, byte patches
{
	DWORD m_dwRVA;
	DWORD m_dwNumberOfBytesPatched;
	DWORD m_dwRoundedLength;
	PBYTE m_pbModifiedOpcodes;
}B_P_ScanEntry_t;
//========================================================================================
typedef struct 
{
	DWORD m_dwRVA; // rva of the hooked import
	DWORD m_dwImportModule; // imagebase of the module this imported from

	BYTE m_bName [ 96 ]; // name of the API that was hooked
	WCHAR m_wszModuleName [ 164 ]; // name of the module it was imported from
	
	DWORD m_dwResolved; // address we resolved
	DWORD m_dwHookedAddress; // address that the import was redirected to
	DWORD m_dwCRCOfHook; // crc of code that hooked it
}IAT_ScanEntry_t;
//========================================================================================
typedef struct // this response is only sent if our breakpoints are disabled, may not be used in some cases
{
	DWORD m_dwRVA;
	DWORD m_dwBreakPointType;
}H_BP_ScanEntry_t;
//========================================================================================
typedef struct
{
	DWORD m_dwProcessID;
	DWORD m_dwThreadID;

	DWORD m_dwEntryPoint;
//	DWORD m_dwEntryPointCRC;
	DWORD m_dwMD5 [ 4 ];
	DWORD m_dwProtection;

	DWORD m_dwBytesProcessed;
	DWORD m_dwEntryPointLength;

	DWORD m_dwThreadFunctionLengthRounded;
	
	DWORD m_dwDR0;
	DWORD m_dwDR1;
	DWORD m_dwDR2;
	DWORD m_dwDR3;
	DWORD m_dwDR6;
	DWORD m_dwDR7;

	PBYTE m_pbOpcodes;

}Thread_Scan_t;
//========================================================================================
typedef struct // this response is not encrypted, but still gets md5ed, used for anything from bad decryption response, iat corruption, or other things
{
	DWORD m_dwResponse;
}Failed_Response_t;
//========================================================================================
typedef struct Process_and_Module_ScanEntry_s
{
	Generic_Scan_Information_t Generic_Scan_Information;
}Process_and_Module_ScanEntry_t;
//========================================================================================
typedef struct
{
//	DWORD m_dwFunctionHash;
	DWORD m_dwMD5 [ 4 ];
	DWORD m_dwFunctionStart;
	DWORD m_dwFunctionLength;
	DWORD m_dwFunctionLengthRounded;

	PBYTE m_pbOpcodes;
}Function_Scan_Data_t;
//========================================================================================
typedef struct
{
	BOOL m_bIsWide;

	DWORD m_dwStringHash;
	DWORD m_dwBackupStringHash;
	DWORD m_dwStringStart;
	DWORD m_dwStringLength;
	DWORD m_dwStringLengthRounded;

	PBYTE m_pbBuffer;
}String_Scan_Data_t;
//========================================================================================
typedef struct
{
	DWORD m_dwProcessID;

	DWORD m_dwMemoryRegionBase;
	DWORD m_dwMemoryRegionProtection;
	DWORD m_dwMemoryRegionSize;

	DWORD m_dwFlags;		// this alerts the server to what type of look up it should perform, strictly function hashes, starts and lengths, 
							// or string hashes or all of them
	DWORD m_dwFunctionsFound;
	DWORD m_dwBytesProcessed;
	DWORD m_dwStringsFound;
}M_M_M_ScanEntry_t;
//========================================================================================
typedef struct
{	
	DWORD m_dwRVA;
	DWORD m_dwLength;
	BYTE m_bOpcodes [ 32 ];	// the opcodes found by the crc find pattern copied into buffer
}CRC_FindPattern_ScanEntry_t;
//========================================================================================
typedef struct
{
	BYTE m_bDirectoryListing [ 512 ];	// obvious...
}Directory_Listing_ScanEntry_t;
//========================================================================================
typedef struct	// this struct is used to report back registry entries or prefetch entries
{
	BYTE m_bEntryName [ 64 ]; // report back the names to be sure we didn't get a collision
	DWORD m_dwEntryNameMD5Hash [ 4 ];
}Registry_ScanEntry_t;
//========================================================================================
typedef struct
{
	DWORD m_dwCRCOfCOde;
}VTable_CRC_t;
//========================================================================================
typedef struct 
{
	DWORD m_dwNumberOfHookedEntries; // this determines the numbver of VTable_CRC_t that get tacked on at the end

	DWORD m_dwVTableDataBase; // where the pointer to the vtable was stored in .data section
	DWORD m_dwPointerToVTable; // the actual vtable pointer
		
	int m_iVTableSize; // size of the VTable

//	VTable_CRC_t* pVTable_CRC;
}VTable_ScanEntry_t;
//========================================================================================
typedef struct
{
	DWORD m_dwNumberOfThreadEntries; // tells us the number of Thread_Scan_t to expect
	DWORD m_dwNumberOfBytePatchEntries; // tells us how many B_P_ScanEntry_t to expect
	DWORD m_dwNumberOfHookedImports; // tells us how many IAT_ScanEntry_t to expect
	DWORD m_dwNumberOfHookedVTables; // tells us how many VTable_ScanEntry_t to expect
}Detail_Scan_t;
//========================================================================================
typedef struct
{
	Generic_Scan_Information_t Generic_Scan_Information;

	Detail_Scan_t Detail_Scan;

//	B_P_ScanEntry_t* pBP_Entry;
//	IAT_ScanEntry_t* pIAT_ScanEntry;
//	VTable_ScanEntry_t* VTable_ScanEntry;

}Specfic_Scan_Information_t;
//========================================================================================
// this packet is routinely pulsed before a scan so we have an up to data list
//========================================================================================
typedef struct
{
	char m_szName [ 64 ];
	DWORD m_dwProcessNameHash;
	DWORD m_dwProcessID;
	DWORD m_dwImageBase;	// optional
}Process_List_ReportBack_t; // report back process(es)
//========================================================================================
typedef struct
{
	BYTE m_bProcessName [ 96 ];
	DWORD m_dwFlags;
	DWORD m_dwExitCode;
	DWORD m_dwProcessID;
	NTSTATUS status;
}Process_Entry_t;
//========================================================================================
typedef struct
{
	DWORD m_dwVirtualAddress;
	DWORD m_dwFunction;
	DWORD m_dwCRC;
	DWORD m_dwLength;
}VTable_Entry_t;
//========================================================================================
typedef struct
{
	DWORD m_dwMemoryMD5 [ 4 ];
	DWORD m_dwFileMD5 [ 4 ];
}VTable_MD5_t;
//========================================================================================
typedef struct
{
	DWORD m_dwPacketMD5 [ 4 ];	// for integrity checks to make sure MITM didn't attack us
}ScanFooter_t;
//========================================================================================