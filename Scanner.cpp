//========================================================================================
#include "Scanner.h"
//========================================================================================
#include "FuncLen.h"
#include "hde32.h"
#include "IAT.h"
#include "Memory.h"
#include "Packet.h"
#include "RebaseFunction.h"
#include "StatusCodes.h"
#include "Strings.h"
//========================================================================================
#include "qsort.h"
//========================================================================================
extern "C" void * _ReturnAddress ( void );
#pragma intrinsic ( _ReturnAddress )
//========================================================================================
DWORD g_dwGlobalStatus = 0;
DWORD g_dwStoredNtStatus = 0;
DWORD g_dwEIP = 0;
//========================================================================================
DWORD g_dwSizeOfPage;
//========================================================================================
NTSTATUS g_NtStatus = 0;
//========================================================================================
MD5Context_t MD5Context;
//========================================================================================
Module_t Module;
//========================================================================================
PVOID g_pvMemoryAllocatedBase = NULL;
PVOID g_pvFileAllocatedBase = NULL;
//========================================================================================
DWORD __stdcall ScanProcessAndModules ( HANDLE hProcessToScan, PSYSTEM_PROCESS_INFORMATION PSPI, PDWORD pdwBufferOut );
DWORD __stdcall ScanProcessesManualMappedModules ( HANDLE hProcessToScan, PSYSTEM_PROCESS_INFORMATION PSPI, PDWORD pdwBufferOut );
//========================================================================================
bool __stdcall AsciiToUnicode(const char * szAscii, wchar_t * szUnicode)
{
	int len, i;
	if((szUnicode == NULL) || (szAscii == NULL))
		return false;
	len = strlen(szAscii);
	for(i=0;i<len+1;i++)
		*szUnicode++ = static_cast<wchar_t>(*szAscii++);
	return true;
}
//========================================================================================
bool __stdcall UnicodeToAscii(const wchar_t * szUnicode, char * szAscii )   
{   
	int len, i;   

	if ((szUnicode == NULL) || (szAscii == NULL))   
		return false;   
   
	len = wstrlen(szUnicode);   
    
	len = 127;
	
	for (i = 0; i<len + 1; i++ )   
		*szAscii++ = static_cast<char>(*szUnicode++ );
	

	return true;   
}   
//========================================================================================
BOOLEAN CharIsPrintable [ 256 ] =
{
    0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, /* 0 - 15 */ // TAB, LF and CR are printable
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 16 - 31 */
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, /* ' ' - '/' */
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, /* '0' - '9' */
    1, 1, 1, 1, 1, 1, 1, /* ':' - '@' */
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, /* 'A' - 'Z' */
    1, 1, 1, 1, 1, 1, /* '[' - '`' */
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, /* 'a' - 'z' */
    1, 1, 1, 1, 0, /* '{' - 127 */ // DEL is not printable
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 128 - 143 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 144 - 159 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 160 - 175 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 176 - 191 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 192 - 207 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 208 - 223 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 224 - 239 */
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* 240 - 255 */
};
//========================================================================================
BOOL __stdcall IsProcessDEPEnabled ( HANDLE hProcessToCheck )
{
	BOOL bReturn = 0, lpPolicy = 0, lpFlags = 0, lpPermanent = 0;

	NTSTATUS status;

	status = NtQueryInformationProcess ( hProcessToCheck, ProcessExecuteFlags, &lpPolicy, sizeof ( BOOL ), NULL );

	if ( NT_SUCCESS ( status ) )
	{
		if ( !( lpPolicy & 2 ) )
		{
			lpFlags = 1;

			if ( lpPolicy & 4 )
			{
				lpFlags = 3;
			}
		}
		else
		{
			lpFlags = 0;
		}

		lpPermanent = ( lpPolicy >> 3 );
		lpPermanent &= 1;
				
		if ( lpFlags != 0 )
		{
			bReturn = 1;
		}
	}
	else
	{
		RtlNtStatusToDosError ( status );
	}

	return bReturn;
}
//========================================================================================
void __stdcall ProcessBufferMD5 ( MD5Context_t& MD5Context, const PBYTE pbBuffer, UINT _Size )
{
//	MD5Final_t MD5Final = ( MD5Final_t )g_dwCoreDLLFunctions [ IMPORT_MD5Final ];
//	MD5Init_t MD5Init = ( MD5Init_t )g_dwCoreDLLFunctions [ IMPORT_MD5Init ];
//	MD5Update_t MD5Update = ( MD5Update_t )g_dwCoreDLLFunctions [ IMPORT_MD5Update ];

	memset ( &MD5Context, 0, sizeof ( MD5Context_t ) );

	MD5Init ( &MD5Context );
	MD5Update ( &MD5Context, pbBuffer, _Size );
	MD5Final ( &MD5Context );
}
//========================================================================================
int __stdcall GetModuleInformation ( HANDLE hProcessToScan, DWORD dwProcessID, DWORD dwModuleBase, DWORD dwModuleSize, Module_t& Module )
{
	BOOL bReturn;

	DWORD dwCRC, dwOffset, dwSize, dwSizeRequired;
	
	int i, iIterator, iPos, iStrLen;

	HANDLE hCurrentProcessHandle, hDuplicateHandle, hFile;

	NTSTATUS status;

	IO_STATUS_BLOCK   IoStatusBlock;

	PIMAGE_DOS_HEADER lpImageDosHeader;
	PIMAGE_NT_HEADERS lpImageNtHeaders;
	PIMAGE_SECTION_HEADER lpSectionHeader;

	PSYSTEM_HANDLE_INFORMATION PSHI;

	PBYTE pbFileBuffer;

	UNICODE_STRING wszPath;
	OBJECT_ATTRIBUTES ObjectAttributes;

	POBJECT_NAME_INFORMATION pObjNameInfo;

	SYSTEM_HANDLE_ENTRY handle;

	bReturn = 0;
	iStrLen = 0;
	dwCRC = 0;
	dwOffset = 0;
	dwSize = 0;

	PSHI = NULL;
			
	hCurrentProcessHandle = GetCurrentProcess();

	pbFileBuffer = ( PBYTE )NtAllocateMemory ( hCurrentProcessHandle, 0, g_dwSizeOfPage, PAGE_READWRITE, g_NtStatus ); // allocate only 1 page to read the file's header

	if ( pbFileBuffer == NULL )
	{
		SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		return 1;
	}

	DWORD dwNameCRC;
	
	wszPath.Buffer = Module.m_wszModuleName;
	wszPath.Length = wstrlen ( Module.m_wszModuleName );
	wszPath.MaximumLength = MAX_PATH;

	dwNameCRC = dwCRCBlock ( 0, wszPath.Buffer, wszPath.Length );
						
	InitializeObjectAttributes ( &ObjectAttributes, &wszPath, OBJ_CASE_INSENSITIVE, NULL, NULL );
		
	status = NtCreateFile ( &hFile, GENERIC_READ | SYNCHRONIZE | FILE_READ_ATTRIBUTES, &ObjectAttributes, &IoStatusBlock, NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ, FILE_OPEN, FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0 );
		
	if ( status == 0xC0000022 ) // access denied
	{					
		status = NtQuerySystemInformation ( SystemHandleInformation, NULL, 0, &dwSizeRequired );

		if ( NT_ERROR ( status ) )
		{			
			do
			{
				dwSizeRequired = ROUND_UP ( dwSizeRequired, g_dwSizeOfPage );

				dwSizeRequired += g_dwSizeOfPage;
				
				PSHI = ( PSYSTEM_HANDLE_INFORMATION )AllocateFromHeap ( dwSizeRequired  );

				if ( PSHI == NULL )
				{
					bReturn = 1;
				}
						
				status = NtQuerySystemInformation ( SystemHandleInformation, PSHI, dwSizeRequired, &dwSizeRequired );

				if ( NT_ERROR ( status ) )
				{
					DeallocateOffHeap ( PSHI );
				}

			}while ( !NT_SUCCESS ( status ) );

			if ( NT_SUCCESS ( status ) )
			{
				for ( iIterator = 0; iIterator < PSHI->HandleCount; iIterator++ )
				{
					handle = PSHI->Handles [ iIterator ];

					if ( handle.GrantedAccess & SYNCHRONIZE /* 0x0012019F*/ ) // ignore synchronous pipes
					{
						continue;
					}
					else
					{						
						status = NtDuplicateObject ( hProcessToScan, ( HANDLE )handle.HandleValue, hCurrentProcessHandle, &hDuplicateHandle, 0, 0, 0 );
						
						if ( NT_SUCCESS ( status ) )
						{				
							status = NtQueryObject ( hDuplicateHandle, ObjectNameInformation, NULL, 0, &dwSizeRequired );
									 
							if ( NT_ERROR ( status ) )
							{
								dwSizeRequired = g_dwSizeOfPage;

								pObjNameInfo = ( POBJECT_NAME_INFORMATION )AllocateFromHeap ( dwSizeRequired );

								if ( pObjNameInfo == NULL )
								{
									continue;
								}
									
								status = NtQueryObject ( hDuplicateHandle, ObjectNameInformation, pObjNameInfo, dwSizeRequired, &dwSizeRequired );

								if ( NT_SUCCESS ( status ) )
								{
									if ( pObjNameInfo->Name.Length != 0 )
									{
										for ( iPos = 0; iPos < pObjNameInfo->Name.Length; iPos++ )
										{
											if ( pObjNameInfo->Name.Buffer [ iPos ] >= 0x41 && pObjNameInfo->Name.Buffer [ iPos ] <= 0x5A ) // lowercase
												pObjNameInfo->Name.Buffer [ iPos ] |= 0x20;
										}

										dwCRC = dwCRCBlock ( 0, pObjNameInfo->Name.Buffer, pObjNameInfo->Name.Length );
																								
										if ( dwCRC == dwNameCRC ) // match found
										{
											status = NtReadFile ( hDuplicateHandle, NULL, NULL, NULL, &IoStatusBlock, ( LPVOID )pbFileBuffer, g_dwSizeOfPage, NULL, NULL );

											if ( !NT_SUCCESS ( status ) )
											{
												bReturn = 1;
											}

											Module.m_hFile = hDuplicateHandle;
										}
										else
										{
											NtClose ( hDuplicateHandle );
										}
									}
									else
									{
										NtClose ( hDuplicateHandle );
									}

									DeallocateOffHeap ( pObjNameInfo );

									if ( Module.m_hFile )
									{
										break;
									}
								}
								else
								{
									DeallocateOffHeap ( pObjNameInfo );
										
									continue;
								}
							}
						}
						else
						{
							continue;
						}
					}
				}
			}

			DeallocateOffHeap ( PSHI );
		}
	}
	else if ( NT_SUCCESS ( status ) )
	{
		status = NtReadFile ( hFile, NULL, NULL, NULL, &IoStatusBlock, ( LPVOID )pbFileBuffer, g_dwSizeOfPage, NULL, NULL );

		if ( !NT_SUCCESS ( status ) )
		{
			bReturn = 1;
		}
	}
	else
	{
		bReturn = 1;
	}
	
	Module.m_dwModuleBase = dwModuleBase;
	Module.m_dwTotalSize = dwModuleSize;

	Module.m_dwTotalSize = g_dwSizeOfPage; // start with pe header size
	
	lpImageDosHeader = ( PIMAGE_DOS_HEADER )pbFileBuffer;
			
	if ( lpImageDosHeader->e_magic == IMAGE_DOS_SIGNATURE && bReturn != 1 )
	{
		lpImageNtHeaders = ( PIMAGE_NT_HEADERS )( ( DWORD )pbFileBuffer + lpImageDosHeader->e_lfanew );

		if ( lpImageNtHeaders->Signature == IMAGE_NT_SIGNATURE )
		{
			lpSectionHeader = IMAGE_FIRST_SECTION ( lpImageNtHeaders );

			Module.m_dwVirtualSize = lpImageNtHeaders->OptionalHeader.SizeOfImage;

			Module.m_dwImageBase = lpImageNtHeaders->OptionalHeader.ImageBase;

			Module.m_dwFileAlignment = lpImageNtHeaders->OptionalHeader.FileAlignment;
			Module.m_dwSizeOfHeaders = lpImageNtHeaders->OptionalHeader.SizeOfHeaders;

			if ( lpImageNtHeaders->OptionalHeader.DllCharacteristics & IMAGE_DLLCHARACTERISTICS_NX_COMPAT )
			{
				Module.m_bIsDEPCompatible = 1;
			}
			else
			{
				Module.m_bIsDEPCompatible = 0;
			}

			for ( i = 0; i < lpImageNtHeaders->FileHeader.NumberOfSections; i++ )
			{						
				if ( Module.m_wNumberOfSections == 128 )
				{
					bReturn = 0;

					break;
				}

				iStrLen = strlen ( ( const char* )lpSectionHeader->Name );

				dwCRC = dwCRCBlock ( 0, ( PVOID )lpSectionHeader->Name, iStrLen );
					
				dwOffset = lpSectionHeader->VirtualAddress;
				dwSize = ROUND_UP ( lpSectionHeader->Misc.VirtualSize, lpImageNtHeaders->OptionalHeader.SectionAlignment );

				Module.Section_Information [ Module.m_wNumberOfSections ].m_dwCharacteristics = lpSectionHeader->Characteristics;
				Module.Section_Information [ Module.m_wNumberOfSections ].m_dwSectionNameHash = dwCRC;
				Module.Section_Information [ Module.m_wNumberOfSections ].m_dwSize = dwSize;
				Module.Section_Information [ Module.m_wNumberOfSections ].m_dwSectionAlignment = lpImageNtHeaders->OptionalHeader.SectionAlignment;
				Module.Section_Information [ Module.m_wNumberOfSections ].m_dwVirtualAddress = dwOffset;

				Module.m_dwTotalSize += dwSize;

				Module.m_wNumberOfSections++;
														
				lpSectionHeader++;
			}

//			dwSize = ROUND_UP ( lpImageNtHeaders->OptionalHeader.SectionAlignment, SBI.PhysicalPageSize );

			Module.m_dwCRCOfPEHeader = dwCRCBlock ( 0, pbFileBuffer, g_dwSizeOfPage );

			Module.m_dwTimeDateStamp = lpImageNtHeaders->FileHeader.TimeDateStamp;
			Module.m_dwCRCOfFilePEHeader = lpImageNtHeaders->OptionalHeader.CheckSum;

			NtClose ( hFile );
		}
		else
		{
			bReturn = 1;
		}
	}

	if ( !NtFreeMemory ( hCurrentProcessHandle, ( DWORD )pbFileBuffer, 0, MEM_RELEASE, g_NtStatus ) )
	{
		SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		bReturn = 1;
	}
		
	return bReturn; // Move along people there's nothing to see here
}
//========================================================================================
int __stdcall AcquireSectionNameAndMD5 ( HANDLE hProcessToScan, DWORD dwProcessID, MD5Context_t& MD5Context, DWORD dwBaseAddress, DWORD dwModuleSize, Module_t& Module, PMEMORY_SECTION_NAME PMSN )
{
	BOOL bReturn;

	DWORD dwCRC;

	int iCurLen, iIterator, iLen, iPos, iSavedPos;

	wchar_t* pwszBuffer;

	dwCRC = 0;

	iCurLen = 0;
	iIterator = 0;
	iLen = 0;
	iPos = 0;
	iSavedPos = 0;

	bReturn = 0;
	
	PBYTE pbBuffer = ( PBYTE )PMSN->SectionFileName.Buffer;

	dwCRC = dwCRCBlock ( 0, PMSN->SectionFileName.Buffer, PMSN->SectionFileName.Length );
					
	if ( Module.m_dwModuleNameCRC == dwCRC ) // prevent processing same region twice 
	{				
		return 2;
	}
	else
	{
		if ( PMSN->SectionFileName.Length != 0 )
		{
			memset ( &Module, 0, sizeof ( Module_t ) );

			for ( iIterator = 0; iIterator < PMSN->SectionFileName.Length; iIterator++ )
			{
				if ( pbBuffer [ iIterator ] == '\\' || pbBuffer [ iIterator ] == '/' ) // keep going until we hit the last '\\' or '/'
				{
					iPos = iIterator + 2;

					iSavedPos = iPos;
				}
			}
			if ( iPos != 0 )
			{					
				pwszBuffer = PMSN->SectionFileName.Buffer;
			
				for ( iPos = 0; iPos < PMSN->SectionFileName.Length; iPos++ )
				{
					if ( pbBuffer [ iPos ] >= 'A' && pbBuffer [ iPos ] <= 'Z' ) // lowercase
						pbBuffer [ iPos ] |= ' ';
				}

				ProcessBufferMD5 ( MD5Context, ( PBYTE )pwszBuffer, PMSN->SectionFileName.Length );
				
				Module.m_dwModuleNameCRC = dwCRC;

				memcpy ( Module.m_bName, &pbBuffer [ iSavedPos ], sizeof ( Module.m_bName ) );
				
				memcpy ( Module.m_wszModuleName, PMSN->SectionFileName.Buffer, sizeof ( Module.m_wszModuleName ) );

				memcpy ( Module.m_dwModuleNameHash, MD5Context.buf, sizeof ( MD5Context.buf ) );

				if ( GetModuleInformation ( hProcessToScan, dwProcessID, dwBaseAddress, dwModuleSize, Module ) == 0 )
				{
					bReturn = 1;
				}
			}
		}
	}
	
	return bReturn;
}
//========================================================================================
#define MINIMUM_LENGTH 8
#define MAX_SIZE_OF_BUFFER 0x1000
//========================================================================================
DWORD __stdcall SearchMemoryString ( DWORD dwBaseAddress, DWORD dwRegionBase, DWORD dwRegionSize, PDWORD pdwBufferOut, int& iStringsFound )
{
	DWORD dwBufferSize, dwCRC, dwRoundedLength, dwSize, dwSizeOfPacket;

	UCHAR byte; // current byte
	UCHAR byte1; // previous byte
	UCHAR byte2; // byte before previous byte
	BOOLEAN printable;
	BOOLEAN printable1;
	BOOLEAN printable2;
	ULONG length;
	
	byte1 = 0;
	byte2 = 0;
	printable1 = FALSE;
	printable2 = FALSE;
	length = 0;
	
	ULONG lengthInBytes;
	ULONG bias;
	BOOLEAN isWide;

	lengthInBytes = length;
	bias = 0;
	isWide = FALSE;

	PBYTE pbBuffer;

	int iIterator, iLength;

	HANDLE hCurrentProcess;

	PBYTE pbDisplayBuffer = NULL;

	dwCRC = 0;
	dwSizeOfPacket = 0;

	String_Scan_Data_t* pString_Scan_Data;

	hCurrentProcess = GetCurrentProcess();
		
	pbDisplayBuffer = ( PBYTE )NtAllocateMemory ( hCurrentProcess, 0, 0x10000, PAGE_READWRITE, g_NtStatus ); 

	if ( pbDisplayBuffer == NULL )
	{
		SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		goto clean_up;
	}
		
	iLength = 0;

	pbBuffer = ( PBYTE )dwRegionBase;

	dwSize = dwRegionSize;

	for ( iIterator = 0; iIterator < dwSize; iIterator++ )
	{
		byte = pbBuffer [ iIterator ];
		printable = CharIsPrintable [ byte ];
				
		// To find strings Process Hacker uses a state table.
		// * byte2 - byte before previous byte
		// * byte1 - previous byte
		// * byte - current byte
		// * length - length of current string run
		//
		// The states are described below.
		//
		//    [byte2] [byte1] [byte] ...
		//    [char] means printable, [oth] means non-printable.
		//
		// 1. [char] [char] [char] ...
		//      (we're in a non-wide sequence)
		//      -> append char.
		// 2. [char] [char] [oth] ...
		//      (we reached the end of a non-wide sequence, or we need to start a wide sequence)
		//      -> if current string is big enough, create result (non-wide).
		//         otherwise if byte = null, reset to new string with byte1 as first character.
		//         otherwise if byte != null, reset to new string.
		// 3. [char] [oth] [char] ...
		//      (we're in a wide sequence)
		//      -> (byte1 should = null) append char.
		// 4. [char] [oth] [oth] ...
		//      (we reached the end of a wide sequence)
		//      -> (byte1 should = null) if the current string is big enough, create result (wide).
		//         otherwise, reset to new string.
		// 5. [oth] [char] [char] ...
		//      (we reached the end of a wide sequence, or we need to start a non-wide sequence)
		//      -> (excluding byte1) if the current string is big enough, create result (wide).
		//         otherwise, reset to new string with byte1 as first character and byte as
		//         second character.
		// 6. [oth] [char] [oth] ...
		//      (we're in a wide sequence)
		//      -> (byte2 and byte should = null) do nothing.
		// 7. [oth] [oth] [char] ...
		//      (we're starting a sequence, but we don't know if it's a wide or non-wide sequence)
		//      -> append char.
		// 8. [oth] [oth] [oth] ...
		//      (nothing)
		//      -> do nothing.
						
		if ( printable2 && printable1 && printable )
		{
			if ( length < MAX_SIZE_OF_BUFFER )
			{
				pbDisplayBuffer [ length ] = byte;

				length++;
			}
		}
		else if ( printable2 && printable1 && !printable )
		{
			if ( length >= MINIMUM_LENGTH )
			{
				goto CreateResult;
			}
			else if ( byte == 0 )
			{
				length = 1;

				pbDisplayBuffer [ 0 ] = byte1;
			}
			else
			{
				length = 0;
			}
		}
		else if ( printable2 && !printable1 && printable )
		{
			if ( length < MAX_SIZE_OF_BUFFER )
			{
				pbDisplayBuffer [ length ] = byte;

				length++;
			}
		}
		else if ( printable2 && !printable1 && !printable )
		{
			if ( length >= MINIMUM_LENGTH )
			{
				goto CreateResult;
			}
			else
			{
				length = 0;
			}
		}
		else if ( !printable2 && printable1 && printable )
		{
			if ( length >= MINIMUM_LENGTH + 1 ) // length - 1 >= minimumLength but avoiding underflow
			{
				length--; // exclude byte1

				goto CreateResult;
			}
			else
			{
				length = 2;
				
				pbDisplayBuffer [ 0 ] = byte1;
				pbDisplayBuffer [ 1 ] = byte;
			}
		}
		else if ( !printable2 && printable1 && !printable )
		{
			// Nothing
		}
		else if ( !printable2 && !printable1 && printable )
		{
			if ( length < MAX_SIZE_OF_BUFFER )
			{
				pbDisplayBuffer [ length ] = byte;

				length++;
			}
		}
		else if ( !printable2 && !printable1 && !printable )
		{
			// Nothing
		}

		goto AfterCreateResult;
		
		CreateResult:
		{			
			bias = 0;

			lengthInBytes = length;

			isWide = FALSE;

			dwBufferSize = sizeof ( String_Scan_Data_t );
						
			if ( printable1 == printable ) // determine if string was wide (refer to state table, 4 and 5)
			{
				isWide = TRUE;
//				lengthInBytes *= 2;
			}

			if ( printable ) // byte1 excluded (refer to state table, 5)
			{
				bias = 1;
			}
				
			dwCRC = dwCRCBlock ( dwCRC, pbDisplayBuffer, length );
								
			iStringsFound++;

			pString_Scan_Data = ( String_Scan_Data_t* )pdwBufferOut;

			memset ( pString_Scan_Data, 0, sizeof ( String_Scan_Data_t ) );

			dwRoundedLength = length;

			dwRoundedLength = ROUND_UP ( length, sizeof ( DWORD ) );
									
			pString_Scan_Data->m_dwStringHash = dwCRC;
			pString_Scan_Data->m_dwBackupStringHash = dwCRCBlock ( 0, pbDisplayBuffer, length );
			pString_Scan_Data->m_dwStringLengthRounded = dwRoundedLength;
			pString_Scan_Data->m_bIsWide = isWide;
			pString_Scan_Data->m_dwStringStart = dwBaseAddress + ( iIterator - bias - length );
			pString_Scan_Data->m_dwStringLength = length;

			memcpy ( ( PVOID )&pString_Scan_Data->m_pbBuffer, pbDisplayBuffer, length );

			dwBufferSize += dwRoundedLength;
														
			dwSizeOfPacket += dwBufferSize;

			pdwBufferOut += dwBufferSize / sizeof ( PVOID );
			
			length = 0;
			lengthInBytes = 0;
		}
					
		AfterCreateResult:

		byte2 = byte1;
		byte1 = byte;
		printable2 = printable1;
		printable1 = printable;
	}

	
clean_up:

	if ( pbDisplayBuffer != NULL )
	{
		if ( !NtFreeMemory ( hCurrentProcess, ( DWORD )pbDisplayBuffer, 0, MEM_RELEASE, g_NtStatus ) )
		{
			SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

			RtlNtStatusToDosError ( g_NtStatus );

			dwSizeOfPacket = 0;
		}
	}
	
	return dwSizeOfPacket;
}
//========================================================================================
void Asemble ( PBYTE pbOutput, hde32s* hs )
{
	if ( hs->p_seg )
	{
		*pbOutput++ = hs->p_seg;
	}
	if ( hs->p_lock )
	{
		*pbOutput++ = hs->p_lock;
	}
	if ( hs->p_rep )
	{
		*pbOutput++ = hs->p_rep;
	}
	if ( hs->p_66 )
	{
		*pbOutput++ = hs->p_66;
	}
	if ( hs->p_67 ) 
	{	
		*pbOutput++ = hs->p_67;
	}
	
	*pbOutput++ = hs->opcode;
	
	if ( hs->opcode2 )
	{
		*pbOutput++ = hs->opcode2;
	}
	if ( hs->flags & F_MODRM )
	{
		*pbOutput++ = hs->modrm;
	}
	if ( hs->flags & F_SIB )
	{
		*pbOutput++ = hs->sib;
	}
				
	if ( ( hs->disp.disp8 == hs->disp.disp16 == hs->disp.disp32 ) && hs->disp.disp8 != 0 )
	{
		*pbOutput++ = hs->disp.disp8;
	}
	else if ( ( hs->disp.disp16 == hs->disp.disp32 ) && hs->disp.disp16 != 0 )
	{
		if ( hs->disp.disp16 > 0xFF )
		{
			memcpy ( ( void* )pbOutput, &hs->disp.disp16, sizeof ( uint16_t ) );
		
			pbOutput += sizeof ( uint16_t );
		}
		else
		{
			memcpy ( ( void* )pbOutput, &hs->disp.disp16, sizeof ( uint8_t ) );
		
			pbOutput += sizeof ( uint8_t );
		}
	}
	if ( ( hs->imm.imm8 == hs->imm.imm16 == hs->imm.imm32  ) && hs->imm.imm8 != 0 )
	{
		*pbOutput++ = hs->imm.imm8;
	}
	else if ( ( hs->imm.imm16 == hs->imm.imm32 ) && hs->imm.imm16 != 0 )
	{
		if ( hs->imm.imm16 > 0xFF )
		{
			memcpy ( ( void* )pbOutput, &hs->imm.imm16, sizeof ( uint16_t ) );
		
			pbOutput += sizeof ( uint16_t );
		}
		else
		{
			memcpy ( ( void* )pbOutput, &hs->imm.imm16, sizeof ( uint8_t ) );
		
			pbOutput += sizeof ( uint8_t );
		}
	}
	if ( ( hs->rel.rel8 == hs->rel.rel16 == hs->rel.rel32 ) && hs->rel.rel8 != 0 )
	{
		*pbOutput++ = hs->rel.rel8;
	}
	else if ( ( hs->rel.rel16 == hs->rel.rel32 ) && hs->rel.rel16 != 0 )
	{
		if ( hs->rel.rel16 > 0xFF )
		{
			memcpy ( ( void* )pbOutput, &hs->rel.rel16, sizeof ( uint16_t ) );
		
			pbOutput += sizeof ( uint16_t );
		}
		else
		{
			memcpy ( ( void* )pbOutput, &hs->rel.rel16, sizeof ( uint8_t ) );
		
			pbOutput += sizeof ( uint8_t );
		}
	}

	return;
}
//========================================================================================
DWORD __stdcall dwCRCFunction ( DWORD dwAllocatedRegion, DWORD dwAllocatedRegionSize, DWORD& dwBytesProcessed, DWORD& dwFunctionLength )
{
	BYTE bAssembledOutput [ 16 ];

	DWORD dwCRC, dwStart;
		
	hde32s hs;

	int iBytesProcessed, iCurrentLength, iFunctionLength, iLength, iPosition;

	PBYTE pbMemoryRegion;

	dwCRC = 0;

	iBytesProcessed = 0;
	iCurrentLength = 0;
	iFunctionLength = 0;

	iPosition = 0;

	pbMemoryRegion = ( PBYTE )dwAllocatedRegion;

	dwStart = ( DWORD )pbMemoryRegion;

	iFunctionLength = GetFunctionLengthVAC3Method ( ( DWORD )pbMemoryRegion, dwAllocatedRegionSize );

	do
	{
		iCurrentLength = hde32_disasm ( pbMemoryRegion, &hs );

		iPosition += iCurrentLength;

		Asemble ( bAssembledOutput, &hs ); // construct the bytes to md5
										
		dwCRC = dwCRCBlock ( dwCRC, ( PVOID )pbMemoryRegion, iCurrentLength );
																									
		pbMemoryRegion += iCurrentLength;
		
		iBytesProcessed += iCurrentLength;
				
	}while ( iPosition <= iFunctionLength );

	dwBytesProcessed = iBytesProcessed;
	dwFunctionLength = iFunctionLength;

	return dwCRC;

}
struct MD5Context {
        uint32 buf[4];
        uint32 bits[2];
        unsigned char in[64];
};

void MD5Transform(uint32 *buf, uint32 *in);

#ifdef sgi
#define HIGHFIRST
#endif

#ifdef sun
#define HIGHFIRST
#endif

#ifndef HIGHFIRST
#define byteReverse(buf, len)	/* Nothing */
#else
/*
 * Note: this code is harmless on little-endian machines.
 */
void byteReverse(unsigned char *buf, unsigned int longs)
{
   uint32 t;
    do {
	t = (uint32) ((unsigned) buf[3] << 8 | buf[2]) << 16 |
	    ((unsigned) buf[1] << 8 | buf[0]);
	*(uint32 *) buf = t;
	buf += 4;
    } while (--longs);
}
#endif

/*
 * Start MD5 accumulation.  Set bit count to 0 and buffer to mysterious
 * initialization constants.
 */
void MD5Init(struct MD5Context *ctx)
{
    ctx->buf[0] = 0x67452301;
    ctx->buf[1] = 0xefcdab89;
    ctx->buf[2] = 0x98badcfe;
    ctx->buf[3] = 0x10325476;

    ctx->bits[0] = 0;
    ctx->bits[1] = 0;
}

/*
 * Update context to reflect the concatenation of another buffer full
 * of bytes.
 */
void MD5Update(struct MD5Context *ctx, unsigned char *buf, unsigned int len)
{
    uint32 t;

    /* Update bitcount */

    t = ctx->bits[0];
    if ((ctx->bits[0] = t + ((uint32) len << 3)) < t)
	ctx->bits[1]++; 	/* Carry from low to high */
    ctx->bits[1] += len >> 29;

    t = (t >> 3) & 0x3f;	/* Bytes already in shsInfo->data */

    /* Handle any leading odd-sized chunks */

    if (t) {
	unsigned char *p = (unsigned char *) ctx->in + t;

	t = 64 - t;
	if (len < t) {
	    memcpy(p, buf, len);
	    return;
	}
	memcpy(p, buf, t);
	byteReverse(ctx->in, 16);
	MD5Transform(ctx->buf, (uint32 *) ctx->in);
	buf += t;
	len -= t;
    }
    /* Process data in 64-byte chunks */

    while (len >= 64) {
	memcpy(ctx->in, buf, 64);
	byteReverse(ctx->in, 16);
	MD5Transform(ctx->buf, (uint32 *) ctx->in);
	buf += 64;
	len -= 64;
    }

    /* Handle any remaining bytes of data. */

    memcpy(ctx->in, buf, len);
}

/*
 * Final wrapup - pad to 64-byte boundary with the bit pattern 
 * 1 0* (64-bit count of bits processed, MSB-first)
 */
void MD5Final(unsigned char *digest, struct MD5Context *ctx)
{
    unsigned count;
    unsigned char *p;

    /* Compute number of bytes mod 64 */
    count = (ctx->bits[0] >> 3) & 0x3F;

    /* Set the first char of padding to 0x80.  This is safe since there is
       always at least one byte free */
    p = ctx->in + count;
    *p++ = 0x80;

    /* Bytes of padding needed to make 64 bytes */
    count = 64 - 1 - count;

    /* Pad out to 56 mod 64 */
    if (count < 8) {
	/* Two lots of padding:  Pad the first block to 64 bytes */
	memset(p, 0, count);
	byteReverse(ctx->in, 16);
	MD5Transform(ctx->buf, (uint32 *) ctx->in);

	/* Now fill the next block with 56 bytes */
	memset(ctx->in, 0, 56);
    } else {
	/* Pad block to 56 bytes */
	memset(p, 0, count - 8);
    }
    byteReverse(ctx->in, 14);

    /* Append length in bits and transform */
    ((uint32 *) ctx->in)[14] = ctx->bits[0];
    ((uint32 *) ctx->in)[15] = ctx->bits[1];

    MD5Transform(ctx->buf, (uint32 *) ctx->in);
    byteReverse((unsigned char *) ctx->buf, 4);
    memcpy(digest, ctx->buf, 16);
    memset(ctx, 0, sizeof(ctx));        /* In case it's sensitive */
}


/* The four core functions - F1 is optimized somewhat */

/* #define F1(x, y, z) (x & y | ~x & z) */
#define F1(x, y, z) (z ^ (x & (y ^ z)))
#define F2(x, y, z) F1(z, x, y)
#define F3(x, y, z) (x ^ y ^ z)
#define F4(x, y, z) (y ^ (x | ~z))

/* This is the central step in the MD5 algorithm. */
#define MD5STEP(f, w, x, y, z, data, s) \
	(w += f(x, y, z) + data,  w = w<<s | w>>(32-s),  w += x)

/*
 * The core of the MD5 algorithm, this alters an existing MD5 hash to
 * reflect the addition of 16 longwords of new data.  MD5Update blocks
 * the data and converts bytes into longwords for this routine.
 */
void MD5Transform(uint32 *buf, uint32 *in)
    // uint32 buf[4]; uint32 in[16];
{
    register uint32 a, b, c, d;

    a = buf[0];
    b = buf[1];
    c = buf[2];
    d = buf[3];

    MD5STEP(F1, a, b, c, d, in[0] + 0xd76aa478, 7);
    MD5STEP(F1, d, a, b, c, in[1] + 0xe8c7b756, 12);
    MD5STEP(F1, c, d, a, b, in[2] + 0x242070db, 17);
    MD5STEP(F1, b, c, d, a, in[3] + 0xc1bdceee, 22);
    MD5STEP(F1, a, b, c, d, in[4] + 0xf57c0faf, 7);
    MD5STEP(F1, d, a, b, c, in[5] + 0x4787c62a, 12);
    MD5STEP(F1, c, d, a, b, in[6] + 0xa8304613, 17);
    MD5STEP(F1, b, c, d, a, in[7] + 0xfd469501, 22);
    MD5STEP(F1, a, b, c, d, in[8] + 0x698098d8, 7);
    MD5STEP(F1, d, a, b, c, in[9] + 0x8b44f7af, 12);
    MD5STEP(F1, c, d, a, b, in[10] + 0xffff5bb1, 17);
    MD5STEP(F1, b, c, d, a, in[11] + 0x895cd7be, 22);
    MD5STEP(F1, a, b, c, d, in[12] + 0x6b901122, 7);
    MD5STEP(F1, d, a, b, c, in[13] + 0xfd987193, 12);
    MD5STEP(F1, c, d, a, b, in[14] + 0xa679438e, 17);
    MD5STEP(F1, b, c, d, a, in[15] + 0x49b40821, 22);

    MD5STEP(F2, a, b, c, d, in[1] + 0xf61e2562, 5);
    MD5STEP(F2, d, a, b, c, in[6] + 0xc040b340, 9);
    MD5STEP(F2, c, d, a, b, in[11] + 0x265e5a51, 14);
    MD5STEP(F2, b, c, d, a, in[0] + 0xe9b6c7aa, 20);
    MD5STEP(F2, a, b, c, d, in[5] + 0xd62f105d, 5);
    MD5STEP(F2, d, a, b, c, in[10] + 0x02441453, 9);
    MD5STEP(F2, c, d, a, b, in[15] + 0xd8a1e681, 14);
    MD5STEP(F2, b, c, d, a, in[4] + 0xe7d3fbc8, 20);
    MD5STEP(F2, a, b, c, d, in[9] + 0x21e1cde6, 5);
    MD5STEP(F2, d, a, b, c, in[14] + 0xc33707d6, 9);
    MD5STEP(F2, c, d, a, b, in[3] + 0xf4d50d87, 14);
    MD5STEP(F2, b, c, d, a, in[8] + 0x455a14ed, 20);
    MD5STEP(F2, a, b, c, d, in[13] + 0xa9e3e905, 5);
    MD5STEP(F2, d, a, b, c, in[2] + 0xfcefa3f8, 9);
    MD5STEP(F2, c, d, a, b, in[7] + 0x676f02d9, 14);
    MD5STEP(F2, b, c, d, a, in[12] + 0x8d2a4c8a, 20);

    MD5STEP(F3, a, b, c, d, in[5] + 0xfffa3942, 4);
    MD5STEP(F3, d, a, b, c, in[8] + 0x8771f681, 11);
    MD5STEP(F3, c, d, a, b, in[11] + 0x6d9d6122, 16);
    MD5STEP(F3, b, c, d, a, in[14] + 0xfde5380c, 23);
    MD5STEP(F3, a, b, c, d, in[1] + 0xa4beea44, 4);
    MD5STEP(F3, d, a, b, c, in[4] + 0x4bdecfa9, 11);
    MD5STEP(F3, c, d, a, b, in[7] + 0xf6bb4b60, 16);
    MD5STEP(F3, b, c, d, a, in[10] + 0xbebfbc70, 23);
    MD5STEP(F3, a, b, c, d, in[13] + 0x289b7ec6, 4);
    MD5STEP(F3, d, a, b, c, in[0] + 0xeaa127fa, 11);
    MD5STEP(F3, c, d, a, b, in[3] + 0xd4ef3085, 16);
    MD5STEP(F3, b, c, d, a, in[6] + 0x04881d05, 23);
    MD5STEP(F3, a, b, c, d, in[9] + 0xd9d4d039, 4);
    MD5STEP(F3, d, a, b, c, in[12] + 0xe6db99e5, 11);
    MD5STEP(F3, c, d, a, b, in[15] + 0x1fa27cf8, 16);
    MD5STEP(F3, b, c, d, a, in[2] + 0xc4ac5665, 23);

    MD5STEP(F4, a, b, c, d, in[0] + 0xf4292244, 6);
    MD5STEP(F4, d, a, b, c, in[7] + 0x432aff97, 10);
    MD5STEP(F4, c, d, a, b, in[14] + 0xab9423a7, 15);
    MD5STEP(F4, b, c, d, a, in[5] + 0xfc93a039, 21);
    MD5STEP(F4, a, b, c, d, in[12] + 0x655b59c3, 6);
    MD5STEP(F4, d, a, b, c, in[3] + 0x8f0ccc92, 10);
    MD5STEP(F4, c, d, a, b, in[10] + 0xffeff47d, 15);
    MD5STEP(F4, b, c, d, a, in[1] + 0x85845dd1, 21);
    MD5STEP(F4, a, b, c, d, in[8] + 0x6fa87e4f, 6);
    MD5STEP(F4, d, a, b, c, in[15] + 0xfe2ce6e0, 10);
    MD5STEP(F4, c, d, a, b, in[6] + 0xa3014314, 15);
    MD5STEP(F4, b, c, d, a, in[13] + 0x4e0811a1, 21);
    MD5STEP(F4, a, b, c, d, in[4] + 0xf7537e82, 6);
    MD5STEP(F4, d, a, b, c, in[11] + 0xbd3af235, 10);
    MD5STEP(F4, c, d, a, b, in[2] + 0x2ad7d2bb, 15);
    MD5STEP(F4, b, c, d, a, in[9] + 0xeb86d391, 21);

    buf[0] += a;
    buf[1] += b;
    buf[2] += c;
    buf[3] += d;
}
//========================================================================================
DWORD __stdcall MD5Function ( DWORD dwAllocatedRegion, DWORD dwAllocatedRegionSize, DWORD& dwBytesProcessed, PBYTE pbDigest )
{
	BYTE bAssembledOutput [ 16 ];

	DWORD dwFunctionLength, dwStart;

	hde32s hs;

	int iBytesProcessed, iCurrentLength, iFunctionLength, iLength, iPosition;

	PBYTE pbMemoryRegion;

	iBytesProcessed = 0;
	iCurrentLength = 0;
	iFunctionLength = 0;

	iPosition = 0;

	pbMemoryRegion = ( PBYTE )dwAllocatedRegion;

	dwStart = ( DWORD )pbMemoryRegion;

	iFunctionLength = GetFunctionLengthVAC3Method ( ( DWORD )pbMemoryRegion, dwAllocatedRegionSize );

	MD5Context ctx;

	memset ( &ctx, 0, sizeof ( MD5Context ) );
	
	MD5Init ( &ctx );

	do
	{
		iCurrentLength = hde32_disasm ( pbMemoryRegion, &hs );

		iPosition += iCurrentLength;

		memset ( bAssembledOutput, 0, sizeof ( bAssembledOutput ) );

		Asemble ( bAssembledOutput, &hs ); // construct the bytes to md5
										
//		dwCRC = dwCRCBlock ( dwCRC, ( PVOID )pbMemoryRegion, iCurrentLength );

		MD5Update ( &ctx, bAssembledOutput, iCurrentLength );
																									
		pbMemoryRegion += iCurrentLength;
		
		iBytesProcessed += iCurrentLength;
				
	}while ( iPosition <= iFunctionLength );

	dwBytesProcessed = iBytesProcessed;
	dwFunctionLength = iFunctionLength;

	MD5Final ( pbDigest, &ctx );

	return iFunctionLength;

}
//========================================================================================
DWORD __stdcall ScanPrivateRegion ( HANDLE hProcessToScan, PSYSTEM_PROCESS_INFORMATION PSPI, DWORD dwBaseAddress, DWORD dwRegionSize, DWORD dwRegionProtect, PDWORD pdwBufferOut )
{	
	bool bFound;

	DWORD dwBase, dwBytesProcessed, dwBytesRead, dwCRC, dwLength, dwPrologue, dwAllocatedBase, dwAllocatedRegionSize, dwSize, dwSizeOfPacket, dwStart, dwTotalBytesProcessed;

	HANDLE hCurrentProcess;

	int iBytesProcessed, iCurrentFunctions, iCurrentLength, iIterator, iFunctionLength, iPosition, iStringsFound;

	PBYTE pbMemoryRegion, pbCopyBuffer;

	PDWORD pdwPrologue;
	
	dwBase = 0;
	dwCRC = 0;
	dwLength = 0;
	dwSize = 0;
	
	iBytesProcessed = 0;
	iCurrentFunctions = 0;
	iCurrentLength = 0;
	iIterator = 0;
	iFunctionLength = 0;
	iPosition = 0;
	iStringsFound = 0;
	
	hCurrentProcess = GetCurrentProcess();

	NTSTATUS status;

	M_M_M_ScanEntry_t* PM_M_M_ScanEntry;

	Function_Scan_Data_t* PFunction_Scan_Data;
	
	PM_M_M_ScanEntry = ( M_M_M_ScanEntry_t* )pdwBufferOut;

	memset ( PM_M_M_ScanEntry, 0, sizeof ( M_M_M_ScanEntry_t ) );

	pdwBufferOut += sizeof ( M_M_M_ScanEntry_t ) / sizeof ( PVOID ); // adjust so we have place to put the function/string hashes after the header for this part of the packet

	dwSizeOfPacket = sizeof ( M_M_M_ScanEntry_t );
	
	dwAllocatedBase = ( DWORD )g_pvMemoryAllocatedBase;
	dwAllocatedRegionSize = dwRegionSize;

	pbMemoryRegion = ( PBYTE )dwAllocatedBase;
	pbCopyBuffer = ( PBYTE )g_pdwFixedFunction;

	status = NtReadVirtualMemory ( hProcessToScan, ( PVOID )dwBaseAddress, g_pvMemoryAllocatedBase, dwAllocatedRegionSize, &dwBytesRead );

	dwTotalBytesProcessed = 0;

	if ( NT_SUCCESS ( status ) )
	{
		while ( ( ( DWORD )pbMemoryRegion + 4 ) < ( dwAllocatedBase + dwAllocatedRegionSize ) ) 
		{
			bFound = false;

			pdwPrologue = ( PDWORD )pbMemoryRegion;

			dwPrologue = *pdwPrologue;
																	
			if ( dwPrologue == 0x83EC8B55 )
			{
				bFound = true;
			}
			else if ( dwPrologue == 0x56EC8B55 )
			{
				bFound = true;
			}
			else if ( dwPrologue == 0x83F98B57 )
			{
				bFound = true;
			}
			else if ( dwPrologue == 0xF18B5653 )
			{
				bFound = true;
			}
			else if ( dwPrologue == 0xEC83EED9 )
			{
				bFound = true;
			}
			else if ( ( dwPrologue & 0x00FFFFFF ) == 0x8B5651 )
			{
				bFound = true;
			}
			else if ( ( dwPrologue & 0x00FFFFFF )  == 0xF18B56 )
			{
				bFound = true;
			}
			else if ( ( dwPrologue & 0x00FFFFFF )  == 0x68FF6A )
			{
				bFound = true;
			}
			else if ( (  dwPrologue & 0x00FFFFFF )  == 0x565553 )
			{
				bFound = true;
			}
			else if ( ( dwPrologue & 0xFFFF00FF ) == 0x83EC0053 )
			{
				bFound = true;
			}
			else if ( ( dwPrologue & 0x00FFFFFF ) == 0xEC8B55 )
			{
				bFound = true;
			}
			if ( bFound == false )
			{
				pbMemoryRegion++;
			}
			else
			{
				dwStart = ( DWORD )pbMemoryRegion;

				PFunction_Scan_Data = ( Function_Scan_Data_t* )pdwBufferOut;

//				PFunction_Scan_Data->m_dwFunctionHash = dwCRCFunction ( ( DWORD )pbMemoryRegion, dwAllocatedRegionSize, dwBytesProcessed, PFunction_Scan_Data->m_dwFunctionLength );
				
				memset ( PFunction_Scan_Data, 0, sizeof ( Function_Scan_Data_t ) );

				PFunction_Scan_Data->m_dwFunctionLength = MD5Function ( ( DWORD )pbMemoryRegion, dwAllocatedRegionSize, dwBytesProcessed, ( PBYTE )PFunction_Scan_Data->m_dwMD5 );

				PFunction_Scan_Data->m_dwFunctionStart = dwBaseAddress + ( dwStart - dwAllocatedBase );
								
				iCurrentFunctions++;

				dwLength = ROUND_UP ( PFunction_Scan_Data->m_dwFunctionLength, sizeof ( DWORD ) );

				dwSizeOfPacket += sizeof ( Function_Scan_Data_t );
				
				dwSizeOfPacket += dwLength;
				
				memcpy ( ( PVOID )&PFunction_Scan_Data->m_pbOpcodes, pbMemoryRegion, PFunction_Scan_Data->m_dwFunctionLength );

				pbMemoryRegion += PFunction_Scan_Data->m_dwFunctionLength;

				PFunction_Scan_Data->m_dwFunctionLengthRounded = dwLength;
							
				pdwBufferOut += sizeof ( Function_Scan_Data_t ) / sizeof ( PVOID );

				pdwBufferOut += PFunction_Scan_Data->m_dwFunctionLengthRounded / sizeof ( PVOID );

				dwTotalBytesProcessed += dwBytesProcessed;
			}
		}
								
		dwSize = SearchMemoryString ( dwBaseAddress, dwAllocatedBase, dwAllocatedRegionSize, pdwBufferOut, iStringsFound );

		pdwBufferOut += dwSize / sizeof ( PVOID );

		dwSizeOfPacket += dwSize;
		
		// now search for kernel32,user32 and ntdll imports
	}
	else
	{

	}

	PM_M_M_ScanEntry->m_dwBytesProcessed = dwTotalBytesProcessed;
		
	if ( iCurrentFunctions != 0 )
	{
		PM_M_M_ScanEntry->m_dwFlags |= CODE_FOUND;
		PM_M_M_ScanEntry->m_dwFunctionsFound = iCurrentFunctions;
	}
	if ( iStringsFound != 0 )
	{
		PM_M_M_ScanEntry->m_dwFlags |= STRINGS_FOUND;
		PM_M_M_ScanEntry->m_dwStringsFound = iStringsFound;
	}

	PM_M_M_ScanEntry->m_dwMemoryRegionBase = dwBaseAddress;
	PM_M_M_ScanEntry->m_dwMemoryRegionSize = dwRegionSize;
	PM_M_M_ScanEntry->m_dwMemoryRegionProtection = dwRegionProtect;

	PM_M_M_ScanEntry->m_dwProcessID = PSPI->ProcessId;
	
	return dwSizeOfPacket;
}
//========================================================================================
HMODULE __stdcall GetModuleHandleEx ( HANDLE hCurrentProcess, HANDLE hProcessToScan, DWORD dwModuleNameHash )
{	
	DWORD dwBytesRead, dwCRC, dwLength;

	HMODULE hModuleToScan;

	int iEntryIterator, iPosition;

	LDR_DATA_TABLE_ENTRY LDR_MODULE;
	LIST_ENTRY Entry;
	
	NTSTATUS status;

	PBYTE pbName;

	PLIST_ENTRY InLoadOrderModuleList;
	PLIST_ENTRY InMemoryOrderModuleList;
	PLIST_ENTRY InInitializationOrderModuleList;
	PLIST_ENTRY pCurrent;

	PROCESS_BASIC_INFORMATION PBI;
	PROCESS_ENVIRONMENT_BLOCK PEB;

	PEB_LDR_DATA LDR_DATA;

	PLDR_DATA_TABLE_ENTRY module;

	PLIST_ENTRY pEntry;
	
	PWSTR pwsBuffer;
			
	pwsBuffer = ( PWSTR )NtAllocateMemory ( hCurrentProcess, 0, g_dwSizeOfPage, PAGE_READWRITE, g_NtStatus );

	hModuleToScan = 0;
			
	if ( pwsBuffer == NULL )
	{
		SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		return hModuleToScan;
	}

	memset ( pwsBuffer, 0, g_dwSizeOfPage );
	
	status = NtQueryInformationProcess ( hProcessToScan, ProcessBasicInformation, &PBI, sizeof ( PROCESS_BASIC_INFORMATION ), &dwLength );

	if ( NT_SUCCESS ( status ) )
	{
		status = NtReadVirtualMemory ( hProcessToScan, PBI.PebBaseAddress, &PEB, sizeof ( PROCESS_ENVIRONMENT_BLOCK ), &dwBytesRead );

		if ( NT_SUCCESS ( status ) )
		{
			status = NtReadVirtualMemory ( hProcessToScan, PEB.Ldr, &LDR_DATA, sizeof ( PEB_LDR_DATA ), &dwBytesRead );

			if ( NT_SUCCESS ( status ) )
			{
				InLoadOrderModuleList = ( PLIST_ENTRY )&LDR_DATA.InLoadOrderModuleList;
				InMemoryOrderModuleList = ( PLIST_ENTRY )&LDR_DATA.InMemoryOrderModuleList;
				InInitializationOrderModuleList = ( PLIST_ENTRY )&LDR_DATA.InInitializationOrderModuleList;
																		
				for ( iEntryIterator = 0; iEntryIterator < 3; iEntryIterator++ )
				{
					if ( hModuleToScan != 0 )
					{
						break;
					}

					if ( iEntryIterator == 0 )
					{
						pCurrent = LDR_DATA.InLoadOrderModuleList.Blink;
						pEntry = LDR_DATA.InLoadOrderModuleList.Flink;
					}
					else if ( iEntryIterator == 1 )
					{
						pCurrent = LDR_DATA.InMemoryOrderModuleList.Blink;
						pEntry = LDR_DATA.InMemoryOrderModuleList.Flink;
					}
					else if ( iEntryIterator == 2 )
					{
						pCurrent = LDR_DATA.InInitializationOrderModuleList.Blink;
						pEntry = LDR_DATA.InInitializationOrderModuleList.Flink;
					}

					pCurrent = pEntry;

					for ( ; pEntry != NULL/*, pEntry != pCurrent*/; )
					{
						if ( iEntryIterator == 0 )
						{
							module = CONTAINING_RECORD ( pEntry, LDR_DATA_TABLE_ENTRY, InLoadOrderModuleList );
						}
						else if ( iEntryIterator == 1 )
						{
							module = CONTAINING_RECORD ( pEntry, LDR_DATA_TABLE_ENTRY, InMemoryOrderModuleList );
						}
						else if ( iEntryIterator == 2 )
						{
							module = CONTAINING_RECORD ( pEntry, LDR_DATA_TABLE_ENTRY, InInitializationOrderModuleList );
						}
						
						status = NtReadVirtualMemory ( hProcessToScan, module, &LDR_MODULE, sizeof ( LDR_DATA_TABLE_ENTRY ), &dwBytesRead );
					
						if ( NT_SUCCESS ( status ) )
						{	
							memset ( pwsBuffer, 0, g_dwSizeOfPage );

							status = NtReadVirtualMemory ( hProcessToScan, LDR_MODULE.BaseDllName.Buffer, pwsBuffer, LDR_MODULE.BaseDllName.Length, &dwBytesRead );
													
							if ( NT_SUCCESS ( status ) )
							{
								pbName = ( PBYTE )pwsBuffer;

								for ( iPosition = 0; iPosition < LDR_MODULE.BaseDllName.Length ; iPosition++ )
								{
									if ( pbName [ iPosition ] >= 'A' && pbName [ iPosition ] <= 'Z' ) // lowercase
										pbName [ iPosition ] |= ' ';
								}

								dwCRC = dwCRCBlock ( 0, pwsBuffer, LDR_MODULE.BaseDllName.Length );
							
								if ( dwCRC == dwModuleNameHash )
								{
									hModuleToScan = ( HMODULE )LDR_MODULE.BaseAddress;

									break;
								}
							}
							else
							{
								break;
							}

							status = NtReadVirtualMemory ( hProcessToScan, pEntry, &Entry, sizeof ( LIST_ENTRY ), &dwBytesRead );

							if ( !NT_SUCCESS ( status ) )
							{
								break;
							}

							pEntry = Entry.Flink;

							if ( pEntry == pCurrent )
							{
								break;
							}
						}
					}
				}
			}
		}
	}
	if ( !NtFreeMemory ( hCurrentProcess, ( DWORD )pwsBuffer, 0, MEM_RELEASE, g_NtStatus ) )
	{
		SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		hModuleToScan = 0;
	}

	return hModuleToScan;
}
//========================================================================================
#define PRESENT_IN_LOAD_ORDER ( 1 << 0 )
#define PRESENT_IN_MEMORY_ORDER ( 1 << 1 )
#define PRESENT_IN_INITILIZATION_ORDER ( 1 << 2 )
//========================================================================================
BOOL __stdcall bIsUnlinked ( HANDLE hCurrentProcess, HANDLE hProcessToScan, PMEMORY_SECTION_NAME PMSN, DWORD dwSizeOfPage )
{
	BOOL bFoundModule;

	DWORD dwBytesRead, dwCRC, dwLength, dwMSNCRC;

	int iEntryIterator, iIterator, iPosition;

	LDR_DATA_TABLE_ENTRY LDR_MODULE;
	LIST_ENTRY Entry;
	
	NTSTATUS status;

	PBYTE pbBuffer;

	PLIST_ENTRY InLoadOrderModuleList;
	PLIST_ENTRY InMemoryOrderModuleList;
	PLIST_ENTRY InInitializationOrderModuleList;
	PLIST_ENTRY pCurrent;

	PROCESS_BASIC_INFORMATION PBI;
	PROCESS_ENVIRONMENT_BLOCK PEB;

	PEB_LDR_DATA LDR_DATA;

	PLDR_DATA_TABLE_ENTRY module;

	PLIST_ENTRY pEntry;
	
	PWSTR pwsBuffer;

	bFoundModule = 0;

	pwsBuffer = ( PWSTR )NtAllocateMemory ( hCurrentProcess, 0, dwSizeOfPage, PAGE_READWRITE, g_NtStatus );
	
	if ( pwsBuffer == NULL )
	{
		SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		return bFoundModule;
	}

	memset ( pwsBuffer, 0, dwSizeOfPage );

	memset ( PMSN, 0, dwSizeOfPage );

	status = NtQueryVirtualMemory ( hProcessToScan, ( PVOID )Module.m_dwModuleBase, MemorySectionName, PMSN, dwSizeOfPage, 0 );

	if ( NT_SUCCESS ( status ) )
	{
		status = NtQueryInformationProcess ( hProcessToScan, ProcessBasicInformation, &PBI, sizeof ( PROCESS_BASIC_INFORMATION ), &dwLength );

		if ( NT_SUCCESS ( status ) )
		{
			status = NtReadVirtualMemory ( hProcessToScan, PBI.PebBaseAddress, &PEB, sizeof ( PROCESS_ENVIRONMENT_BLOCK ), &dwBytesRead );

			if ( NT_SUCCESS ( status ) )
			{
				status = NtReadVirtualMemory ( hProcessToScan, PEB.Ldr, &LDR_DATA, sizeof ( PEB_LDR_DATA ), &dwBytesRead );

				if ( NT_SUCCESS ( status ) )
				{
					InLoadOrderModuleList = ( PLIST_ENTRY )&LDR_DATA.InLoadOrderModuleList;
					InMemoryOrderModuleList = ( PLIST_ENTRY )&LDR_DATA.InMemoryOrderModuleList;
					InInitializationOrderModuleList = ( PLIST_ENTRY )&LDR_DATA.InInitializationOrderModuleList;

					for ( iEntryIterator = 0; iEntryIterator < 3; iEntryIterator++ )
					{
						if ( iEntryIterator == 0 )
						{
							pCurrent = LDR_DATA.InLoadOrderModuleList.Blink;
							pEntry = LDR_DATA.InLoadOrderModuleList.Flink;
						}
						else if ( iEntryIterator == 1 )
						{
							pCurrent = LDR_DATA.InMemoryOrderModuleList.Blink;
							pEntry = LDR_DATA.InMemoryOrderModuleList.Flink;
						}
						else if ( iEntryIterator == 2 )
						{
							pCurrent = LDR_DATA.InInitializationOrderModuleList.Blink;
							pEntry = LDR_DATA.InInitializationOrderModuleList.Flink;
						}

						pCurrent = pEntry;

						for ( ; pEntry != NULL/*, pEntry != pCurrent*/; )
						{
							if ( iEntryIterator == 0 )
							{
								module = CONTAINING_RECORD ( pEntry, LDR_DATA_TABLE_ENTRY, InLoadOrderModuleList );
							}
							else if ( iEntryIterator == 1 )
							{
								module = CONTAINING_RECORD ( pEntry, LDR_DATA_TABLE_ENTRY, InMemoryOrderModuleList );
							}
							else if ( iEntryIterator == 2 )
							{
								module = CONTAINING_RECORD ( pEntry, LDR_DATA_TABLE_ENTRY, InInitializationOrderModuleList );
							}
							
							status = NtReadVirtualMemory ( hProcessToScan, module, &LDR_MODULE, sizeof ( LDR_DATA_TABLE_ENTRY ), &dwBytesRead );
					
							if ( NT_SUCCESS ( status ) )
							{	
								memset ( pwsBuffer, 0, dwSizeOfPage );

								status = NtReadVirtualMemory ( hProcessToScan, LDR_MODULE.BaseDllName.Buffer, pwsBuffer, LDR_MODULE.BaseDllName.Length, &dwBytesRead );
													
								if ( NT_SUCCESS ( status ) )
								{
									pbBuffer = ( PBYTE )pwsBuffer;

									for ( iIterator = 0; iIterator < LDR_MODULE.BaseDllName.Length ; iIterator++ )
									{
										if ( pbBuffer [ iIterator ] >= 'A' && pbBuffer [ iIterator ] <= 'Z' ) // lowercase
											pbBuffer [ iIterator ] |= ' ';
									}

									dwCRC = dwCRCBlock ( 0, pwsBuffer, LDR_MODULE.BaseDllName.Length );
							
									pbBuffer = ( PBYTE )PMSN->SectionFileName.Buffer;
									
									for ( iIterator = 0; iIterator < PMSN->SectionFileName.Length ; iIterator++ )
									{
										if ( pbBuffer [ iIterator ] >= 'A' && pbBuffer [ iIterator ] <= 'Z' ) // lowercase
											pbBuffer [ iIterator ] |= ' ';
									}
							
									for ( iIterator = 0; iIterator < PMSN->SectionFileName.Length; iIterator++ )
									{
										if ( pbBuffer [ iIterator ] == '\\' || pbBuffer [ iIterator ] == '/' ) // keep going until we hit the last '\\' or '/'
										{
											iPosition = iIterator + 2;
										}
									}

									dwMSNCRC = dwCRCBlock ( 0, &pbBuffer [ iPosition ], ( PMSN->SectionFileName.Length - iPosition ) );

									if ( dwCRC == dwMSNCRC )
									{
										bFoundModule |= ( 1 << iEntryIterator );
																			
										break;
									}
								}
							}
							else
							{
								break;
							}

							status = NtReadVirtualMemory ( hProcessToScan, pEntry, &Entry, sizeof ( LIST_ENTRY ), &dwBytesRead );

							if ( !NT_SUCCESS ( status ) )
							{
								break;
							}

							pEntry = Entry.Flink;

							if ( pEntry == pCurrent )
							{
								break;
							}
						}
					}
				}
			}
		}
	}
	if ( !NtFreeMemory ( hCurrentProcess, ( DWORD )pwsBuffer, 0, MEM_RELEASE, g_NtStatus ) )
	{
		SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		return bFoundModule;
	}

	return bFoundModule;
}
//========================================================================================
DWORD __stdcall ScanImageSection ( HANDLE hProcessToScan, MD5Context_t& MD5Context, Module_t& Module, PSYSTEM_PROCESS_INFORMATION PSPI, PMEMORY_SECTION_NAME PMSN, PDWORD pdwBufferOut )
{
	BYTE bChar;

	BOOL bFoundModule, bFoundTextSection;

	DWORD dwBase, dwBytesRead, dwSize, dwMemoryBase, dwFileBase, dwBytesWritten, dwBaseMemory;
		
	DWORD dwCRC, dwPacketSize;

	DWORD dwRegionBase;
	DWORD dwRegionSize;

	HANDLE hCurrentProcess;

	int iIterator, iStrLen;

	FILE_STANDARD_INFORMATION FSI;

	MEMORY_BASIC_INFORMATION MBI;

    PIMAGE_BASE_RELOCATION RelocationDir;
    DWORD Count;
    DWORD Address;
    PUSHORT TypeOffset;
    DWORD Delta, dwSizeOfPacket;

	HANDLE hFile;

	IO_STATUS_BLOCK IoStatusBlock;

	UNICODE_STRING wszPath;
	
	OBJECT_ATTRIBUTES ObjectAttributes;
		
	PIMAGE_DOS_HEADER lpImageDosHeader;
	PIMAGE_NT_HEADERS lpImageNtHeaders;
	PIMAGE_SECTION_HEADER lpSectionHeader;

	Process_and_Module_ScanEntry_t* PP_M_ScanEntry;

	PBYTE pbBuffer;

	hCurrentProcess = GetCurrentProcess();

	NTSTATUS status;
	
	dwSizeOfPacket = 0;

	bFoundModule = 0;

	PP_M_ScanEntry = ( Process_and_Module_ScanEntry_t* )pdwBufferOut;

	PP_M_ScanEntry->Generic_Scan_Information.m_dwTimeDateStamp = Module.m_dwTimeDateStamp;
	PP_M_ScanEntry->Generic_Scan_Information.m_dwCRCOfFilePEHeader = Module.m_dwCRCOfFilePEHeader;
	PP_M_ScanEntry->Generic_Scan_Information.m_dwCRCOfPEHeader = Module.m_dwCRCOfPEHeader;
	
	PP_M_ScanEntry->Generic_Scan_Information.m_dwImageBase = Module.m_dwImageBase;
	PP_M_ScanEntry->Generic_Scan_Information.m_dwModuleBase = Module.m_dwModuleBase;
	PP_M_ScanEntry->Generic_Scan_Information.m_dwProcessID = PSPI->ProcessId;
	PP_M_ScanEntry->Generic_Scan_Information.m_dwTotalSize = Module.m_dwTotalSize;
	PP_M_ScanEntry->Generic_Scan_Information.m_dwVirtualSize = Module.m_dwVirtualSize;

	memcpy ( PP_M_ScanEntry->Generic_Scan_Information.m_bName, Module.m_bName, sizeof ( Module.m_bName )  );

	dwPacketSize = sizeof ( Process_and_Module_ScanEntry_t );

//	P_M_ScanEntry.m_dwFlags = 0;
		
	dwSize = 0;

	dwRegionBase = 0;
	dwRegionSize = 0;

	dwBase = Module.m_dwModuleBase;

	dwMemoryBase = ( DWORD )g_pvMemoryAllocatedBase;
	dwFileBase = ( DWORD )g_pvFileAllocatedBase;

	SYSTEM_BASIC_INFORMATION SBI;
	
	status = NtQuerySystemInformation ( SystemBasicInformation, &SBI, sizeof ( SYSTEM_BASIC_INFORMATION ), NULL );

	if ( NT_ERROR ( status ) )
	{
		SetStatus ( CORE_SYSTEM_API_FAILURE, status );

		RtlNtStatusToDosError ( g_NtStatus );

		return 0;
	}
		
	do
	{
		dwCRC = ~Module.m_dwModuleNameCRC;

		status = NtQueryVirtualMemory ( hProcessToScan, ( PVOID )dwBase, MemoryBasicInformation, &MBI, sizeof ( MEMORY_BASIC_INFORMATION ), 0 );

		if ( NT_SUCCESS ( status ) )
		{
			if ( MBI.State & MEM_COMMIT )
			{
				if ( MBI.Type & MEM_IMAGE )
				{	
					status = NtQueryVirtualMemory ( hProcessToScan, MBI.BaseAddress, MemorySectionName, PMSN, g_dwSizeOfPage, 0 );
				
					if ( NT_SUCCESS ( status ) )
					{
						dwCRC = dwCRCBlock ( 0, PMSN->SectionFileName.Buffer, PMSN->SectionFileName.Length );

						if ( dwCRC == Module.m_dwModuleNameCRC )
						{
							dwRegionSize += MBI.RegionSize;
						}
					}
				}
			}
			
			dwBase += MBI.RegionSize;
		}
		else
		{
			dwBase += g_dwSizeOfPage;
		}

	}while ( dwBase <= SBI.HighestUserAddress );
	
	if ( dwRegionSize > Module.m_dwVirtualSize )
	{
		dwRegionSize = Module.m_dwVirtualSize;
	}
	else if ( dwRegionSize <= 0 )
	{
		dwPacketSize = 0;

		goto end;
	}
	
	memset ( ( PVOID )dwMemoryBase, 0, 0x4000000 );
	memset ( ( PVOID )dwFileBase, 0, 0x4000000  );
/*
	bChar = 0;

	status = NtWriteVirtualMemory ( hCurrentProcess, g_pvMemoryAllocatedBase, &bChar, 0x4000000, NULL );

	if ( NT_ERROR ( status ) )
	{
		SetStatus ( CORE_SYSTEM_API_FAILURE, status );

		return 0;
	}

	status = NtWriteVirtualMemory ( hCurrentProcess, g_pvFileAllocatedBase, &bChar, 0x4000000, NULL );

	if ( NT_ERROR ( status ) )
	{
		SetStatus ( CORE_SYSTEM_API_FAILURE, status );

		return 0;
	}
*/	
	PP_M_ScanEntry->Generic_Scan_Information.m_dwFlags = bIsUnlinked ( hCurrentProcess, hProcessToScan, PMSN, g_dwSizeOfPage );
		
	status = NtReadVirtualMemory ( hProcessToScan, ( PVOID )Module.m_dwModuleBase, ( PVOID )dwMemoryBase, dwRegionSize, &dwBytesRead );
		
	if ( NT_SUCCESS ( status ) )
	{	
		lpImageDosHeader = ( PIMAGE_DOS_HEADER )dwMemoryBase;
						
		if ( lpImageDosHeader->e_magic == IMAGE_DOS_SIGNATURE )
		{						
			lpImageNtHeaders = ( PIMAGE_NT_HEADERS )( dwMemoryBase + lpImageDosHeader->e_lfanew );

			if ( lpImageNtHeaders->Signature == IMAGE_NT_SIGNATURE )
			{
				lpSectionHeader = IMAGE_FIRST_SECTION ( lpImageNtHeaders );
								
				Delta = Module.m_dwImageBase - Module.m_dwModuleBase;
								
				RelocationDir = ( PIMAGE_BASE_RELOCATION )( dwMemoryBase + lpImageNtHeaders->OptionalHeader.DataDirectory [ IMAGE_DIRECTORY_ENTRY_BASERELOC ].VirtualAddress );
				
				if ( RelocationDir != 0 )
				{
					while ( Delta != 0 && RelocationDir->SizeOfBlock > 0 )
					{
						Count = ( ( RelocationDir->SizeOfBlock ) - sizeof ( IMAGE_BASE_RELOCATION ) ) / sizeof ( USHORT );
						Address = ( DWORD )( dwMemoryBase + RelocationDir->VirtualAddress );
						TypeOffset = ( PUSHORT )( RelocationDir + 1 );

						RelocationDir = LdrProcessRelocationBlockLongLong ( Address,
										Count,
										TypeOffset,
										Delta );

						if ( RelocationDir == NULL )
						{
							break;
						}
					}
																														
					dwBase = 0;
					dwSize = 0;

					lpSectionHeader = IMAGE_FIRST_SECTION ( lpImageNtHeaders );

					bFoundTextSection = 0;
										
					memset ( &MD5Context, 0, sizeof ( MD5Context_t ) );

					MD5Init ( &MD5Context );

					dwBaseMemory = Module.m_dwModuleBase;
					
					for ( iIterator = 0; iIterator < lpImageNtHeaders->FileHeader.NumberOfSections; iIterator++ )
					{
						status = NtQueryVirtualMemory ( hProcessToScan, ( PVOID )dwBaseMemory, MemoryBasicInformation, &MBI, sizeof ( MEMORY_BASIC_INFORMATION ), 0 );

						if ( NT_SUCCESS ( status ) )
						{
							if ( MBI.Protect & PAGE_EXECUTE_READ || MBI.Protect & PAGE_EXECUTE_READWRITE
								|| MBI.Protect & PAGE_EXECUTE )
							{
								dwBase = dwBaseMemory - Module.m_dwModuleBase;

								dwSize = MBI.RegionSize;

								dwSize = ROUND_UP ( dwSize, g_dwSizeOfPage );

								bFoundTextSection = 1;

								pbBuffer = ( PBYTE )( dwMemoryBase + dwBase );
							
								do
								{
									MD5Update ( &MD5Context, pbBuffer, g_dwSizeOfPage );

									pbBuffer += g_dwSizeOfPage;

								}while ( ( DWORD )pbBuffer < ( dwMemoryBase + dwBase + dwSize ) );
							}

							dwBaseMemory += MBI.RegionSize;
						
							lpSectionHeader++;
						}
					}

					MD5Final ( &MD5Context );

					memcpy ( PP_M_ScanEntry->Generic_Scan_Information.m_dwMemoryMD5, MD5Context.buf, sizeof ( MD5Context.buf ) );

					if ( bFoundTextSection != 1 )
					{																
						dwPacketSize = 0;

						goto end;
					}

					PP_M_ScanEntry->Generic_Scan_Information.m_dwCRCOfMemoryPEHeader = dwCRCBlock ( 0, ( PVOID )dwMemoryBase, g_dwSizeOfPage );
				}
			}
			else
			{
				goto HASH_IT;
			}
		}
		else
		{
HASH_IT:
			memset ( &MD5Context, 0, sizeof ( MD5Context_t ) );
						
			pbBuffer = ( PBYTE )dwMemoryBase;

			MD5Init ( &MD5Context );

			do
			{
				MD5Update ( &MD5Context, pbBuffer, g_dwSizeOfPage );

				pbBuffer += g_dwSizeOfPage;

			}while ( ( DWORD )pbBuffer < ( ( DWORD )g_pvMemoryAllocatedBase + dwRegionSize ) );

			MD5Final ( &MD5Context );

			memcpy ( PP_M_ScanEntry->Generic_Scan_Information.m_dwMemoryMD5, MD5Context.buf, sizeof ( MD5Context.buf ) );
		}
	
		// now open the file on disk and scan it

		wszPath.Buffer = PMSN->SectionFileName.Buffer;
		wszPath.Length = PMSN->SectionFileName.Length;//wstrlen ( Module.m_wszModuleName );
		wszPath.MaximumLength = MAX_PATH;

		InitializeObjectAttributes ( &ObjectAttributes, &wszPath, OBJ_CASE_INSENSITIVE, NULL, NULL );
							
		if ( Module.m_hFile == NULL )
		{
			status = NtCreateFile ( &hFile, GENERIC_READ | SYNCHRONIZE, &ObjectAttributes, &IoStatusBlock, NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ, FILE_OPEN, FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0 );
		}
		else
		{
			hFile = Module.m_hFile;

			status = NtCreateFile ( &hFile, GENERIC_READ | SYNCHRONIZE, &ObjectAttributes, &IoStatusBlock, NULL, FILE_ATTRIBUTE_NORMAL, FILE_SHARE_READ, FILE_OPEN, FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT, NULL, 0 );
		}
		if ( NT_SUCCESS ( status ) )
		{
			status = NtQueryInformationFile ( hFile, &IoStatusBlock, &FSI, sizeof ( FILE_STANDARD_INFORMATION ), FileStandardInformation );
			
			if ( NT_SUCCESS ( status ) )
			{
				dwSize = Module.m_dwSizeOfHeaders;

				dwBytesRead = 0;

				if ( dwSize < 0x400 )
				{
					dwSize = g_dwSizeOfPage;
				}

				status = NtReadFile ( hFile, NULL, NULL, NULL, &IoStatusBlock, ( PVOID )dwFileBase, dwSize, NULL, NULL );

				if ( !NT_SUCCESS ( status ) )
				{
					goto clean_up;
				}

				dwBytesRead += ROUND_UP ( dwSize, g_dwSizeOfPage );

				lpImageDosHeader = ( PIMAGE_DOS_HEADER )dwFileBase;
			
				if ( lpImageDosHeader->e_magic == IMAGE_DOS_SIGNATURE )
				{
					lpImageNtHeaders = ( PIMAGE_NT_HEADERS )( dwFileBase + lpImageDosHeader->e_lfanew );
					
					if ( lpImageNtHeaders->Signature == IMAGE_NT_SIGNATURE )
					{
						lpSectionHeader = IMAGE_FIRST_SECTION ( lpImageNtHeaders );

						for ( iIterator = 0; iIterator < lpImageNtHeaders->FileHeader.NumberOfSections; iIterator++ )
						{
							status = NtReadFile ( hFile, NULL, NULL, NULL, &IoStatusBlock, ( LPVOID )( dwFileBase + dwBytesRead ), lpSectionHeader->SizeOfRawData, NULL, NULL );

							dwSize = ROUND_UP ( lpSectionHeader->Misc.VirtualSize, g_dwSizeOfPage );

							if ( NT_SUCCESS ( status ) )
							{
								dwBytesRead += dwSize;
							}
							else
							{
								goto clean_up;
							}
							
							lpSectionHeader++;
						}
												
						dwBase = 0;
						dwSize = 0;

						lpSectionHeader = IMAGE_FIRST_SECTION ( lpImageNtHeaders );

						bFoundTextSection = 0;

						memset ( &MD5Context, 0, sizeof ( MD5Context_t ) );

						MD5Init ( &MD5Context );
						
						for ( iIterator = 0; iIterator < lpImageNtHeaders->FileHeader.NumberOfSections; iIterator++ )
						{
							if ( lpSectionHeader->Characteristics & IMAGE_SCN_CNT_CODE
								|| lpSectionHeader->Characteristics & IMAGE_SCN_MEM_EXECUTE )
							{
								dwBase = lpSectionHeader->VirtualAddress;

								dwSize = ROUND_UP ( lpSectionHeader->Misc.VirtualSize, g_dwSizeOfPage );

								bFoundTextSection = 1;

								pbBuffer = ( PBYTE )( dwFileBase + dwBase );
							
								do
								{
									MD5Update ( &MD5Context, pbBuffer, g_dwSizeOfPage );

									pbBuffer += g_dwSizeOfPage;

								}while ( ( DWORD )pbBuffer < ( dwFileBase + dwBase + dwSize ) );
							}
							
							lpSectionHeader++;
							
						}

						MD5Final ( &MD5Context );

						memcpy ( PP_M_ScanEntry->Generic_Scan_Information.m_dwFileMD5, MD5Context.buf, sizeof ( MD5Context.buf ) );

						if ( bFoundTextSection != 1 )
						{
							dwPacketSize = 0;

							goto end;
						}
					}
				}																			
clean_up:										
				NtClose ( hFile );
			}
		}
		else
		{

		}
	}
	
end:
	return dwPacketSize;
}
//========================================================================================
DWORD __stdcall ScanProcessAndModules ( HANDLE hProcessToScan, PSYSTEM_PROCESS_INFORMATION PSPI, PVOID pvStartOfScanPacket, DWORD dwSizeTotal, PDWORD pdwBufferOut )
{
	BOOL bInheritHandle;

	BOOL bIsDEPEnabled, bScanRegion;
	
	CLIENT_ID CID;

	CONTEXT Context;

	memset ( &Context, 0, sizeof ( CONTEXT ) );

	DWORD dwAllocatedBase, dwAllocatedRegionSize, dwBase, dwBytesRead, dwCRC, dwPacketSize, dwSize, dwThreadStart;

	DWORD dwBytesProcessed, dwEntryPointLength, dwSizeRemaining;

	DWORD dwLength;

	int iCounter, iIterator, iReturnCode;
	
	HANDLE hCurrentProcess, hThread;

	MEMORY_BASIC_INFORMATION MBI;

	PMEMORY_SECTION_NAME PMSN;

	NTSTATUS status;

	OBJECT_ATTRIBUTES ObjectAttributes;
	
	Thread_Scan_t* pThread_Scan;

	ScanHeader_t ScanHeader;
	ScanFooter_t ScanFooter;

	ScanHeader_t* pScanHeader;
	ScanFooter_t* pScanFooter;

	PVOID pPacketBase;

	PVOID pvAllocatedBase;

	Mega_Scan_Header_t* pMega_Scan_Header;

	memset ( &MBI, 0, sizeof ( MEMORY_BASIC_INFORMATION ) );
	memset ( &Module, 0, sizeof ( Module_t ) );
	memset ( &Context, 0, sizeof ( CONTEXT ) );
	
	hCurrentProcess = GetCurrentProcess();

	PMSN = ( PMEMORY_SECTION_NAME )NtAllocateMemory ( hCurrentProcess, 0, g_dwSizeOfPage, PAGE_READWRITE, g_NtStatus );

	if ( PMSN == NULL )
	{
		SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		return 0;
	}

	pvAllocatedBase = NtAllocateMemory ( hCurrentProcess, 0, g_dwSizeOfPage * 2, PAGE_READWRITE, g_NtStatus );

	if ( pvAllocatedBase == NULL )
	{
		SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		return 0;
	}
		
	memset ( PMSN, 0, g_dwSizeOfPage );

	Scan_Packet_Process_t* pScan_Packet_Process;

	pScanHeader = ( ScanHeader_t* )pdwBufferOut;
		
	pdwBufferOut += sizeof ( ScanHeader_t ) / sizeof ( PVOID );

	pMega_Scan_Header = ( Mega_Scan_Header_t* )pdwBufferOut;
		
	pdwBufferOut += sizeof ( Mega_Scan_Header_t ) / sizeof ( PVOID );

	dwPacketSize = 0;

	SYSTEM_BASIC_INFORMATION SBI;
	
	status = NtQuerySystemInformation ( SystemBasicInformation, &SBI, sizeof ( SYSTEM_BASIC_INFORMATION ), NULL );

	if ( NT_ERROR ( status ) )
	{
		SetStatus ( CORE_SYSTEM_API_FAILURE, status );

		RtlNtStatusToDosError ( status );

		return 0;
	}

	pScan_Packet_Process = ( Scan_Packet_Process_t* )pvStartOfScanPacket;

	dwSizeRemaining = dwSizeTotal;
	
	do{	

		dwCRC = dwCRCBlock ( 0, PSPI->ProcessName.Buffer, PSPI->ProcessName.Length );

		if ( pScan_Packet_Process->m_dwProcessID != PSPI->ProcessId )
		{
			dwPacketSize = 0;

			break;
		}
		
		bIsDEPEnabled = IsProcessDEPEnabled ( hProcessToScan );

		pMega_Scan_Header->m_dwFlags = 0xDEADBEEF;
		pMega_Scan_Header->m_dwNumberOfManualMapEntries = 0;
		pMega_Scan_Header->m_dwNumberOfProcessAndModuleEntries = 0;
		pMega_Scan_Header->m_dwNumberOfThreadEntries = 0;
	
		dwBase = 0;
							
		while ( dwBase <= SBI.HighestUserAddress )
		{
			status = NtQueryVirtualMemory ( hProcessToScan, ( PVOID )dwBase, MemoryBasicInformation, &MBI, sizeof ( MEMORY_BASIC_INFORMATION ), 0 );

			if ( NT_SUCCESS ( status ) )
			{
				if ( MBI.State & MEM_COMMIT )
				{
					if ( MBI.Type & MEM_IMAGE )
					{	
						status = NtQueryVirtualMemory ( hProcessToScan, MBI.BaseAddress, MemorySectionName, PMSN, g_dwSizeOfPage, 0 );
				
						if ( NT_SUCCESS ( status ) )
						{
							iReturnCode = AcquireSectionNameAndMD5 ( hProcessToScan, PSPI->ProcessId, MD5Context, ( DWORD )MBI.BaseAddress, ( DWORD )MBI.RegionSize, Module, PMSN );
												
							if ( iReturnCode == 1 )
							{	
								dwSize = ScanImageSection ( hProcessToScan, MD5Context, Module, PSPI, PMSN, pdwBufferOut );

								if ( dwSize != 0 )
								{
									pdwBufferOut += dwSize / sizeof ( PVOID );

									dwPacketSize += dwSize;

									pMega_Scan_Header->m_dwNumberOfProcessAndModuleEntries++;
								}
							}
						}
					}
			
					dwBase += MBI.RegionSize;
				}
				else
				{
					dwBase += g_dwSizeOfPage;
				}
			}
		}
	
		dwBase = 0;
							
		while ( dwBase <= SBI.HighestUserAddress )
		{
			status = NtQueryVirtualMemory ( hProcessToScan, ( PVOID )dwBase, MemoryBasicInformation, &MBI, sizeof ( MEMORY_BASIC_INFORMATION ), 0 );

			if ( NT_SUCCESS ( status ) )
			{
				if ( MBI.State & MEM_COMMIT )
				{					
					if ( MBI.Type & MEM_PRIVATE || MBI.Type & MEM_IMAGE )
					{	
						bScanRegion = 0;

						if ( bIsDEPEnabled == 1 )
						{
//							if ( ( MBI.Protect & PAGE_EXECUTE_READ ) || ( MBI.Protect & PAGE_EXECUTE_READWRITE ) )
//							{
								bScanRegion = 1;
//							}
						}
						else
						{
							if ( ( MBI.Protect & PAGE_READONLY ) 
								|| ( MBI.Protect & PAGE_READWRITE )
								|| ( MBI.Protect & PAGE_EXECUTE_READ )
								|| ( MBI.Protect & PAGE_EXECUTE_READWRITE ) )
							{
								bScanRegion = 1;
							}
						}
							
						if ( MBI.RegionSize >= 0x6000 && MBI.RegionSize < 0x4000000 && ( MBI.Protect & PAGE_GUARD ) == false 
							&& ( MBI.Protect & PAGE_NOACCESS ) == false && bScanRegion == 1 )
						{
							dwSize = ScanPrivateRegion ( hProcessToScan, PSPI, ( DWORD )MBI.BaseAddress, ( DWORD )MBI.RegionSize, MBI.Protect, pdwBufferOut );
						
							pdwBufferOut += dwSize / sizeof ( PVOID );

							dwPacketSize += dwSize;

							pMega_Scan_Header->m_dwNumberOfManualMapEntries++;
						}
					}
			
					dwBase += MBI.RegionSize;
				}
				else
				{
					dwBase += g_dwSizeOfPage;
				}
			}
		}
	
		iCounter = 0;
	
		while ( iCounter < PSPI->ThreadCount )
		{			
			CID.UniqueProcess = PSPI->ProcessId;
			CID.UniqueThread = PSPI->Threads [ iCounter ].ClientId.UniqueThread;

			bInheritHandle = FALSE;

			InitializeObjectAttributes ( &ObjectAttributes, NULL, ( bInheritHandle ? OBJ_INHERIT : 0 ), NULL, NULL );

			status = NtOpenThread ( &hThread, THREAD_GET_CONTEXT | THREAD_QUERY_INFORMATION | THREAD_SET_INFORMATION | THREAD_SET_CONTEXT | THREAD_SUSPEND_RESUME, &ObjectAttributes, &CID );

			if ( NT_SUCCESS ( status ) )
			{
				status = NtQueryInformationThread ( hThread, ThreadQuerySetWin32StartAddress, &dwThreadStart, sizeof ( DWORD ), NULL );
						
				if ( NT_SUCCESS ( status ) )
				{
					status = NtReadVirtualMemory ( hProcessToScan, ( PVOID )dwThreadStart, ( PVOID )pvAllocatedBase, g_dwSizeOfPage, &dwBytesRead );

					if ( NT_SUCCESS ( status ) )
					{
						Context.ContextFlags = CONTEXT_DEBUG_REGISTERS;

						memset ( &Context, 0, sizeof ( CONTEXT ) ) ;

						status = NtGetContextThread ( hThread, &Context );

						if ( NT_SUCCESS ( status ) )
						{
							dwSize = sizeof ( Thread_Scan_t );

							dwPacketSize += sizeof ( Thread_Scan_t );

							pThread_Scan = ( Thread_Scan_t* )pdwBufferOut;

							memset ( pThread_Scan, 0, sizeof ( Thread_Scan_t ) );

							pThread_Scan->m_dwProcessID = PSPI->ProcessId;
							pThread_Scan->m_dwThreadID = PSPI->Threads [ iCounter ].ClientId.UniqueThread;
						
							dwBytesProcessed = 0;
							dwEntryPointLength = 0;

							pThread_Scan->m_dwEntryPoint = dwThreadStart;
//							pThread_Scan->m_dwEntryPointCRC = dwCRCFunction ( ( DWORD )pvAllocatedBase, g_dwSizeOfPage, dwBytesProcessed, dwEntryPointLength ); // FIXME: make sure method doesn't exceed region size

							status = NtQueryVirtualMemory ( hProcessToScan, ( PVOID )dwThreadStart, MemoryBasicInformation, &MBI, sizeof ( MEMORY_BASIC_INFORMATION ), 0 );

							if ( NT_SUCCESS ( status ) )
							{
								pThread_Scan->m_dwProtection = MBI.Protect;
							}

							dwEntryPointLength = MD5Function ( ( DWORD )pvAllocatedBase, g_dwSizeOfPage, dwBytesProcessed, ( PBYTE )pThread_Scan->m_dwMD5 );

							pThread_Scan->m_dwBytesProcessed = dwBytesProcessed;
							pThread_Scan->m_dwEntryPointLength = dwEntryPointLength;

							pThread_Scan->m_dwDR0 = Context.Dr0;
							pThread_Scan->m_dwDR1 = Context.Dr1;
							pThread_Scan->m_dwDR2 = Context.Dr2;
							pThread_Scan->m_dwDR3 = Context.Dr3;
							
							pThread_Scan->m_dwDR6 = Context.Dr6;
							pThread_Scan->m_dwDR7 = Context.Dr7;

							dwLength = ROUND_UP ( pThread_Scan->m_dwEntryPointLength, sizeof ( DWORD ) );

							dwSize += dwLength;

							dwPacketSize += dwLength;

							pThread_Scan->m_dwThreadFunctionLengthRounded = dwLength;
				
							memcpy ( ( PVOID )&pThread_Scan->m_pbOpcodes,  ( PBYTE )pvAllocatedBase, pThread_Scan->m_dwEntryPointLength );
							
							pdwBufferOut += dwSize / sizeof ( PVOID );

							pMega_Scan_Header->m_dwNumberOfThreadEntries++;
						}
					}
				}
			}

			iCounter++;
		}

		dwSizeRemaining -= sizeof ( Scan_Packet_Process_t );

		pScan_Packet_Process += sizeof ( Scan_Packet_Process_t );

	}while ( dwSizeRemaining != 0 );
	
	if ( NtFreeMemory ( hCurrentProcess, ( DWORD )PMSN, 0, MEM_RELEASE, g_NtStatus ) != 1 )
	{
		SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );
		
		dwPacketSize = 0;
	}
	if ( NtFreeMemory ( hCurrentProcess, ( DWORD )pvAllocatedBase, 0, MEM_RELEASE, g_NtStatus ) != 1 )
	{
		SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );
				
		dwPacketSize = 0;
	}
	if ( dwPacketSize != 0 )
	{
		pScanFooter = ( ScanFooter_t* )pdwBufferOut;

		memset ( &ScanHeader, 0, sizeof ( ScanHeader_t ) );
		memset ( &ScanFooter, 0, sizeof ( ScanFooter_t ) );

		ScanHeader.m_dwPacketSize = dwPacketSize;
		ScanHeader.m_dwCACStatus = 666;

		pPacketBase = ( PVOID )( ( DWORD )pScanHeader + sizeof ( ScanHeader_t ) );

		ScanHeader.m_dwCRCOfPacket = dwCRCBlock ( 0, pPacketBase, dwPacketSize );

		memcpy ( pScanHeader, &ScanHeader, sizeof ( ScanHeader_t ) );

		memset ( &MD5Context, 0, sizeof ( MD5Context_t ) );

		MD5Init ( &MD5Context );

		MD5Update ( &MD5Context, ( PUCHAR )pPacketBase, pScanHeader->m_dwPacketSize );

		MD5Final ( &MD5Context );
		
		memcpy ( ScanFooter.m_dwPacketMD5, MD5Context.digest, sizeof ( MD5Context.digest ) );

		memcpy ( pScanFooter, &ScanFooter, sizeof ( ScanFooter_t ) );

		pdwBufferOut += sizeof ( ScanFooter_t ) / sizeof ( PVOID );

		dwPacketSize = dwPacketSize + ( sizeof ( ScanHeader_t ) + sizeof ( ScanFooter_t ) + sizeof ( Mega_Scan_Header_t ) );
	}
	else
	{
		dwPacketSize = 0;
	}

	return dwPacketSize;
}
//========================================================================================
DWORD __stdcall RunPreVmCode ( PDWORD pdwBufferIn, PDWORD pdwBufferOut )
{
	return 0;
}
//========================================================================================
DWORD __stdcall DefaultResponse ( PDWORD pdwBufferIn, PDWORD pdwBufferOut )
{
	return 0;
}
//========================================================================================
DWORD __stdcall RunPostVmCode ( PDWORD pdwBufferIn, PDWORD pdwBufferOut )
{
	return 0;
}
//========================================================================================
typedef struct
{
	WORD m_wHint;
	DWORD m_dwCRC;
}Import_t;
//========================================================================================
DWORD __stdcall ScanSpecificProcessModules  ( HANDLE hProcessHandle, PSYSTEM_PROCESS_INFORMATION PSPI, PVOID pvStartOfScanPacket, DWORD dwSizeTotal, PDWORD pdwBufferOut )
{
	BOOL bInMisMatchingSequence, bReturn;

	BOOL bInheritHandle;
	
	CLIENT_ID CID;

	CONTEXT Context;

	DWORD dwBytesProcessed, dwBytesRead, dwBufferSize, dwCRC, dwFunctionLength, dwPacketSize, dwRoundedLength, dwSizeOfPacket, dwSizeRemaining, dwThreadStart;
	
	DWORD dwBase, dwEnd, dwSectionSize, dwSize, dwImport, dwModuleNameHash;

	DWORD dwBaseData, dwEndData, dwBaseRData, dwEndRData, dwBaseText, dwEndText, dwVTablePointer, dwFunction;

	DWORD Count;
	DWORD Address;
	PUSHORT TypeOffset;
	DWORD Delta;

	HANDLE hCurrentProcess, hThread;

	PIMAGE_THUNK_DATA lpImageThunkData;
	PIMAGE_THUNK_DATA lpOriginalImageThunkData;
	PIMAGE_IMPORT_BY_NAME lpImageImportName;

	int iBytesLeftInPage, iCounter, iImports, iIterator, iLength, iMisMatches, iMisMatchingBytes, iPosition, iReturnCode, iVTablePointersFound;

	HMODULE hModuleToScan;

	MEMORY_BASIC_INFORMATION MBI;

	NTSTATUS status;

	OBJECT_ATTRIBUTES ObjectAttributes;

	PBYTE pbBuffer, pbFile, pbMemory, pbName;

	PDWORD pdwImportsIn, pdwImportsOut, pdwImportsModule, pdwDataBuffer;

	PMEMORY_SECTION_NAME PMSN;

	PIMAGE_BASE_RELOCATION RelocationDir;

	Scan_Packet_Specific_Module_t* pScan_Specific_Module;

	wchar_t wszModuleName [ 64 ];

	pScan_Specific_Module = ( Scan_Packet_Specific_Module_t* )pvStartOfScanPacket;

	dwSizeRemaining = dwSizeTotal;

	hCurrentProcess = GetCurrentProcess();

	Import_t* pImport;
	VTable_Entry_t* pVTable_Entry;
	VTable_MD5_t* pVTable_MD5;

	ScanHeader_t* pScanHeader;
	ScanFooter_t* pScanFooter;

	ScanHeader_t ScanHeader;
	ScanFooter_t ScanFooter;

	Specfic_Scan_Information_t* pSpecific_Scan_Entry;
	B_P_ScanEntry_t BP_Entry;
	B_P_ScanEntry_t* pBP_Entry;
	IAT_ScanEntry_t* pIAT_ScanEntry;
	Thread_Scan_t* pThread_Scan;

	PIMAGE_DOS_HEADER lpImageDosHeader;
	PIMAGE_NT_HEADERS lpImageNtHeaders;
	PIMAGE_IMPORT_DESCRIPTOR lpImageImportDescriptor;

	dwSize = 0;

	dwSizeOfPacket = 0;

	iCounter = 0;
	iImports = 0;
	iMisMatches = 0;

	PVOID pPacketBase;

	dwPacketSize = 0;

	pScanHeader = ( ScanHeader_t* )pdwBufferOut;

	pdwBufferOut += sizeof ( ScanHeader_t ) / sizeof ( PVOID );

	memset ( &ScanHeader, 0, sizeof ( ScanHeader_t ) );
	memset ( &ScanFooter, 0, sizeof ( ScanFooter_t ) );
	
	memset ( &MBI, 0, sizeof ( MEMORY_BASIC_INFORMATION ) );

	memset ( &ScanHeader, 0, sizeof ( ScanHeader_t ) );
	memset ( &ScanFooter, 0, sizeof ( ScanFooter_t ) );
	memset ( &Context, 0, sizeof ( CONTEXT ) );

	PMSN = ( PMEMORY_SECTION_NAME )NtAllocateMemory ( hCurrentProcess, 0, g_dwSizeOfPage, PAGE_READWRITE, g_NtStatus );

	if ( PMSN == NULL )
	{
		SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		return 0;
	}
	
	pdwImportsIn = ( PDWORD )NtAllocateMemory ( hCurrentProcess, 0, g_dwSizeOfPage * 2, PAGE_READWRITE, g_NtStatus );

	if ( pdwImportsIn == NULL )
	{
		SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		return 0;
	}

	pdwImportsOut = ( PDWORD )NtAllocateMemory ( hCurrentProcess, 0, g_dwSizeOfPage * 2, PAGE_READWRITE, g_NtStatus );

	if ( pdwImportsOut == NULL )
	{
		SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		return 0;
	}

	pdwImportsModule = ( PDWORD )NtAllocateMemory ( hCurrentProcess, 0, g_dwSizeOfPage * 2, PAGE_READWRITE, g_NtStatus );

	if ( pdwImportsModule == NULL )
	{
		SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		return 0;
	}

	pImport = ( Import_t* )NtAllocateMemory ( hCurrentProcess, 0, g_dwSizeOfPage * 2, PAGE_READWRITE, g_NtStatus );

	if ( pImport == NULL )
	{
		SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		return 0;
	}

	pbBuffer = ( PBYTE )NtAllocateMemory ( hCurrentProcess, 0, g_dwSizeOfPage * 2, PAGE_READWRITE, g_NtStatus );

	if ( pbBuffer == NULL )
	{
		SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		return 0;
	}
/*
	pVTable = ( VTable_Entry_t* )NtAllocateMemory ( hCurrentProcess, 0, g_dwSizeOfPage * 8, PAGE_READWRITE, g_NtStatus );

	if ( pVTable == NULL )
	{
		SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

		return 0;
	}
*/
	do
	{
		dwCRC = dwCRCBlock ( 0, PSPI->ProcessName.Buffer, PSPI->ProcessName.Length );

		if ( pScan_Specific_Module->m_dwProcessID != PSPI->ProcessId )
		{
			dwPacketSize = 0;

			break;
		}

		if ( pScan_Specific_Module->m_dwProcessNameHash != dwCRC )
		{
			dwPacketSize = 0;

			break;
		}

		hModuleToScan = GetModuleHandleEx ( hCurrentProcess, hProcessHandle, pScan_Specific_Module->m_dwModuleNameHash );

		if ( hModuleToScan == 0  )
		{
			dwPacketSize = 0;

			break;
		}

		pSpecific_Scan_Entry = ( Specfic_Scan_Information_t* )pdwBufferOut;

		memset ( pSpecific_Scan_Entry, 0, sizeof ( Specfic_Scan_Information_t ) );

		status = NtQueryVirtualMemory ( hProcessHandle, ( PVOID )hModuleToScan, MemoryBasicInformation, &MBI, sizeof ( MEMORY_BASIC_INFORMATION ), 0 );

		if ( NT_SUCCESS ( status ) )
		{
			status = NtQueryVirtualMemory ( hProcessHandle, MBI.BaseAddress, MemorySectionName, PMSN, g_dwSizeOfPage, 0 );
				
			if ( NT_SUCCESS ( status ) )
			{
				iReturnCode = AcquireSectionNameAndMD5 ( hProcessHandle, PSPI->ProcessId, MD5Context, ( DWORD )MBI.BaseAddress, ( DWORD )MBI.RegionSize, Module, PMSN );
												
				if ( iReturnCode == 1 )
				{							
					dwSize = ScanImageSection ( hProcessHandle, MD5Context, Module, PSPI, PMSN, ( PDWORD )&pSpecific_Scan_Entry->Generic_Scan_Information );

					if ( dwSize != 0 )
					{
						pdwBufferOut += sizeof ( Specfic_Scan_Information_t ) / sizeof ( PVOID );

						dwPacketSize += sizeof ( Specfic_Scan_Information_t );
					}
				}
			}
		}
						
		do
		{
			CID.UniqueProcess = PSPI->ProcessId;
			CID.UniqueThread = PSPI->Threads [ iCounter ].ClientId.UniqueThread;

			bInheritHandle = FALSE;

			InitializeObjectAttributes ( &ObjectAttributes, NULL, ( bInheritHandle ? OBJ_INHERIT : 0 ), NULL, NULL );

			status = NtOpenThread ( &hThread, THREAD_GET_CONTEXT | THREAD_QUERY_INFORMATION | THREAD_SET_INFORMATION | THREAD_SET_CONTEXT | THREAD_SUSPEND_RESUME, &ObjectAttributes, &CID );

			if ( NT_SUCCESS ( status ) )
			{
				status = NtQueryInformationThread ( hThread, ThreadQuerySetWin32StartAddress, &dwThreadStart, sizeof ( DWORD ), NULL );
						
				if ( NT_SUCCESS ( status ) )
				{
					status = NtReadVirtualMemory ( hProcessHandle, ( PVOID )dwThreadStart, pbBuffer, g_dwSizeOfPage, &dwBytesRead );

					if ( NT_SUCCESS ( status ) )
					{
						dwPacketSize += sizeof ( Thread_Scan_t );

						pThread_Scan = ( Thread_Scan_t* )pdwBufferOut;

						pThread_Scan->m_dwProcessID = PSPI->ProcessId;
						pThread_Scan->m_dwThreadID = PSPI->Threads [ iCounter ].ClientId.UniqueThread;
						
						dwBytesProcessed = 0;
						dwFunctionLength = 0;

						pThread_Scan->m_dwEntryPoint = dwThreadStart;

						status = NtQueryVirtualMemory ( hProcessHandle, ( PVOID )dwThreadStart, MemoryBasicInformation, &MBI, sizeof ( MEMORY_BASIC_INFORMATION ), 0 );

						if ( NT_SUCCESS ( status ) )
						{
							pThread_Scan->m_dwProtection = MBI.Protect;
						}

//						pThread_Scan->m_dwEntryPointCRC = dwCRCFunction ( ( DWORD )pbBuffer, g_dwSizeOfPage, dwBytesProcessed, dwFunctionLength ); // FIXME: make sure method doesn't exceed region size
						
						dwFunctionLength = MD5Function ( ( DWORD )pbBuffer, g_dwSizeOfPage, dwBytesProcessed, ( PBYTE )pThread_Scan->m_dwMD5 );

						pThread_Scan->m_dwBytesProcessed = dwBytesProcessed;
						pThread_Scan->m_dwEntryPointLength = dwFunctionLength;

						Context.ContextFlags = CONTEXT_DEBUG_REGISTERS;

						status = NtGetContextThread ( hThread, &Context );

						if ( NT_SUCCESS ( status ) )
						{
							pThread_Scan->m_dwDR0 = Context.Dr0;
							pThread_Scan->m_dwDR1 = Context.Dr1;
							pThread_Scan->m_dwDR2 = Context.Dr2;
							pThread_Scan->m_dwDR3 = Context.Dr3;
							
							pThread_Scan->m_dwDR6 = Context.Dr6;
							pThread_Scan->m_dwDR7 = Context.Dr7;
						}

						pSpecific_Scan_Entry->Detail_Scan.m_dwNumberOfThreadEntries++;

						pdwBufferOut += sizeof ( Thread_Scan_t ) / sizeof ( PVOID );
					}
				}
				
				iCounter++;
			}
		}while ( iCounter < PSPI->ThreadCount );

		pbFile = ( PBYTE )g_pvFileAllocatedBase;
		pbMemory = ( PBYTE )g_pvMemoryAllocatedBase;
						
		for ( iIterator = 0; iIterator < Module.m_wNumberOfSections; iIterator++ )
		{
			if ( Module.Section_Information [ iIterator ].m_dwCharacteristics & IMAGE_SCN_MEM_EXECUTE
				|| Module.Section_Information [ iIterator ].m_dwCharacteristics & IMAGE_SCN_CNT_CODE )
			{
				dwBase = Module.Section_Information [ iIterator ].m_dwVirtualAddress;
				dwSize = Module.Section_Information [ iIterator ].m_dwSize;

				
//		IMAGE_SCN_MEM_EXECUTE 
//		IMAGE_SCN_CNT_CODE
					
				bInMisMatchingSequence = 0;

				dwBufferSize = 0;

				for ( iIterator = dwBase; iIterator < dwSize; iIterator++ ) 
				{
					if ( pbFile [ iIterator ] != pbMemory [ iIterator ] )
					{
						bInMisMatchingSequence = 1;
					}
					if ( bInMisMatchingSequence == 1 )
					{
						iBytesLeftInPage = 0;

						iMisMatchingBytes = 0;

						iCounter = 0;

						iBytesLeftInPage = ROUND_UP ( iIterator, g_dwSizeOfPage );

						iBytesLeftInPage -= iIterator;

						while ( iCounter < iBytesLeftInPage )
						{
							if ( pbFile [ iIterator + iCounter ] != pbMemory [ iIterator + iCounter ] )
							{
								iMisMatchingBytes++;
							}
							else
							{
								if ( iMisMatchingBytes != 0 )
								{
									// create entry

									dwBufferSize = sizeof ( B_P_ScanEntry_t );

									pSpecific_Scan_Entry->Detail_Scan.m_dwNumberOfBytePatchEntries++;
																								
									dwRoundedLength = ROUND_UP ( iMisMatchingBytes, sizeof ( DWORD ) );

									pBP_Entry = ( B_P_ScanEntry_t* )pdwBufferOut;

									pBP_Entry->m_dwNumberOfBytesPatched = iMisMatchingBytes;
									pBP_Entry->m_dwRoundedLength = dwRoundedLength;
									pBP_Entry->m_dwRVA = /*pSpecific_Scan_Entry->Generic_Scan_Information.m_dwModuleBase + */iIterator + iCounter - iMisMatchingBytes;

									memcpy ( ( PVOID )&pBP_Entry->m_pbModifiedOpcodes, ( const void* )&pbMemory [ iIterator + iCounter - iMisMatchingBytes ], iMisMatchingBytes );

									dwBufferSize += dwRoundedLength;
																														
									dwPacketSize += dwBufferSize;
																
									pdwBufferOut += dwBufferSize / sizeof ( PVOID );
						
									iMisMatchingBytes = 0;
								}
							}

							iCounter++;
						}

						iIterator += iBytesLeftInPage;

						bInMisMatchingSequence = 0;
					}
				}
			}
		}
				
		lpImageDosHeader = ( PIMAGE_DOS_HEADER  )( DWORD )g_pvMemoryAllocatedBase;

		if ( lpImageDosHeader->e_magic == IMAGE_DOS_SIGNATURE )
		{
			lpImageNtHeaders = ( PIMAGE_NT_HEADERS ) ( ( DWORD )g_pvMemoryAllocatedBase + lpImageDosHeader->e_lfanew );

			if ( lpImageNtHeaders->Signature == IMAGE_NT_SIGNATURE )
			{
				lpImageImportDescriptor = ( PIMAGE_IMPORT_DESCRIPTOR )( ( DWORD )g_pvMemoryAllocatedBase +
					lpImageNtHeaders->OptionalHeader.DataDirectory [ IMAGE_DIRECTORY_ENTRY_IMPORT ].VirtualAddress );

				while ( lpImageImportDescriptor->Name != 0 )
				{
					pbName = ( PBYTE )( ( DWORD )g_pvMemoryAllocatedBase + lpImageImportDescriptor->Name );

					iMisMatches = 0;

					if ( AsciiToUnicode ( ( const char* )pbName, wszModuleName ) )
					{
						memset ( pdwImportsOut, 0, g_dwSizeOfPage * 2 );
						memset ( pdwImportsModule, 0, g_dwSizeOfPage * 2 );
						memset ( pdwImportsIn, 0, g_dwSizeOfPage * 2 );
						memset ( pImport, 0, g_dwSizeOfPage * 2 );
						
						iLength = wstrlen ( wszModuleName );
						
						pbName = ( PBYTE )wszModuleName;

						for ( iPosition = 0; iPosition < iLength; iPosition++ )
						{
							if ( pbName [ iPosition ] >= 'A' && pbName [ iPosition ] <= 'Z' ) // lowercase
								pbName [ iPosition ] |= ' ';
						}
																		
						iIterator = 0;
						iImports = 0;

						pdwImportsIn [ iIterator ] = BLOCK_BEGIN;

						iIterator++;

						pdwImportsIn [ iIterator ] = BLOCK_BEGIN_1;
											
						iIterator++;
												
						dwCRC = dwCRCBlock ( 0, pbName, iLength );

						pdwImportsIn [ iIterator ] = dwCRC;

						dwModuleNameHash = dwCRC;

						hModuleToScan = GetModuleHandleEx ( hCurrentProcess, hProcessHandle, dwCRC );
											
						iIterator++;
																								
						if ( lpImageImportDescriptor->FirstThunk )
						{
							lpImageThunkData = ( PIMAGE_THUNK_DATA )( ( DWORD )g_pvMemoryAllocatedBase + lpImageImportDescriptor->FirstThunk ); 
						
							lpOriginalImageThunkData = ( PIMAGE_THUNK_DATA )( ( DWORD )g_pvMemoryAllocatedBase + lpImageImportDescriptor->DUMMYUNIONNAME.OriginalFirstThunk ); 

							while ( lpImageThunkData->u1.Function != 0 )
							{
								lpImageImportName = ( PIMAGE_IMPORT_BY_NAME )( ( DWORD )g_pvMemoryAllocatedBase + lpOriginalImageThunkData->u1.AddressOfData );

								pbName = ( PBYTE )lpImageImportName->Name;
								
								iLength = strlen ( ( const char* )pbName );
																														
								dwCRC = dwCRCBlock ( 0, pbName, iLength );
								
								pImport [ iImports ].m_dwCRC = dwCRC;
								pImport [ iImports ].m_wHint = lpImageImportName->Hint;
								
								pdwImportsModule [ iImports ] = lpImageThunkData->u1.Function;
																
								iIterator++;

								iImports++;
								
								lpOriginalImageThunkData++;
								lpImageThunkData++;
							}
						}
												
						_quicksort ( pImport, iImports, sizeof ( Import_t ), compare );

						for ( iCounter = 0; iCounter < iImports; iCounter++ )
						{
							pdwImportsIn [ iCounter + 3 ] = pImport [ iCounter ].m_dwCRC;
						}
						
						pdwImportsIn [ iIterator ] = BLOCK_END;

						iIterator++;

						pdwImportsIn [ iIterator ] = BLOCK_END_1;

						iIterator++;
					
						bReturn = ResolveImportsEx ( hProcessHandle, pdwImportsIn, iIterator, pdwImportsOut );
						
						if ( bReturn != 0 )
						{
							_quicksort ( pdwImportsModule, iImports, sizeof ( DWORD ), compare );
							_quicksort ( pdwImportsOut, iImports, sizeof ( DWORD ), compare ); // resolved table

							// scan imports
							
							for ( iCounter = 0; iCounter < iImports; iCounter++ )
							{								
								if ( pdwImportsOut [ iCounter ] != pdwImportsModule [ iCounter ] )
								{
									// mismatch notate it

									iMisMatches++;
								}
							}

							// so we know the import table has some mismatch(es), walk it and figure out
							// who hooked and why

							if ( iMisMatches > 0 )
							{
								lpImageThunkData = ( PIMAGE_THUNK_DATA )( ( DWORD )g_pvMemoryAllocatedBase + lpImageImportDescriptor->FirstThunk ); 
						
								lpOriginalImageThunkData = ( PIMAGE_THUNK_DATA )( ( DWORD )g_pvMemoryAllocatedBase + lpImageImportDescriptor->DUMMYUNIONNAME.OriginalFirstThunk ); 

								while ( lpImageThunkData->u1.Function != 0 )
								{
									lpImageImportName = ( PIMAGE_IMPORT_BY_NAME )( ( DWORD )g_pvMemoryAllocatedBase + lpOriginalImageThunkData->u1.AddressOfData );

									pbName = ( PBYTE )lpImageImportName->Name;
								
									iLength = strlen ( ( const char* )pbName );
																														
									dwCRC = dwCRCBlock ( 0, pbName, iLength );
									
									dwImport = ResolveSingleImport ( hProcessHandle, dwModuleNameHash, dwCRC );
																										
									if ( lpImageThunkData->u1.Function != dwImport ) // check lpImageThunkData->u1.Function, because it could be forwarded
									{
										// found the mismatch
										// prepare patcket

										pIAT_ScanEntry = ( IAT_ScanEntry_t* )pdwBufferOut;

										memset ( pIAT_ScanEntry->m_bName, 0, sizeof ( pIAT_ScanEntry->m_bName ) );
										memset ( pIAT_ScanEntry->m_wszModuleName, 0, sizeof ( pIAT_ScanEntry->m_wszModuleName ) );

										pbName = ( PBYTE )lpImageImportName->Name;
								
										iLength = strlen ( ( const char* )pbName );
																														
										dwCRC = dwCRCBlock ( 0, pbName, iLength );
										
										memcpy ( pIAT_ScanEntry->m_bName, pbName, iLength );

										iLength = wstrlen ( wszModuleName );

										memcpy ( pIAT_ScanEntry->m_wszModuleName, wszModuleName, iLength );

										pIAT_ScanEntry->m_dwResolved = dwImport;

										pIAT_ScanEntry->m_dwHookedAddress = lpImageThunkData->u1.Function;

										pIAT_ScanEntry->m_dwImportModule = ( DWORD )hModuleToScan;

										pIAT_ScanEntry->m_dwRVA = ( DWORD )&lpImageThunkData->u1.AddressOfData - ( DWORD )g_pvMemoryAllocatedBase;

										status = NtReadVirtualMemory ( hProcessHandle, ( PVOID )lpImageThunkData->u1.Function, pbBuffer, g_dwSizeOfPage, &dwBytesRead );

										if ( NT_SUCCESS ( status ) )
										{
											pIAT_ScanEntry->m_dwCRCOfHook = dwCRCFunction ( ( DWORD )pbBuffer, g_dwSizeOfPage, dwBytesProcessed, dwFunctionLength );
										}

										pSpecific_Scan_Entry->Detail_Scan.m_dwNumberOfHookedImports++;

										pdwBufferOut += sizeof ( IAT_ScanEntry_t ) / sizeof ( PVOID );
									}

									iIterator++;

									iImports++;
								
									lpOriginalImageThunkData++;
									lpImageThunkData++;
									
									lpImageImportName++;
								}
							}
						}
					}
										
					lpImageImportDescriptor++;
				}
			}
		}
				
		// now scan for hooks to vtables in .rdata

		// start by zeroing all imports

		pVTable_MD5 = ( VTable_MD5_t* )pdwBufferOut;

		memset ( &MD5Context, 0, sizeof ( MD5Context_t ) );

		lpImageDosHeader = ( PIMAGE_DOS_HEADER  )( DWORD )g_pvMemoryAllocatedBase;

		if ( lpImageDosHeader->e_magic == IMAGE_DOS_SIGNATURE )
		{
			lpImageNtHeaders = ( PIMAGE_NT_HEADERS ) ( ( DWORD )g_pvMemoryAllocatedBase + lpImageDosHeader->e_lfanew );

			if ( lpImageNtHeaders->Signature == IMAGE_NT_SIGNATURE )
			{
				lpImageImportDescriptor = ( PIMAGE_IMPORT_DESCRIPTOR )( ( DWORD )g_pvMemoryAllocatedBase +
					lpImageNtHeaders->OptionalHeader.DataDirectory [ IMAGE_DIRECTORY_ENTRY_IMPORT ].VirtualAddress );

				while ( lpImageImportDescriptor->Name != 0 )
				{
					if ( lpImageImportDescriptor->FirstThunk )
					{
						lpImageThunkData = ( PIMAGE_THUNK_DATA )( ( DWORD )g_pvMemoryAllocatedBase + lpImageImportDescriptor->FirstThunk ); 
						
						lpOriginalImageThunkData = ( PIMAGE_THUNK_DATA )( ( DWORD )g_pvMemoryAllocatedBase + lpImageImportDescriptor->DUMMYUNIONNAME.OriginalFirstThunk ); 

						while ( lpImageThunkData->u1.Function != 0 )
						{
							lpImageThunkData->u1.Function = 0;

							lpOriginalImageThunkData++;
							lpImageThunkData++;
						}
					}

					lpImageImportDescriptor++;
				}
	
				RelocationDir = ( PIMAGE_BASE_RELOCATION )( ( DWORD )g_pvMemoryAllocatedBase + lpImageNtHeaders->OptionalHeader.DataDirectory [ IMAGE_DIRECTORY_ENTRY_BASERELOC ].VirtualAddress );
		
				Delta = Module.m_dwImageBase - Module.m_dwModuleBase;

				if ( RelocationDir != 0 )
				{			
					for ( iIterator = 0; iIterator < Module.m_wNumberOfSections; iIterator++ ) 
					{
						if ( Module.Section_Information [ iIterator ].m_dwSectionNameHash == SECTION_NAME_HASH_TEXT )
						{
							dwBaseText = ( DWORD )g_pvMemoryAllocatedBase + Module.Section_Information [ iIterator ].m_dwVirtualAddress;
							dwEndText = dwBaseText + Module.Section_Information [ iIterator ].m_dwSize;
						}
						else if ( Module.Section_Information [ iIterator ].m_dwSectionNameHash == SECTION_NAME_HASH_RDATA )
						{
							dwBaseRData = ( DWORD )g_pvMemoryAllocatedBase + Module.Section_Information [ iIterator ].m_dwVirtualAddress;
							dwEndRData = dwBaseRData + Module.Section_Information [ iIterator ].m_dwSize;
						}
						else if ( Module.Section_Information [ iIterator ].m_dwSectionNameHash == SECTION_NAME_HASH_DATA )
						{
							dwBaseData = ( DWORD )g_pvMemoryAllocatedBase + Module.Section_Information [ iIterator ].m_dwVirtualAddress;
							dwEndData = dwBaseData + Module.Section_Information [ iIterator ].m_dwSize;
						}

					}

					dwSize = 0;
					dwSectionSize = 0;

					dwSectionSize = lpImageNtHeaders->OptionalHeader.DataDirectory [ IMAGE_DIRECTORY_ENTRY_BASERELOC ].Size;

					memset ( &MD5Context, 0, sizeof ( MD5Context_t ) );

					MD5Init ( &MD5Context );
			
					while ( RelocationDir->SizeOfBlock > 0 )
					{
						Count = ( ( RelocationDir->SizeOfBlock ) - sizeof ( IMAGE_BASE_RELOCATION ) ) / sizeof ( USHORT );
						Address = ( DWORD )( ( DWORD )g_pvMemoryAllocatedBase + RelocationDir->VirtualAddress );
						TypeOffset = ( PUSHORT )( RelocationDir + 1 );

						RelocationDir = ScanVTables ( MD5Context, dwBaseRData, dwEndRData, Address,
							Count,
							TypeOffset,
							Delta );

						if ( RelocationDir == NULL || dwSize >= dwSectionSize )
						{
							break;
						}

						dwSize += RelocationDir->SizeOfBlock;
					}
		
					MD5Final ( &MD5Context );
				}
			}
		}
				
		memcpy ( pVTable_MD5->m_dwMemoryMD5, MD5Context.buf, sizeof ( MD5Context.buf ) );

		memset ( &MD5Context, 0, sizeof ( MD5Context_t ) );

		lpImageDosHeader = ( PIMAGE_DOS_HEADER  )( DWORD )g_pvFileAllocatedBase;

		if ( lpImageDosHeader->e_magic == IMAGE_DOS_SIGNATURE )
		{
			lpImageNtHeaders = ( PIMAGE_NT_HEADERS ) ( ( DWORD )g_pvFileAllocatedBase + lpImageDosHeader->e_lfanew );

			if ( lpImageNtHeaders->Signature == IMAGE_NT_SIGNATURE )
			{
				lpImageImportDescriptor = ( PIMAGE_IMPORT_DESCRIPTOR )( ( DWORD )g_pvFileAllocatedBase +
					lpImageNtHeaders->OptionalHeader.DataDirectory [ IMAGE_DIRECTORY_ENTRY_IMPORT ].VirtualAddress );

				while ( lpImageImportDescriptor->Name != 0 )
				{
					if ( lpImageImportDescriptor->FirstThunk )
					{
						lpImageThunkData = ( PIMAGE_THUNK_DATA )( ( DWORD )g_pvFileAllocatedBase + lpImageImportDescriptor->FirstThunk ); 
						
						lpOriginalImageThunkData = ( PIMAGE_THUNK_DATA )( ( DWORD )g_pvFileAllocatedBase + lpImageImportDescriptor->DUMMYUNIONNAME.OriginalFirstThunk ); 

						while ( lpImageThunkData->u1.Function != 0 )
						{
							lpImageThunkData->u1.Function = 0;

							lpOriginalImageThunkData++;
							lpImageThunkData++;
						}
					}

					lpImageImportDescriptor++;
				}
			
				RelocationDir = ( PIMAGE_BASE_RELOCATION )( ( DWORD )g_pvFileAllocatedBase + lpImageNtHeaders->OptionalHeader.DataDirectory [ IMAGE_DIRECTORY_ENTRY_BASERELOC ].VirtualAddress );
		
				Delta = Module.m_dwImageBase - Module.m_dwModuleBase;

				if ( RelocationDir != 0 )
				{			
					for ( iIterator = 0; iIterator < Module.m_wNumberOfSections; iIterator++ ) 
					{
						if ( Module.Section_Information [ iIterator ].m_dwSectionNameHash == SECTION_NAME_HASH_TEXT )
						{
							dwBaseText = ( DWORD )g_pvFileAllocatedBase + Module.Section_Information [ iIterator ].m_dwVirtualAddress;
							dwEndText = dwBaseText + Module.Section_Information [ iIterator ].m_dwSize;
						}
						else if ( Module.Section_Information [ iIterator ].m_dwSectionNameHash == SECTION_NAME_HASH_RDATA )
						{
							dwBaseRData =( DWORD )g_pvFileAllocatedBase + Module.Section_Information [ iIterator ].m_dwVirtualAddress;
							dwEndRData = dwBaseRData + Module.Section_Information [ iIterator ].m_dwSize;
						}
						else if ( Module.Section_Information [ iIterator ].m_dwSectionNameHash == SECTION_NAME_HASH_DATA )
						{
							dwBaseData = ( DWORD )g_pvFileAllocatedBase + Module.Section_Information [ iIterator ].m_dwVirtualAddress;
							dwEndData = dwBaseData + Module.Section_Information [ iIterator ].m_dwSize;
						}

					}

					dwSize = 0;
					dwSectionSize = 0;

					dwSectionSize = lpImageNtHeaders->OptionalHeader.DataDirectory [ IMAGE_DIRECTORY_ENTRY_BASERELOC ].Size;

					memset ( &MD5Context, 0, sizeof ( MD5Context_t ) );

					MD5Init ( &MD5Context );
			
					while ( RelocationDir->SizeOfBlock > 0 )
					{
						Count = ( ( RelocationDir->SizeOfBlock ) - sizeof ( IMAGE_BASE_RELOCATION ) ) / sizeof ( USHORT );
						Address = ( DWORD )( ( DWORD )g_pvFileAllocatedBase + RelocationDir->VirtualAddress );
						TypeOffset = ( PUSHORT )( RelocationDir + 1 );

						RelocationDir = ScanVTables ( MD5Context, dwBaseRData, dwEndRData, Address,
							Count,
							TypeOffset,
							Delta );

						if ( RelocationDir == NULL || dwSize >= dwSectionSize )
						{
							break;
						}

						dwSize += RelocationDir->SizeOfBlock;
					}
		
					MD5Final ( &MD5Context );
				}
			}
		}

		memcpy ( pVTable_MD5->m_dwFileMD5, MD5Context.buf, sizeof ( MD5Context.buf ) );

		pdwBufferOut += sizeof ( VTable_MD5_t ) / sizeof ( PVOID );

		for ( iIterator = 0; iIterator < Module.m_wNumberOfSections; iIterator++ )
		{
			if ( Module.Section_Information [ iIterator ].m_dwSectionNameHash == SECTION_NAME_HASH_RDATA )
			{
				dwBase = Module.Section_Information [ iIterator ].m_dwVirtualAddress;
				dwSize = Module.Section_Information [ iIterator ].m_dwSize;

				break;
			}
		}

		

		if ( pVTable_MD5->m_dwFileMD5 [ 0 ] != pVTable_MD5->m_dwMemoryMD5 [ 0 ]
			|| pVTable_MD5->m_dwFileMD5 [ 1 ] != pVTable_MD5->m_dwMemoryMD5 [ 1 ]
			|| pVTable_MD5->m_dwFileMD5 [ 2 ] != pVTable_MD5->m_dwMemoryMD5 [ 2 ]
			|| pVTable_MD5->m_dwFileMD5 [ 3 ] != pVTable_MD5->m_dwMemoryMD5 [ 3 ] )
		{
			pbFile = ( PBYTE )g_pvFileAllocatedBase;
			pbMemory = ( PBYTE )g_pvMemoryAllocatedBase;
	
			bInMisMatchingSequence = 0;

			dwBufferSize = 0;

			for ( iIterator = dwBase; iIterator < dwSize; iIterator++ ) 
			{				
				if ( pbFile [ iIterator ] != pbMemory [ iIterator ] )
				{
					bInMisMatchingSequence = 1;
				}
				if ( bInMisMatchingSequence == 1 )
				{
					iBytesLeftInPage = 0;

					iMisMatchingBytes = 0;

					iCounter = 0;

					iBytesLeftInPage = ROUND_UP ( iIterator, g_dwSizeOfPage );

					iBytesLeftInPage -= iIterator;

					while ( iCounter < iBytesLeftInPage )
					{
						if ( pbFile [ iIterator + iCounter ] != pbMemory [ iIterator + iCounter ] )
						{
							iMisMatchingBytes++;
						}
						else
						{
							if ( iMisMatchingBytes != 0 )
							{
								// create entry
																						
								iMisMatchingBytes = 0;
							}
						}

						iCounter++;
					}

					iIterator += iBytesLeftInPage;

					bInMisMatchingSequence = 0;
				}
			}

		}

		
		
		// store memory MD5 of rdata vtables, then check file md5

//		VTable_Entry

//		dwBase = dwBaseData;
//		dwSize = dwEndData;

//		 dwVTablePointer, dwFunction
/*
		iVTablePointersFound = 0;

		while ( dwBase < dwSize )
		{
			status = NtReadVirtualMemory ( hProcessHandle, ( PVOID )dwBase, &dwVTablePointer, sizeof ( DWORD ), &dwBytesRead );
		
			if ( NT_SUCCESS ( status ) )
			{
				if ( dwVTablePointer >= dwBaseRData && dwVTablePointer <= dwEndRData )
				{
					status = NtReadVirtualMemory ( hProcessHandle, ( PVOID )dwVTablePointer, &dwFunction, sizeof ( DWORD ), &dwBytesRead );
		
					if ( NT_SUCCESS ( status ) )
					{
						if ( dwFunction >= dwBaseText && dwFunction <= dwEndText )
						{
							pVTable [ iVTablePointersFound ].m_dwPointer = dwVTablePointer;
							pVTable [ iVTablePointersFound ].m_dwVirtualAddress = dwBase;

							iVTablePointersFound++;
						}
					}
				}
			}

			dwBase += sizeof ( DWORD );
		}

		Delta = Module.m_dwImageBase - Module.m_dwModuleBase;

		pOrigVTable = pVTable;

		RelocationDir = ( PIMAGE_BASE_RELOCATION )( ( DWORD )g_pvMemoryAllocatedBase + lpImageNtHeaders->OptionalHeader.DataDirectory [ IMAGE_DIRECTORY_ENTRY_BASERELOC ].VirtualAddress );
		
		if ( RelocationDir != 0 )
		{
			memset ( &MD5Context, 0, sizeof ( MD5Context_t ) );

			MD5Init ( &MD5Context );

			dwSize = 0;

			pdwDataBuffer = ( PDWORD )pVTable;
			
			while ( RelocationDir->SizeOfBlock > 0 )
			{
				Count = ( ( RelocationDir->SizeOfBlock ) - sizeof ( IMAGE_BASE_RELOCATION ) ) / sizeof ( USHORT );
				Address = ( DWORD )( Module.m_dwModuleBase + RelocationDir->VirtualAddress );
				TypeOffset = ( PUSHORT )( RelocationDir + 1 );

				dwBufferSize = 0;

				RelocationDir = ProcessDataSection ( MD5Context, pVTable, dwBufferSize, iVTablePointersFound, Address, Count, TypeOffset, Delta );

				pVTable += dwBufferSize;

				if ( RelocationDir == NULL || dwSize >= dwSectionSize )
				{
					break;
				}

				dwSize += RelocationDir->SizeOfBlock;
			}

			__asm
			{
				INT 3
			}
		
			MD5Final ( &MD5Context );
		}

		pVTable = pOrigVTable;
*/				
		// get the hash of the vtables in rdata on disk

		// now count the vtable pointers in .data, if it points to rdata and has entries in .text
		// remember to cross reference with the .reloc to eliminate spurious entries
		// also store the virtual address, and RVA where is was found

		dwSizeRemaining -= sizeof ( Scan_Packet_Specific_Module_t );

		pScan_Specific_Module += sizeof ( Scan_Packet_Specific_Module_t );

	}while ( dwSizeRemaining != 0 );
	
	if ( NtFreeMemory ( hCurrentProcess, ( DWORD )PMSN, 0, MEM_RELEASE, g_NtStatus ) != 1 )
	{
		SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		dwPacketSize = 0;
	}
	if ( NtFreeMemory ( hCurrentProcess, ( DWORD )pdwImportsIn, 0, MEM_RELEASE, g_NtStatus ) != 1 )
	{
		SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		dwPacketSize = 0;
	}
	if ( NtFreeMemory ( hCurrentProcess, ( DWORD )pdwImportsOut, 0, MEM_RELEASE, g_NtStatus ) != 1 )
	{
		SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

		dwPacketSize = 0;
	}
	if ( NtFreeMemory ( hCurrentProcess, ( DWORD )pdwImportsModule, 0, MEM_RELEASE, g_NtStatus ) != 1 )
	{
		SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		dwPacketSize = 0;
	}
	if ( NtFreeMemory ( hCurrentProcess, ( DWORD )pImport, 0, MEM_RELEASE, g_NtStatus ) != 1 )
	{
		SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		dwPacketSize = 0;
	}
	if ( NtFreeMemory ( hCurrentProcess, ( DWORD )pbBuffer, 0, MEM_RELEASE, g_NtStatus ) != 1 )
	{
		SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

		dwPacketSize = 0;
	}
	if ( dwPacketSize != 0 )
	{
		pScanFooter = ( ScanFooter_t* )pdwBufferOut;

		memset ( &ScanHeader, 0, sizeof ( ScanHeader_t ) );
		memset ( &ScanFooter, 0, sizeof ( ScanFooter_t ) );

		ScanHeader.m_dwPacketSize = dwPacketSize;
		ScanHeader.m_dwCACStatus = 666;

		pPacketBase = ( PVOID )( ( DWORD )pScanHeader + sizeof ( ScanHeader_t ) );

		ScanHeader.m_dwCRCOfPacket = dwCRCBlock ( 0, pPacketBase, dwPacketSize );

		memcpy ( pScanHeader, &ScanHeader, sizeof ( ScanHeader_t ) );

		MD5Init ( &MD5Context );

		MD5Update ( &MD5Context, ( PUCHAR )pPacketBase, pScanHeader->m_dwPacketSize );

		MD5Final ( &MD5Context );
		
		memcpy ( ScanFooter.m_dwPacketMD5, MD5Context.buf, sizeof ( MD5Context.buf ) );

		memcpy ( pScanFooter, &ScanFooter, sizeof ( ScanFooter_t ) );

		pdwBufferOut += sizeof ( ScanFooter_t ) / sizeof ( PVOID );

		dwPacketSize += ( sizeof ( ScanHeader_t ) + sizeof ( ScanFooter_t ) );
	}
	else
	{
		dwPacketSize = 0;
	}

	return dwPacketSize;
/*
	if ( NtFreeMemory ( hCurrentProcess, ( DWORD )pVTable, 0, MEM_RELEASE, g_NtStatus ) != 1 )
	{
		SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

		dwPacketSize = 0;
	}
*/
}
//========================================================================================
DWORD __stdcall ScanHandlesToProcess ( HANDLE hProcessToScan, PSYSTEM_HANDLE_INFORMATION PSHI, PVOID pvStartOfScanPacket, DWORD dwSizeTotal, PDWORD pdwBufferOut )
{
	DWORD dwBase, dwPacketSize, dwSize;

	int iReturnCode;
	
	HANDLE hCurrentProcess;

	return dwPacketSize;
}
//========================================================================================
DWORD __stdcall UpdateProcessList ( PDWORD pdwBufferOut )
{
	BYTE bProcessName [ 128 ];

	BOOL bInheritHandle;

	CLIENT_ID CID;

	DWORD dwLength, dwProcessID, dwPacketSize, dwTEB, dwStatus;

	HANDLE hCurrentProcess, hProcessHandle;

	int iIterator, iPosition, iSavedPosition;

	NTSTATUS status;

	OBJECT_ATTRIBUTES ObjectAttributes;

	PBYTE pbBuffer;

	PVOID pPacketBase;
		
	ScanHeader_t* pScanHeader;
	ScanFooter_t* pScanFooter;

	ScanHeader_t ScanHeader;
	ScanFooter_t ScanFooter;

	Process_Entry_t* pProcess_Entry;
	PROCESS_BASIC_INFORMATION PBI;

	UNICODE_STRING* pProcessFileName;

	dwProcessID = 0;
	dwPacketSize = 0;

	pScanHeader = ( ScanHeader_t* )pdwBufferOut;

	pdwBufferOut += sizeof ( ScanHeader_t ) / sizeof ( PVOID );

	memset ( &ScanHeader, 0, sizeof ( ScanHeader_t ) );
	memset ( &ScanFooter, 0, sizeof ( ScanFooter_t ) );

	dwProcessID = 0;

	hCurrentProcess = GetCurrentProcess();
		
	pProcessFileName = ( UNICODE_STRING* )NtAllocateMemory ( hCurrentProcess, 0, g_dwSizeOfPage, PAGE_READWRITE, g_NtStatus );

	if ( pProcessFileName == NULL )
	{
		SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

		RtlNtStatusToDosError ( g_NtStatus );

		return 0;
	}

	while ( dwProcessID <= 65535 )
	{
		pProcess_Entry = ( Process_Entry_t* )pdwBufferOut;

		CID.UniqueProcess = dwProcessID;
		CID.UniqueThread = 0;
						
		bInheritHandle = FALSE;

		InitializeObjectAttributes ( &ObjectAttributes, NULL, ( bInheritHandle ? OBJ_INHERIT : 0 ), NULL, NULL );

		hProcessHandle = ( HANDLE )-1;

		memset ( pProcess_Entry, 0, sizeof ( Process_Entry_t ) );

		pProcess_Entry->m_dwProcessID = dwProcessID;

		status = NtOpenProcess ( &hProcessHandle, PROCESS_QUERY_INFORMATION, &ObjectAttributes, &CID );

		pProcess_Entry->status = status;

		dwStatus = RtlNtStatusToDosError ( status );

		memset ( &PBI, 0, sizeof ( PROCESS_BASIC_INFORMATION ) );

		if ( NT_SUCCESS ( status ) && dwStatus != ERROR_INVALID_PARAMETER )
		{
			status = NtQueryInformationProcess ( hProcessHandle, ProcessBasicInformation, &PBI, sizeof ( PROCESS_BASIC_INFORMATION ), &dwLength );

			pProcess_Entry->m_dwExitCode = PBI.dwExitStatus;

			if ( NT_SUCCESS ( status ) && PBI.dwExitStatus == STILL_ACTIVE )
			{
				status = NtQueryInformationProcess ( hProcessHandle, ProcessWow64Information, &dwTEB, sizeof ( DWORD ), &dwLength );
							
				if ( NT_SUCCESS ( status ) )
				{
					if ( dwTEB != 0 )
					{
						pProcess_Entry->m_dwFlags |= PROCESS_ENTRY_32BIT;
					}
					else if ( dwTEB == 0 )
					{
						pProcess_Entry->m_dwFlags |= PROCESS_ENTRY_64BIT;
					}
				}

				memset ( bProcessName, 0, sizeof ( bProcessName ) );
				memset ( pProcessFileName, 0, g_dwSizeOfPage );
			
				status = NtQueryInformationProcess ( hProcessHandle, ProcessImageFileName, pProcessFileName, g_dwSizeOfPage, &dwLength );
				
				if ( NT_SUCCESS ( status ) )
				{
					pbBuffer = ( PBYTE )pProcessFileName->Buffer;

					for ( iIterator = 0; iIterator < pProcessFileName->Length; iIterator++ )
					{
						if ( pbBuffer [ iIterator ] == '\\' || pbBuffer [ iIterator ] == '/' ) // keep going until we hit the last '\\' or '/'
						{
							iPosition = iIterator + 2;

							iSavedPosition = iPosition;
						}
					}
					for ( iIterator = 0; iIterator < pProcessFileName->Length; iIterator++ )
					{
						if ( pbBuffer [ iIterator ] >= 'A' && pbBuffer [ iIterator ] <= 'Z' ) // lowercase
						{
							pbBuffer [ iIterator ] |= ' ';
						}
					}

					iSavedPosition /= sizeof ( WCHAR );

					if ( UnicodeToAscii ( ( wchar_t* )&pProcessFileName->Buffer [ iSavedPosition ], ( char* )bProcessName ) )
					{
						memcpy ( pProcess_Entry->m_bProcessName, bProcessName, sizeof ( pProcess_Entry->m_bProcessName ) );
					}
				}
			}

			NtClose ( hProcessHandle );
		}
		else
		{
			pProcess_Entry->m_dwFlags |= PROCESS_ENTRY_INVALID;
		}

		if ( pProcess_Entry->m_dwFlags == 0 )
		{
			pProcess_Entry->m_dwFlags |= PROCESS_ENTRY_UNKNOWN;
		}

		if ( PBI.dwExitStatus == STILL_ACTIVE )
		{
			memcpy ( pdwBufferOut, pProcess_Entry, sizeof ( Process_Entry_t ) );

			pdwBufferOut += sizeof ( Process_Entry_t ) / sizeof ( PVOID );
												
			dwPacketSize += sizeof ( Process_Entry_t );
		}

		dwProcessID += sizeof ( DWORD );
	}

	if ( NtFreeMemory ( hCurrentProcess, ( DWORD )pProcessFileName, 0, MEM_RELEASE, g_NtStatus ) != 1 )
	{
		dwPacketSize = 0;

		RtlNtStatusToDosError ( g_NtStatus );
	}
	if ( dwPacketSize != 0 )
	{
		pScanFooter = ( ScanFooter_t* )pdwBufferOut;

		memset ( &ScanHeader, 0, sizeof ( ScanHeader_t ) );
		memset ( &ScanFooter, 0, sizeof ( ScanFooter_t ) );

		ScanHeader.m_dwPacketSize = dwPacketSize;
		ScanHeader.m_dwCACStatus = 666;

		pPacketBase = ( PVOID )( ( DWORD )pScanHeader + sizeof ( ScanHeader_t ) );

		ScanHeader.m_dwCRCOfPacket = dwCRCBlock ( 0, pPacketBase, dwPacketSize );

		memcpy ( pScanHeader, &ScanHeader, sizeof ( ScanHeader_t ) );

		MD5Init ( &MD5Context );

		MD5Update ( &MD5Context, ( PUCHAR )pPacketBase, pScanHeader->m_dwPacketSize );

		MD5Final ( &MD5Context );
		
		memcpy ( ScanFooter.m_dwPacketMD5, MD5Context.buf, sizeof ( MD5Context.buf ) );

		memcpy ( pScanFooter, &ScanFooter, sizeof ( ScanFooter_t ) );

		pdwBufferOut += sizeof ( ScanFooter_t ) / sizeof ( PVOID );

		dwPacketSize += ( sizeof ( ScanHeader_t ) + sizeof ( ScanFooter_t ) );
	}
	else
	{
		dwPacketSize = 0;
	}

	return dwPacketSize;
}
//========================================================================================
DWORD __stdcall MainScanFunction ( PDWORD pdwBufferIn, PDWORD pdwBufferOut )
{	
	BOOL bInheritHandle;

	CLIENT_ID CID;

	DWORD dwCRC, dwLength, dwProcessID, dwSize, dwSizeRequired, dwTEB;

	HANDLE hProcessHandle;
		
	int iIterator, iScan, iSubScan;

	DWORD dwPacketSize;

	HANDLE hCurrentProcess;
				
	NTSTATUS status;

	OBJECT_ATTRIBUTES ObjectAttributes;

	PVOID pvScanBuffer = NULL;

	PSYSTEM_HANDLE_INFORMATION PSHI;

	PSYSTEM_PROCESS_INFORMATION PSPI;

	POBJECT_NAME_INFORMATION pObjNameInfo;
	POBJECT_TYPE_INFORMATION pObjTypeInfo;
	
	SYSTEM_BASIC_INFORMATION SBI;

	Scan_Packet_t* Scan_Packet;

	SYSTEM_HANDLE_ENTRY handle;

	Scan_Packet = ( Scan_Packet_t* )pdwBufferIn;

	iScan = Scan_Packet->m_dwScanGate & 0xFFFF0000;

	iSubScan = Scan_Packet->m_dwScanGate & 0x0000FFFF;
		
	status = NtQuerySystemInformation ( SystemBasicInformation, &SBI, sizeof ( SYSTEM_BASIC_INFORMATION ), NULL );

	if ( NT_ERROR ( status ) )
	{
		SetStatus ( CORE_SYSTEM_API_FAILURE, status );

		RtlNtStatusToDosError ( status );

		return 0;
	}

	g_dwSizeOfPage = SBI.PhysicalPageSize;

	dwPacketSize = 0;
	dwSize = 0;

	hCurrentProcess = GetCurrentProcess();

	dwProcessID = 0;

	switch ( iScan )
	{
		case SCAN_ALL_USERMODE_PROCESSES_AND_THREADS:
		{
			dwSizeRequired = 0;

			pvScanBuffer = NULL;

			if ( iSubScan == SCAN_UNLISTED_PROCESS )
			{
				dwSize = UpdateProcessList ( pdwBufferOut );

				pdwBufferOut += dwSize / sizeof ( PVOID );
												
				dwPacketSize += dwSize;
			}
			else
			{
				status = NtQuerySystemInformation ( SystemProcessInformation, pvScanBuffer, 0, &dwSizeRequired );

				if ( NT_ERROR ( status ) )
				{
					pvScanBuffer = ( PVOID )NtAllocateMemory ( hCurrentProcess, 0, dwSizeRequired, PAGE_READWRITE, g_NtStatus );
				
					if ( pvScanBuffer == NULL )
					{
						SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

						RtlNtStatusToDosError ( g_NtStatus );

						return 0;
					}

					dwSizeRequired = ROUND_UP ( dwSizeRequired, g_dwSizeOfPage );

					status = NtQuerySystemInformation ( SystemProcessInformation, pvScanBuffer, dwSizeRequired, &dwSizeRequired );

					if ( NT_SUCCESS ( status ) )
					{
						PSPI = ( PSYSTEM_PROCESS_INFORMATION )pvScanBuffer;

						do
						{
							CID.UniqueProcess = ( DWORD )PSPI->ProcessId;
							CID.UniqueThread = 0;
						
							bInheritHandle = FALSE;

							InitializeObjectAttributes ( &ObjectAttributes, NULL, ( bInheritHandle ? OBJ_INHERIT : 0 ), NULL, NULL );

							hProcessHandle = ( HANDLE )-1;

							status = NtOpenProcess ( &hProcessHandle, PROCESS_DUP_HANDLE | PROCESS_QUERY_INFORMATION | PROCESS_SUSPEND_RESUME | PROCESS_VM_OPERATION | PROCESS_VM_READ | PROCESS_VM_WRITE, &ObjectAttributes, &CID );

							if ( NT_SUCCESS ( status ) )
							{
								status = NtQueryInformationProcess ( hProcessHandle, ProcessWow64Information, &dwTEB, sizeof ( DWORD ), &dwLength );

								if ( NT_SUCCESS ( status ) )
								{
//									if ( dwTEB != 0 )
//									{
										switch ( iSubScan )
										{									
											case SCAN_PROCESS_MODULES:
											{												
												dwSize = ScanProcessAndModules ( hProcessHandle, PSPI, Scan_Packet->pvStartOfScanPacketData, Scan_Packet->m_dwSize, pdwBufferOut );
											}
											break;
											
											case SCAN_SPECIFIC_MODULE:
											{
												dwSize = ScanSpecificProcessModules ( hProcessHandle, PSPI, Scan_Packet->pvStartOfScanPacketData, Scan_Packet->m_dwSize, pdwBufferOut );
											}
											break;
										}

										pdwBufferOut += dwSize / sizeof ( PVOID );
												
										dwPacketSize += dwSize;
//									}
								}
							
								NtClose ( hProcessHandle );
							}
							else
							{
								// Try to adjust privledges, then repeat
							}

							
							PSPI = ( SYSTEM_PROCESS_INFORMATION* )( ( DWORD )PSPI + PSPI->NextEntryDelta );

						}while ( PSPI->NextEntryDelta != 0  );
					}
					if ( NtFreeMemory ( hCurrentProcess, ( DWORD )pvScanBuffer, 0, MEM_RELEASE, g_NtStatus ) != 1 )
					{
						SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

						RtlNtStatusToDosError ( g_NtStatus );
					}
				}
			}
		}

		case SCAN_ALL_HANDLES:
		{
			status = NtQuerySystemInformation ( SystemHandleInformation, NULL, 0, &dwSizeRequired );

			if ( NT_ERROR ( status ) )
			{			
				do
				{
					dwSizeRequired = ROUND_UP ( dwSizeRequired, g_dwSizeOfPage );

					dwSizeRequired += g_dwSizeOfPage;
				
					PSHI = ( PSYSTEM_HANDLE_INFORMATION )NtAllocateMemory ( hCurrentProcess, 0, dwSizeRequired, PAGE_READWRITE, g_NtStatus );//AllocateFromHeap ( dwSizeRequired );

					if ( PSHI == NULL )
					{
						SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

						RtlNtStatusToDosError ( g_NtStatus );

						return 0;
					}
						
					status = NtQuerySystemInformation ( SystemHandleInformation, PSHI, dwSizeRequired, &dwSizeRequired );

					if ( NT_ERROR ( status ) )
					{
						if ( NtFreeMemory ( hCurrentProcess, ( DWORD )PSHI, 0, MEM_RELEASE, g_NtStatus ) != 1 )
						{
							SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

							RtlNtStatusToDosError ( g_NtStatus );
							
							return 0;
						}
					}

				}while ( !NT_SUCCESS ( status ) );

				if ( NT_SUCCESS ( status ) )
				{						
					switch ( iSubScan )
					{
						case SCAN_FILES: // open specified process and scan it's handles to open files/directories
						{
//							dwSize = ScanHandlesToProcess ( hProcessHandle, PSHI, Scan_Packet->pvStartOfScanPacketData, Scan_Packet->m_dwSize, pdwBufferOut );
						}
						break;
												
						case SCAN_FOR_HANDLE_TO_GAME:
						{
							dwSize = ScanHandlesToProcess ( hProcessHandle, PSHI, Scan_Packet->pvStartOfScanPacketData, Scan_Packet->m_dwSize, pdwBufferOut );
						}
						break;
								
						default:
						{
	//						dwSize = DefaultResponse ( pdwBufferOut );
						}
						break;
					}
				}
			}
		}
		break;

		case LIST_ALL_USERMODE_PROCESSES:
		{
			do
			{
				dwSizeRequired = ROUND_UP ( dwSizeRequired, g_dwSizeOfPage );

				dwSizeRequired += g_dwSizeOfPage;
				
				PSHI = ( PSYSTEM_HANDLE_INFORMATION )NtAllocateMemory ( hCurrentProcess, 0, dwSizeRequired, PAGE_READWRITE, g_NtStatus );//AllocateFromHeap ( dwSizeRequired );

				if ( PSHI == NULL )
				{
					SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus );

					RtlNtStatusToDosError ( g_NtStatus );

					return 0;
				}
						
				status = NtQuerySystemInformation ( SystemHandleInformation, PSHI, dwSizeRequired, &dwSizeRequired );

				if ( NT_ERROR ( status ) )
				{
					if ( NtFreeMemory ( hCurrentProcess, ( DWORD )PSHI, 0, MEM_RELEASE, g_NtStatus ) != 1 )
					{
						SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

						RtlNtStatusToDosError ( g_NtStatus );
							
						return 0;
					}
				}

			}while ( !NT_SUCCESS ( status ) );

			for ( iIterator = 0; iIterator < PSHI->HandleCount; iIterator++ )
			{
				dwProcessID = PSHI->Handles [ iIterator ].UniqueProcessId;
				
				if ( !( PSHI->Handles [ iIterator ].GrantedAccess & SYNCHRONIZE ) )
				{	
					CID.UniqueProcess = dwProcessID;
					CID.UniqueThread = 0;
						
					bInheritHandle = FALSE;
					
					InitializeObjectAttributes ( &ObjectAttributes, NULL, ( bInheritHandle ? OBJ_INHERIT : 0 ), NULL, NULL );

					hProcessHandle = ( HANDLE )-1;

					status = NtOpenProcess ( &hProcessHandle, PROCESS_DUP_HANDLE | PROCESS_QUERY_INFORMATION | PROCESS_SUSPEND_RESUME | PROCESS_VM_OPERATION | PROCESS_VM_READ | PROCESS_VM_WRITE, &ObjectAttributes, &CID );

					if ( NT_SUCCESS ( status ) )
					{
						continue;
					}
					else
					{
						// hidden process

						RtlNtStatusToDosError ( status );
					}

					NtClose ( hProcessHandle );
				}
			}
			if ( NtFreeMemory ( hCurrentProcess, ( DWORD )PSHI, 0, MEM_RELEASE, g_NtStatus ) != 1 )
			{
				SetStatus ( MEMORY_RELEASE_ERROR, g_NtStatus );

				RtlNtStatusToDosError ( g_NtStatus );
			}
		}
		break;

		default:
		{

		}
		break;
	}
	
	return dwPacketSize;
}
//========================================================================================
DLL_EXPORT DWORD __stdcall RunScan ( PDWORD pdwBufferIn, PDWORD pdwBufferOut )
{
	// DecryptPacket ( pdwBufferIn );

	// MD5PacketIn

	DWORD dwPacketSize;

	if ( g_hProcessHeap == 0 )
		g_hProcessHeap = GetHeap();
			
	dwPacketSize = MainScanFunction ( pdwBufferIn, pdwBufferOut );
	
	return dwPacketSize;
}
//========================================================================================
void __stdcall SetStatus ( DWORD dwStatus, NTSTATUS status )
{
	g_dwEIP = ( DWORD )_ReturnAddress();

	g_dwGlobalStatus |= dwStatus;

	g_dwStoredNtStatus = status;
}
//========================================================================================
DWORD __stdcall GetStatus ( void )
{
	return g_dwGlobalStatus;
}
//========================================================================================
void __stdcall RemoveStatus ( DWORD dwStatus )
{
	g_dwGlobalStatus &= ~dwStatus;
}
//========================================================================================
// Manual work around to allow us to merge mixed code/data segment without generating an error
// thanks to loopnz for this circa 2007, use this to wipe code and data that we no longer need
// you could also move them into header files to be cleaner, note the comments.
// Scroll down for usage.  Note, you can have as many new sections as you need, just change the name
// or use a compiler pre-processor to do it for you
//========================================================================================
// The following 3 lines of code would go into one header file, eg new_section_include.h
//========================================================================================
#pragma comment ( linker, "/SECTION:.ntdll,ERW" )
#pragma comment ( linker, "/SECTION:.merge,ERW" )
#pragma comment ( linker, "/MERGE:.merge=.ntdll" )
//========================================================================================
// The following 4 lines would go into another header file, eg new_section_start.h
//========================================================================================
#pragma bss_seg ( push, ntdll, ".ntdll" )
#pragma code_seg ( push, ntdll, ".merge" )
#pragma const_seg ( push, ntdll, ".ntdll" )
#pragma data_seg ( push, ntdll, ".ntdll" )
//========================================================================================
DWORD Initialize [ 8192 ];
/*
int Initialize ( void ) // this is needed so the linker generates the section with the optimizations I have on, you have to reference code or data otherwise
						// the linker will not generate the section
{
	return 1;
}
*/

//========================================================================================
// Then the following 4 would go into the final header file, eg new_section_end.h
//========================================================================================
#pragma bss_seg ( pop, ntdll )
#pragma code_seg ( pop, ntdll )
#pragma const_seg ( pop, ntdll )
#pragma data_seg ( pop, ntdll )
//========================================================================================
// Usage:
//========================================================================================
// #include "new_section.h"
// #include "new_section_start.h"
// all your code / data you want to go in the new section goes here
// then you do the following after all your code and data
// #include "new_section_end.h"
//========================================================================================
// Doing this will totally encapsulate all the code / data, even if you include header files from a sdk
//========================================================================================

