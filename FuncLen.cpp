//========================================================================================
#include "FuncLen.h"
#include "Memory.h"
#include "qsort.h"
//========================================================================================
#include "hde32.h"
//========================================================================================
#include "RebaseFunction.h"
//========================================================================================
PVOID __stdcall GetFunctionEnd ( PVOID pvFunc );
PVOID __stdcall GetBranchListFromBlock ( PVOID block );
PVOID __stdcall GetBranchAddress ( PBYTE instr );
BOOL __stdcall IsEndPoint ( PBYTE instr, PVOID curblock );
DWORD __stdcall dwGetLongConditionalJumpAddress ( PDWORD pdwAddress );
DWORD __stdcall dwGetCallAddress ( PDWORD pdwAddress );
BOOL __stdcall bIsUnConditionalJump ( PBYTE pbCurrentInstruction );
DWORD __stdcall GetFunctionLength ( PVOID pvFunctionBegin, BOOL bRebaseRelocs, BOOL bFixupRelative, PDWORD pdwFunctionStartOut, DWORD dwRelocateFromBase, DWORD dwRebaseAddress, DWORD dwBufferIn );
//========================================================================================
PDWORD g_pdwBranches = 0;
PDWORD g_pdwInstructions = 0;
PDWORD g_pdwFixedFunction = 0;
PDWORD g_pdwCopiedInstructions = 0;
//========================================================================================
int g_iBranchesFound = 0;
int g_iJumpTablesFound = 0;
int g_iTotalInstructions = 0;
int g_iTotalBlocks = 0;
//========================================================================================
DWORD g_dwFunction;
//========================================================================================
Block_t* g_pBlock;
JumpTable_t* g_pJumpTable;
//========================================================================================
void SetProcessed ( DWORD dwBlockStart, DWORD dwBlockEnd, DWORD dwJumpTable )
{
	if ( g_iJumpTablesFound != 256 )
	{
		g_pJumpTable [ g_iJumpTablesFound ].m_dwBlockStart = dwBlockStart;
		g_pJumpTable [ g_iJumpTablesFound ].m_dwBlockEnd = dwBlockEnd;
		g_pJumpTable [ g_iJumpTablesFound ].m_dwJumpTable = dwJumpTable;
		g_pJumpTable [ g_iJumpTablesFound ].m_dwPadding = 0;

		g_iJumpTablesFound++;
	}
}
//========================================================================================
BOOL CheckIfProcessed ( DWORD dwJumpTable )
{
	BOOL bReturn;

	int iIterator;

	bReturn = 0;

	if ( g_iJumpTablesFound != 1024 )
	{
		for ( iIterator = 0; iIterator < g_iJumpTablesFound; iIterator++ )
		{
			if ( g_pJumpTable [ iIterator ].m_dwJumpTable != 0 )
			{
				if ( g_pJumpTable [ iIterator ].m_dwJumpTable == dwJumpTable )
				{
					bReturn = 1;

					break;
				}
			}
			else
			{
				bReturn = 0;

				break;
			}
		}
	}
	else
	{
		bReturn = 1;
	}

	return bReturn;
}
//========================================================================================
long __stdcall disasm ( PVOID pvAddress )
{
	hde32s hs;

    unsigned int iLength = hde32_disasm ( pvAddress, &hs );

    if ( hs.flags & F_ERROR )
	{
		return -1;
	}
	else
	{
		return iLength;
	}
}
//========================================================================================
int __stdcall compare ( const void* a, const void* b )
{
	return ( *( PDWORD )a - *( PDWORD )b );
}
//========================================================================================
BOOL __stdcall bIsUnConditionalJump ( PBYTE pbCurrentInstruction )
{
	BOOL bReturnValue = 0;

	switch ( *pbCurrentInstruction )
	{
		case INSTR_SHORTJMP:
		{
			bReturnValue = 1;
		}
		break;

		case INSTR_RELJMP:
		{
			bReturnValue = 1;
		}
	}

	return bReturnValue;
}
//========================================================================================
BOOL __stdcall bIsConditionalInstruction ( PBYTE pbCurrentInstruction, BYTE bEFlags )
{
	BOOL bReturnValue = 0;

	if ( *pbCurrentInstruction == INSTR_CMP )
	{
		if ( *( pbCurrentInstruction + 1 ) >= 0xF8 && *( pbCurrentInstruction + 1 ) <= 0xFF )
		{
			bReturnValue = 1;
		}
	}

	return bReturnValue;
}
//========================================================================================
BOOL __stdcall IsSpecialJump ( PBYTE pbCurrentInstruction )
{
	BOOL bReturnValue = 0;

	if ( *pbCurrentInstruction == INSTR_SHORT_JA || *pbCurrentInstruction == INSTR_SHORT_JG )
	{
		bReturnValue = 1;
	}
	if ( *pbCurrentInstruction == INSTR_NEAR_PREFIX && ( *( pbCurrentInstruction + 1 ) == INSTR_NEAR_JA || *( pbCurrentInstruction + 1 ) == INSTR_NEAR_JG ) )
	{
		bReturnValue = 1;
	}
	
	return bReturnValue;
}
//========================================================================================
enum
{	
	CF = ( 1 << 0 ),
	OF = ( 1 << 1 ),
	PF = ( 1 << 2 ),
	SF = ( 1 << 3 ),
	ZF = ( 1 << 4 )
};
//========================================================================================
BYTE g_bJumpType [ 0x10 ] =
{
	OF, CF, CF, ZF, ZF, ZF, ( CF | ZF ), ( CF | ZF ), 
	SF, SF, PF, PF, ( SF | OF ), ( SF | OF ), ( ZF | SF | OF ), ( ZF | SF | OF )
};
//========================================================================================
enum
{
	REG_EAX = 0,
	REG_ECX = 1,
	REG_EDX = 2,
	REG_EBX = 3,
	REG_ESP = 4,
	REG_EBP = 5,
	REG_ESI = 6,
	REG_EDI = 7
};
//========================================================================================
BOOL __stdcall IsJumpTable ( PBYTE pbCurrentInstruction, BYTE bReg )
{
	BOOL bReturnValue = 0;

	BYTE bModRM;

	switch ( *pbCurrentInstruction )
	{
		case INSTR_FAR_PREFIX:
		{
			bModRM = *( pbCurrentInstruction + 1 );

			if ( bModRM == 0x24 )
			{
				switch ( bReg )
				{
					case REG_EAX:
					case REG_EBX:
					case REG_ECX:
					case REG_EDX:
					case REG_ESI:
					case REG_EDI:
					case REG_EBP:
					{
						bReturnValue = 1;
					}
				}
			}
		}
		break;
	}

	return bReturnValue;
}
//========================================================================================
DWORD __stdcall GetFunctionLength ( PVOID pvFunctionBegin, BOOL bRebaseRelocs, BOOL bFixupRelative, PDWORD pdwFunctionStartOut, DWORD dwRelocateFromBase, DWORD dwRebaseAddress, DWORD dwBufferIn )
{
	BOOL bEndProcessing, bFound, bProcessedAlready, bNotAlreadyProcessed;

	BYTE bEFlags;

	DWORD dwBase, dwCurrentInstruction, dwPreviousInstruction, dwSubBase;

	DWORD dwBranchAddress, dwCopyBuffer, dwDelta, dwJumpTable, dwLength, dwSavedPosition, dwSize;

	hde32s hs;

	int iBlockIterator, iCurrentInstruction, iDuplicateInstructions, iJumpTableIterator, iInstructionsLeft, iIterator, iScale, iSizeOfJumpTable;

	PBYTE pbCurrentInstruction;
	
	PVOID pvBranch, pvPseudoFunctionEnd, pvFunctionEnd;

	memset ( g_pdwBranches, 0, 0x4000 );
	memset ( g_pdwInstructions, 0, 0x8000 );
	memset ( g_pdwFixedFunction, 0, 0x8000 );
	memset ( g_pdwCopiedInstructions, 0, 0x8000 );
	memset ( g_pJumpTable, 0, 0x4000 );
	memset ( g_pBlock, 0, 0x4000 );

	bEFlags = 0;

	dwBase = 0;
	dwBranchAddress = 0;
	dwCopyBuffer = 0;
	dwCurrentInstruction = 0;
	dwDelta = 0;
	
	dwJumpTable = 0;
	dwLength = 0;
	dwPreviousInstruction = 0;
	dwSavedPosition = 0;
	dwSize = 0;
	dwSubBase = 0;
	iBlockIterator = 0;
	iCurrentInstruction = 0;
	iDuplicateInstructions = 0;
	iInstructionsLeft = 0;
	iJumpTableIterator = 0;
	iScale = 0;
	iSizeOfJumpTable = 0;
	pbCurrentInstruction = NULL;
	pvFunctionEnd = NULL;

	g_iBranchesFound = 0;
	g_iJumpTablesFound = 0;
	g_iTotalInstructions = 0;
	g_iTotalBlocks = 0;
	
	bEndProcessing = 0;
	bFound = 0;
	bProcessedAlready = 0;
				
	pvFunctionEnd = GetFunctionEnd ( pvFunctionBegin );

	_quicksort ( g_pBlock, g_iTotalBlocks, sizeof ( Block_t ), compare );

	_quicksort ( g_pdwInstructions, g_iTotalInstructions, sizeof ( DWORD ), compare );

	if ( g_iTotalBlocks != 0 )
	{
		do // start checking each block for a jump to unprocessed code
		{
			dwCurrentInstruction = g_pBlock [ iBlockIterator ].m_dwBlockStart;

			iCurrentInstruction = 0;

			for ( iIterator = g_iTotalInstructions; iIterator >= 0; --iIterator ) // find where this block's start resides within our list of referenced instructions
			{
				if ( dwCurrentInstruction == g_pdwInstructions [ iIterator ] )
				{
					iCurrentInstruction = iIterator;

					break;
				}
			}
			do
			{				
				pvBranch = GetBranchAddress ( ( PBYTE )dwCurrentInstruction );

				if ( pvBranch != NULL ) // we found a branching instruction, check the branch to see if it was processed or not
				{
					bProcessedAlready = 0;

					for ( iIterator = g_iTotalInstructions; iIterator >= 0; --iIterator )
					{
						if ( g_pdwInstructions [ iIterator ] == ( DWORD )pvBranch ) // we have processed this branch
						{
							bProcessedAlready = 1;

							break; // this was processed already
						}
					}

					if ( bProcessedAlready != 1 ) // unprocessed branch
					{
						pvPseudoFunctionEnd = GetFunctionEnd ( pvBranch ); // process it

						if ( pvPseudoFunctionEnd == 0 )
						{
							return 0;
						}
					}
				}
				
				iCurrentInstruction++;

				if ( iCurrentInstruction >= g_iTotalInstructions )
				{
					break;
				}
				
				dwCurrentInstruction = g_pdwInstructions [ iCurrentInstruction ];

			}while ( dwCurrentInstruction <= g_pBlock [ iBlockIterator ].m_dwBlockEnd );

			iBlockIterator++;

		}while ( iBlockIterator < g_iTotalBlocks );
	}

	_quicksort ( g_pdwInstructions, g_iTotalInstructions, sizeof ( DWORD ), compare );

	if ( dwBufferIn == 0 )
	{
		dwCopyBuffer = ( DWORD )g_pdwFixedFunction;
	}
	else
	{
		dwCopyBuffer = dwBufferIn;
	}

	dwPreviousInstruction = 0;

	for ( iIterator = 0; iIterator < g_iTotalInstructions; iIterator++ ) // get rid of any duplicate instructions we may have
	{			
		dwCurrentInstruction = g_pdwInstructions [ iIterator ];

		if ( dwPreviousInstruction == g_pdwInstructions [ iIterator ] )
		{
			g_pdwInstructions [ iIterator ] = 0;

			iDuplicateInstructions++;
		}

		dwPreviousInstruction = dwCurrentInstruction;
	}
	if ( iDuplicateInstructions > 0 ) // resort the buffer and move the buffer forward so we don't crash
	{
		_quicksort ( g_pdwInstructions, g_iTotalInstructions, sizeof ( DWORD ), compare );
		
		g_iTotalInstructions -= iDuplicateInstructions;

		g_pdwInstructions += iDuplicateInstructions;
	}

	dwPreviousInstruction = 0;

	for ( iIterator = 0; iIterator < g_iTotalInstructions; iIterator++ ) // copy the code and set the entry point for the function out
	{		
		dwCurrentInstruction = g_pdwInstructions [ iIterator ];

		if ( dwPreviousInstruction != dwCurrentInstruction )
		{
			dwLength = hde32_disasm ( ( PVOID )g_pdwInstructions [ iIterator ], &hs );

			if ( dwLength != -1 )
			{
				memcpy ( ( PVOID )dwCopyBuffer, ( const PVOID )g_pdwInstructions [ iIterator ], dwLength );

				if ( pdwFunctionStartOut != NULL )
				{
					if ( g_pdwInstructions [ iIterator ] == ( DWORD )pvFunctionBegin ) // we hit the function's beginning set the out
					{
						*pdwFunctionStartOut = dwCopyBuffer;
					}
				}

				g_pdwCopiedInstructions [ iIterator ] = dwCopyBuffer;
			
				dwCopyBuffer += dwLength;
			}
			else
			{
				g_pdwInstructions -= iDuplicateInstructions;

				return 0;
			}
		}

		dwPreviousInstruction = dwCurrentInstruction;
	}
	if ( dwBufferIn == 0 ) // align our output buffer to copy the fixed up code
	{
		dwDelta = dwCopyBuffer - ( DWORD )g_pdwFixedFunction;
	}
	else
	{
		dwDelta = dwCopyBuffer - dwBufferIn;
	}
	for ( iIterator = 0; iIterator < g_iTotalInstructions; iIterator++ ) // this here code repairs all jumps and calls
	{
		dwCopyBuffer = g_pdwCopiedInstructions [ iIterator ];

		pbCurrentInstruction = ( PBYTE )g_pdwInstructions [ iIterator ];

		iInstructionsLeft = 0;

		bFound = 0;
			
		if ( bFixupRelative == 1 )
		{
			switch ( *pbCurrentInstruction )
			{
				case INSTR_CALL:
				{
					dwBranchAddress = dwGetCallAddress ( ( PDWORD )pbCurrentInstruction );
						
					*( PDWORD )( dwCopyBuffer + 1 ) = dwBranchAddress - dwCopyBuffer - 5; 
				}
				break;

				case INSTR_RELJMP:
				{
					dwBranchAddress = ( DWORD )GetBranchAddress ( pbCurrentInstruction ); // CALL and JMP are same length and process the same

					while ( iInstructionsLeft != g_iTotalInstructions )
					{
						if ( dwBranchAddress == g_pdwInstructions [ iInstructionsLeft ] )
						{
							bFound = 1;

							break;
						}

						iInstructionsLeft++;
					}
						
					if ( bFound == 1 )
					{
						*( PDWORD )( dwCopyBuffer + 1 ) = g_pdwCopiedInstructions [ iInstructionsLeft ] - dwCopyBuffer - 5;
					}
					else
					{
						*( PDWORD )( dwCopyBuffer + 1 ) = dwBranchAddress - dwCopyBuffer - 5;
					}
				}
				break;

				case INSTR_NEAR_PREFIX:
				{
					if ( *( pbCurrentInstruction + 1 ) >= INSTR_NEARJCC_BEGIN && *( pbCurrentInstruction + 1 ) <= INSTR_NEARJCC_END )
					{
						dwBranchAddress = ( DWORD )GetBranchAddress ( pbCurrentInstruction );

						while ( iInstructionsLeft != g_iTotalInstructions )
						{
							if ( dwBranchAddress == g_pdwInstructions [ iInstructionsLeft ] )
							{
								bFound = 1;

								break;
							}

							iInstructionsLeft++;
						}
						if ( bFound == 1 )
						{
							*( PDWORD )( dwCopyBuffer + 2 ) = g_pdwCopiedInstructions [ iInstructionsLeft ] - dwCopyBuffer - 6;
						}
						else
						{
							*( PDWORD )( dwCopyBuffer + 2 ) = dwBranchAddress - dwCopyBuffer - 6;
						}
					}
				}
				break;

				case INSTR_RELJCX:
				case INSTR_SHORTJMP:
				{
					dwBranchAddress = ( DWORD )GetBranchAddress ( pbCurrentInstruction );

					while ( iInstructionsLeft != g_iTotalInstructions )
					{
						if ( dwBranchAddress == g_pdwInstructions [ iInstructionsLeft ] )
						{
							bFound = 1;

							break;
						}

						iInstructionsLeft++;
					}
						
					if ( bFound == 1 )
					{
						*( PBYTE )( dwCopyBuffer + 1 ) = g_pdwCopiedInstructions [ iInstructionsLeft ] - dwCopyBuffer - 2;
					}
					else
					{
						*( PBYTE )( dwCopyBuffer + 1 ) = dwBranchAddress - dwCopyBuffer - 2;
					}
				}
				break;

				default:
				{
					if ( *pbCurrentInstruction >= INSTR_SHORTJCC_BEGIN && *pbCurrentInstruction <= INSTR_SHORTJCC_END )
					{
						dwBranchAddress = ( DWORD )GetBranchAddress ( pbCurrentInstruction );

						while ( iInstructionsLeft != g_iTotalInstructions )
						{
							if ( dwBranchAddress == g_pdwInstructions [ iInstructionsLeft ] )
							{
								bFound = 1;

								break;
							}

							iInstructionsLeft++;
						}
							
						if ( bFound == 1 )
						{
							*( PBYTE )( dwCopyBuffer + 1 ) = g_pdwCopiedInstructions [ iInstructionsLeft ] - dwCopyBuffer - 2;
						}
						else
						{
							*( PBYTE )( dwCopyBuffer + 1 ) = dwBranchAddress - dwCopyBuffer - 2;
						}
					}
				}
				break;
			}
		}
	}
	if ( bRebaseRelocs == 1 )
	{
		ReprocessRelocations ( dwCopyBuffer, dwRelocateFromBase, dwRebaseAddress, dwDelta );
	}
	for ( iIterator = 0; iIterator < g_iTotalBlocks; iIterator++ ) // go through each block and look for any jumptables
	{
		dwCurrentInstruction = g_pBlock [ iIterator ].m_dwBlockStart;

		bFound = 0;

		dwSize = g_pBlock [ iIterator ].m_dwBlockEnd;

		g_iJumpTablesFound = 0;

		do
		{
			dwLength = hde32_disasm ( ( PVOID )dwCurrentInstruction, &hs );

			if ( dwLength == -1 )
			{
				return 0;
			}
			
			if ( hs.p_seg )
			{
				dwCurrentInstruction += 1; // move forward of the segment prefix
			}

			bNotAlreadyProcessed = CheckIfProcessed ( dwCurrentInstruction );

			if ( IsJumpTable ( ( PBYTE )dwCurrentInstruction, hs.sib_index ) && bNotAlreadyProcessed != 1 )
			{
				// we found a jumptable, mark it for future processing
				
				dwJumpTable = dwCurrentInstruction;
				
				
				__asm
				{
					PUSH EAX
					INT 3
					MOV AL, hs.sib_scale
					MOV AL, hs.sib_base
					MOV AL, hs.sib_index
					MOV EAX, hs.disp
					MOV EAX, 1
					MOV AL, hs.sib
					MOV EAX, 2
					MOV EAX, hs.imm
					MOV EAX, 3
					MOV EAX, hs.rel
					MOV EAX, 4
					MOV AL, hs.modrm
					MOV EAX, 5
					MOV AL, hs.modrm_reg
					MOV EAX, 6
					MOV AL, hs.modrm_mod
					MOV EAX, 7
					MOV AL, hs.opcode
					MOV EAX, 8
					MOV AL, hs.opcode2
					MOV EAX, 9
					MOV AL, hs.p_seg
					POP EAX
					
				}
					
				SetProcessed ( g_pBlock [ iIterator ].m_dwBlockStart, g_pBlock [ iIterator ].m_dwBlockEnd, dwCurrentInstruction );
			}

			if ( hs.p_seg ) // otherwise just reset and continue
			{
				dwCurrentInstruction -= 1;
			}

			dwCurrentInstruction += dwLength;

		}while ( dwCurrentInstruction <= dwSize );

		if ( g_iJumpTablesFound != 0 ) // found jumptable(s)
		{
			for ( iJumpTableIterator = 0; iJumpTableIterator < g_iJumpTablesFound; iJumpTableIterator++ )
			{
				dwCurrentInstruction = g_pJumpTable [ iJumpTableIterator ].m_dwBlockStart;

				do
				{
					// ok so we know the block we're in has a fat jumptable we need to take care of!!
				
					pvBranch = GetBranchAddress ( ( PBYTE )dwCurrentInstruction );

					if ( pvBranch == NULL ) // not a branching instruction
					{
						goto move;
					}
					else					// ok this looks promising... check if it is an unconditional jump if not we do further analysis
					{
						if ( bIsUnConditionalJump ( ( PBYTE )dwCurrentInstruction ) ) // no good, this is an unconditional branch...
						{
							goto move;
						}
						else		
						{
							if ( ( DWORD )pvBranch > g_pJumpTable [ iJumpTableIterator ].m_dwJumpTable  ) // ok so this definitely branches over the jump table address and is not unconditional
							{						
								// alright this jump is definitely of interest, it's not an unconditonal branch and it branches over the jump table
								// now we need to find out which instruction this jump is paired with by examining the jump behavior
								// which means we need to setup our own pseudo eflags register that we can then compare the instructions
								// we're disassembling to determine if an instruction belongs with that jump
								// so for instance:
/*
								mov al, [index_into_jumptable*base+base]
								cmp al, 0xF ; size of jumptable
								ja overjumptable
								jmp dword ptr ds [index_to_jump_to*4+base]

77148F5C   0FB645 0C                            MOVZX EAX,BYTE PTR SS:[EBP+C]
77148F60   48                                   DEC EAX
77148F61   83F8 07                              CMP EAX,7
77148F64   77 15                                JA SHORT ntdll.77148F7B
77148F66   FF2485 BE8E1477                      JMP DWORD PTR DS:[EAX*4+77148EBE]



overjumptable:
*/

								bEFlags = 0;

								// check and see if this jump has a prefix

								pbCurrentInstruction = ( PBYTE )dwCurrentInstruction;

								if ( *pbCurrentInstruction == INSTR_NEAR_PREFIX )
								{
									bEFlags = g_bJumpType [ INSTR_NEARJCC_END - *( pbCurrentInstruction + 1 ) ];
								}
								else
								{
									if ( *pbCurrentInstruction != INSTR_RELJCX )
									{
										bEFlags = g_bJumpType [ INSTR_SHORTJCC_END - *pbCurrentInstruction ];
									}
								}

								if ( IsSpecialJump ( pbCurrentInstruction ) ) // a JA or JG
								{
									dwSavedPosition = dwCurrentInstruction; // store the current conditional jump and consider it the "end" point

									dwCurrentInstruction = g_pJumpTable [ iJumpTableIterator ].m_dwBlockStart;

									do
									{
										//( bEFlags & ( CF | ZF ) || bEFlags & ( OF | SF | ZF ) ) &&

										if ( bIsConditionalInstruction ( ( PBYTE )dwCurrentInstruction, bEFlags ) ) // check this instruction to see what flags it modifies
										{
											dwLength = hde32_disasm ( ( PVOID )dwCurrentInstruction, &hs );

											if ( dwLength == -1 )
											{
												return 0;
											}

											iSizeOfJumpTable = hs.imm.imm32;
																				
											dwSubBase = dwCurrentInstruction;

											break;
										}

										// ok so now we know the size of the jumptable at least...
										// now see if the register can be paired with a matching address that it reads from or passed as an argument

										dwLength = hde32_disasm ( ( PVOID )dwCurrentInstruction, &hs );

										if ( dwLength == -1 )
										{
											return 0;
										}

										dwCurrentInstruction += dwLength;

									}while ( dwCurrentInstruction < dwSavedPosition );

									// now try to find the instruction that has the index into the jumptable where we compared against the size of the table

									dwCurrentInstruction = dwSavedPosition;
								}
							}
							else // branches backwards or not ahead of the jumptable, not what we're looking for...
							{
								goto move; // Officer Barbrady: Move along people there's nothing to see here.
							}
						}
					}

					move:

					dwLength = hde32_disasm ( ( PVOID )dwCurrentInstruction, &hs );

					if ( dwLength == -1 )
					{
						return 0;
					}

					dwCurrentInstruction += dwLength;
				
				}while ( dwCurrentInstruction <  g_pJumpTable [ iJumpTableIterator ].m_dwJumpTable );
			}
		}
	}

	if ( iDuplicateInstructions > 0 )
	{
		g_pdwInstructions -= iDuplicateInstructions;
	}

	return dwDelta;
}
//========================================================================================
PVOID __stdcall GetFunctionEnd ( PVOID pvFunc )
{
	PDWORD pdwBranches;

	PVOID pvBlockEnd;
	
	pvBlockEnd = GetBranchListFromBlock ( pvFunc );

	if ( g_iBranchesFound == 0 )
		return pvBlockEnd;

	if ( g_iBranchesFound == 4096 ) // very unlikely we'll ever hit 4,096 branches in a single function, but it could happen if someone was crazy enough
		return pvBlockEnd;
		
	_quicksort ( g_pdwBranches, g_iBranchesFound, sizeof ( DWORD ), compare ); // compiler optimizes this away

	if ( g_iTotalBlocks < 2048 )
	{
		g_pBlock [ g_iTotalBlocks ].m_dwBlockStart = ( DWORD )pvFunc;
		g_pBlock [ g_iTotalBlocks ].m_dwBlockEnd = ( DWORD )pvBlockEnd;

		g_iTotalBlocks++;
	}
	else
	{
		return pvBlockEnd;
	}
	
	PVOID pvPreviousBranch = NULL;
	PVOID* pvCurrentBranch;
		
	pdwBranches = ( PDWORD )g_pdwBranches;

	for ( pvCurrentBranch = ( PVOID* )g_pdwBranches; *pvCurrentBranch != ( PVOID )g_pdwBranches [ g_iBranchesFound ]; pvCurrentBranch++ )
	{	
		if ( *pvCurrentBranch <= pvBlockEnd || *pvCurrentBranch == pvPreviousBranch )											  
		{
			continue;
		}
		
		pvBlockEnd = GetFunctionEnd ( *pvCurrentBranch );

		pvPreviousBranch = *pvCurrentBranch;
	}

	return pvBlockEnd;
}
//========================================================================================
PVOID __stdcall GetBranchListFromBlock ( PVOID pvCurrentBlock )
{
	long length;

	PBYTE pbCurrentBlock = ( PBYTE )pvCurrentBlock;
	
	while ( IsEndPoint ( pbCurrentBlock, pvCurrentBlock ) != 1 ) // keep going until we hit a predefined end point, jump to previous branch, or retn opcode(s)
	{
		if ( bIsUnConditionalJump ( pbCurrentBlock ) && ( *pbCurrentBlock == INSTR_RELJMP || *pbCurrentBlock == INSTR_SHORTJMP ) && g_pdwBranches [ g_iBranchesFound - 1 ] > ( DWORD )pbCurrentBlock )
		{
			pbCurrentBlock = ( PBYTE )g_pdwBranches [ g_iBranchesFound - 1 ];
		}
		else
		{
			length = disasm ( pbCurrentBlock );

			if ( length == -1 )
			{
				return pbCurrentBlock;
			}

			pbCurrentBlock += length;
		}
	}
	
	return pbCurrentBlock;
}
//========================================================================================
PVOID __stdcall GetBranchAddress ( PBYTE pbCurrentInstruction )
{	
	SDWORD sdwOffset = 0;

	switch ( *pbCurrentInstruction )
	{
		case INSTR_SHORTJMP:
		case INSTR_RELJCX:
		{
			sdwOffset = ( SDWORD )( *( PSBYTE )( pbCurrentInstruction + 1 ) );
			sdwOffset += 2;
		}
		break;

		case INSTR_RELJMP:
		{
			sdwOffset = *( PSDWORD )( pbCurrentInstruction + 1 );
			sdwOffset += 5;
		}
		break;

		case INSTR_NEAR_PREFIX:
		{
			if ( *( pbCurrentInstruction + 1 ) >= INSTR_NEARJCC_BEGIN && *( pbCurrentInstruction + 1 ) <= INSTR_NEARJCC_END )
			{
				sdwOffset = *( PSDWORD )( pbCurrentInstruction + 2 );
				sdwOffset += 6;
			}
		}
		break;
		
		default:
		{
			if ( *pbCurrentInstruction >= INSTR_SHORTJCC_BEGIN && *pbCurrentInstruction <= INSTR_SHORTJCC_END )
			{
				sdwOffset = ( SDWORD )*( ( PSBYTE )( pbCurrentInstruction + 1 ) );
				sdwOffset += 2;
			}
		}
		break;
	}

	if ( sdwOffset == 0 )
	{
		return NULL;
	}

	return pbCurrentInstruction + sdwOffset;
}
//========================================================================================
BOOL __stdcall IsEndPoint ( PBYTE pbCurrentInstruction, PVOID pvCurrentBlock )
{
	BOOL bReturnValue;

	BYTE bModRM;

	PVOID pvAddress;

	SDWORD sdwOffset;

	DWORD dwBasePage;
	
	unsigned int iLength;

	bReturnValue = 0;
	bModRM = 0;
	dwBasePage = 0;
	iLength = 0;
	pvAddress = NULL;

	hde32s hs;

	PVOID pvBranchAddress;

	pvBranchAddress = GetBranchAddress ( pbCurrentInstruction );

	switch ( *pbCurrentInstruction )
	{
		case INSTR_RET:
		case INSTR_RETN:
		case INSTR_RETFN:
		case INSTR_RETF:
		{
			bReturnValue = 1;
		}
		break;

		case INSTR_SHORTJMP:
		{
			sdwOffset = ( SDWORD )( *( PSBYTE )( pbCurrentInstruction + 1 ) );
			pvAddress = GetBranchAddress ( pbCurrentInstruction );

			if ( pvAddress <= pvCurrentBlock ) // returning to a previous branch
			{
				bReturnValue = 1;
			}
		}
		break;

		case INSTR_RELJMP:
		{
			sdwOffset  = *( PSDWORD )( pbCurrentInstruction + 1 );
			pvAddress = GetBranchAddress ( pbCurrentInstruction );

			if ( pvAddress <= pvCurrentBlock ) // returning to a previous branch
			{
				bReturnValue = 1;
			}
		}
		break;

		case INSTR_FAR_PREFIX:
		{
			bModRM = *( pbCurrentInstruction + 1 );

			if ( bModRM == 0x25 )
			{
				bReturnValue = 1;
			}
			else	// 0x20 -> 0x27
			{
				if ( bModRM >= 0x20 && bModRM <= 0x27 )
				{
					iLength = disasm ( pbCurrentInstruction );
					
					bReturnValue = 1;
				}
			}
		}
		break;

		default:
		{
			// check for prefix byte !!!!
			
			iLength = hde32_disasm ( pbCurrentInstruction, &hs );

			if ( iLength != -1 )
			{
				if ( hs.p_seg )
				{
					if ( *( pbCurrentInstruction + 1 ) == INSTR_FAR_PREFIX )
					{
						bModRM = *( pbCurrentInstruction + 2 );

						if ( bModRM == 0x25 )
						{
							bReturnValue = 1;
						}
						else	// 0x20 -> 0x27
						{
							if ( bModRM >= 0x20 && bModRM <= 0x27 )
							{
								iLength = disasm ( pbCurrentInstruction );
								
								bReturnValue = 1;
							}
						}

					}
				}
			}
			else
			{
				bReturnValue = 0;
			}
		}
		break;
	}
	if ( disasm ( pbCurrentInstruction ) != - 1 )
	{
		if ( g_iTotalInstructions < 8192 )
		{
			g_pdwInstructions [ g_iTotalInstructions ] = ( DWORD )pbCurrentInstruction;

			g_iTotalInstructions++;
		}
		else
		{
			bReturnValue = 1;
		}
	}
	else
	{
		bReturnValue = 1;
	}

	if ( pvBranchAddress && g_iBranchesFound < 4096 )
	{
		g_pdwBranches [ g_iBranchesFound ] = ( DWORD )pvBranchAddress;

		g_iBranchesFound++;
	}

	return bReturnValue;
}
//========================================================================================
BYTE bOpcodeTable [ 256 ] =
{
	01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01,
	01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01,
	01, 01, 01, 01, 01, 01, 01, 00, 01, 01, 01, 01, 01, 01, 01, 00,
	01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01,
	01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01,
	02, 02, 02, 02, 02, 02, 02, 02, 01, 01, 01, 01, 01, 01, 01, 01,
	01, 01, 01, 01, 01, 01, 01, 01, 02, 01, 02, 01, 00, 00, 00, 00,
	01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01,
	02, 02, 02, 02, 01, 01, 01, 01, 02, 02, 02, 02, 02, 02, 02, 01,
	01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01,
	02, 02, 02, 02, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01,
	01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01,
	01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 00, 01, 01, 01,
	01, 01, 01, 01, 01, 01, 01, 00, 01, 02, 01, 01, 01, 01, 01, 01,
	01, 01, 01, 01, 00, 00, 00, 00, 01, 01, 01, 01, 00, 00, 00, 00,
	01, 01, 01, 01, 00, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01
};
//========================================================================================
int __stdcall GetFunctionLengthVAC3Method ( DWORD dwFunctionStart, DWORD dwSize )
{
	BYTE bLastOpcode, bPreviousToLastOpcode;

	hde32s hs;

	int iCurIterator, iInstructionLength, iNumINT3s;
	
	PBYTE pbCurrentAddress;

	pbCurrentAddress = ( PBYTE )dwFunctionStart;

	iCurIterator = 0;

	iNumINT3s = 0;

	bLastOpcode = 0;
	bPreviousToLastOpcode = 0;
			
	for ( ;; )
	{
		iInstructionLength = hde32_disasm ( pbCurrentAddress, &hs );

		iCurIterator += iInstructionLength;

		if ( iCurIterator >= dwSize )
		{
			break;
		}
		if ( iInstructionLength == 1 )
		{
			if ( *pbCurrentAddress == 0xCC ) // INT3 
			{
				iNumINT3s++;
			}

			if ( ( iNumINT3s >= 3 && *pbCurrentAddress == 0xCC && bLastOpcode == 0xCC && bPreviousToLastOpcode == 0xCC )|| bOpcodeTable  [ *pbCurrentAddress ] == 0  ) 
			{
				return ( DWORD )pbCurrentAddress - dwFunctionStart;
			}

			switch ( *pbCurrentAddress )
			{
				case INSTR_RET:
				{
					pbCurrentAddress += iInstructionLength;

					return ( DWORD )pbCurrentAddress - dwFunctionStart;
				}
				break;
			}


			bPreviousToLastOpcode = bLastOpcode;
			bLastOpcode = *pbCurrentAddress;
			
			pbCurrentAddress++;
		}
		else
		{
			if ( iInstructionLength != -1 && iInstructionLength != 0 )
			{
				switch ( *pbCurrentAddress )
				{
					case INSTR_RETN:
					case INSTR_RETFN:
					case INSTR_RETF:
					{
						pbCurrentAddress += iInstructionLength;

						return ( DWORD )pbCurrentAddress - dwFunctionStart;
					}
					break;
				}

				pbCurrentAddress += iInstructionLength;
			}
			else
			{
				break;
			}
		}
	}

	return ( DWORD )pbCurrentAddress - dwFunctionStart;
}
//========================================================================================
/*
int __stdcall GetFunctionLengthVAC3Method ( DWORD dwFunctionStart, DWORD dwSize )
{
	hde32s hs;

	int iCurIterator, iInstructionLength;
	
	PBYTE pbCurrentAddress;

	pbCurrentAddress = ( PBYTE )dwFunctionStart;

	iCurIterator = 0;
			
	for ( ;; )
	{
		iInstructionLength = hde32_disasm ( pbCurrentAddress, &hs );

		iCurIterator += iInstructionLength;

		if ( iCurIterator >= dwSize )
		{
			break;
		}

		if ( iInstructionLength == 1 )
		{
			if ( bOpcodeTable  [ *pbCurrentAddress ] == 0 )
			{
				break;
			}
			
			pbCurrentAddress++;
		}
		else
		{
			if ( iInstructionLength != -1 && iInstructionLength != 0 )
			{
				pbCurrentAddress += iInstructionLength;
			}
			else
			{
				break;
			}
		}
	}

	return ( DWORD )pbCurrentAddress - dwFunctionStart;
}
*/
//========================================================================================
DWORD __stdcall dwGetLongConditionalJumpAddress ( PDWORD pdwAddress )
{
	DWORD dwAddress;

	PBYTE pbAddress = ( PBYTE )pdwAddress;
	pbAddress = &pbAddress [ 2 ];

	memcpy( ( PVOID )&dwAddress, pbAddress, sizeof ( DWORD ) );
	return ( dwAddress + ( DWORD ) pdwAddress + 6 );
}
//========================================================================================
DWORD __stdcall dwGetCallAddress ( PDWORD pdwAddress )
{
	DWORD dwAddress;

	PBYTE pbAddress = ( PBYTE )pdwAddress;
	pbAddress = &pbAddress [ 1 ];

	memcpy( ( PVOID )&dwAddress, pbAddress, sizeof ( DWORD ) );
	return ( dwAddress + ( DWORD ) pdwAddress + 5 );
}
//========================================================================================