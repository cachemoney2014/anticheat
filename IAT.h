//========================================================================================
#pragma once
//========================================================================================
#include "NT.h"
#include "macros.h"
//========================================================================================
#include "Packet.h"
//========================================================================================
#include "typedefs.h"
//========================================================================================
// This import list is LIABLE to change keep that in mind
//========================================================================================
enum
{
	IMPORT_MD5Final = 0,
	IMPORT_MD5Init,
	IMPORT_MD5Update,
//	IMPORT_TransformMD5,
	IMPORT_NtAllocateVirtualMemory,
	IMPORT_NtClose,
	IMPORT_NtCreateDirectoryObject,
	IMPORT_NtCreateEvent,
	IMPORT_NtCreateFile,
	IMPORT_NtCreateKey,
	IMPORT_NtCreateSection,
	IMPORT_NtDeleteFile,
	IMPORT_NtDeleteKey,
	IMPORT_NtDuplicateObject,
	IMPORT_NtDuplicateToken,
	IMPORT_NtEnumerateKey,
	IMPORT_NtEnumerateValueKey,
	IMPORT_NtFlushBuffersFile,
//	IMPORT_NtFlushBuffersFileEx,
	IMPORT_NtFlushKey,
	IMPORT_NtFlushVirtualMemory,
	IMPORT_NtFreeVirtualMemory,
	IMPORT_NtGetContextThread,
	IMPORT_NtLockFile,
	IMPORT_NtLockVirtualMemory,
	IMPORT_NtMapViewOfSection,
	IMPORT_NtNotifyChangeKey,
	IMPORT_NtOpenFile,
	IMPORT_NtOpenKey,
	IMPORT_NtOpenProcess,
	IMPORT_NtOpenProcessTokenEx,
	IMPORT_NtOpenSection,
	IMPORT_NtOpenThread,
	IMPORT_NtProtectVirtualMemory,
	IMPORT_NtQueryDirectoryFile,
	IMPORT_NtQueryInformationFile,
	IMPORT_NtQueryInformationProcess,
	IMPORT_NtQueryInformationThread,
	IMPORT_NtQueryInformationToken,
	IMPORT_NtQueryKey,
	IMPORT_NtQueryObject,
	IMPORT_NtQuerySection,
	IMPORT_NtQuerySecurityObject,
	IMPORT_NtQuerySystemInformation,
	IMPORT_NtQueryValueKey,
	IMPORT_NtQueryVirtualMemory,
	IMPORT_NtQueryVolumeInformationFile,
	IMPORT_NtReadFile,
	IMPORT_NtReadVirtualMemory,
	IMPORT_NtResumeThread,
	IMPORT_NtSetInformationToken,
	IMPORT_NtSetSecurityObject,
	IMPORT_NtSuspendThread,
	IMPORT_NtUnlockFile,
	IMPORT_NtUnlockVirtualMemory,
	IMPORT_NtUnmapViewOfSection,
	IMPORT_NtWriteFile,
	IMPORT_NtWriteVirtualMemory,
	IMPORT_RtlAllocateHeap,
	IMPORT_RtlFreeHeap,
	IMPORT_RtlGetVersion,
	IMPORT_RtlNtStatusToDosError,
	IMPORT_GetCurrentProcess,
	IMPORT_END
};
//========================================================================================
#define BLOCK_BEGIN							0x1337BEEF
#define BLOCK_BEGIN_1						0xCAFEBEEF
//========================================================================================
#define BLOCK_END							0xC01DBEEF
#define BLOCK_END_1							0xDEADBEEF
//========================================================================================
#define NTDLL_HASH							0x85DCD932
#define KERNEL32_HASH						0x6FF346E
//========================================================================================
#define MD5Final_HASH						0xF465E8D3
#define MD5Init_HASH						0x800F0ECB
#define MD5Update_HASH						0x2B9D9079
#define NtAllocateVirtualMemory_HASH		0x7D5E775A
#define NtClose_HASH						0xC75FCAF4
#define NtCreateDirectoryObject_HASH		0xA1927E70
#define NtCreateEvent_HASH					0xBE397B01
#define NtCreateFile_HASH					0x6125A54D
#define NtCreateKey_HASH					0x95CDEFC6
#define NtCreateSection_HASH				0xF6A63D5B
#define NtDeleteFile_HASH					0x49A487FC
#define NtDeleteKey_HASH					0x3F880B9B
#define NtDuplicateObject_HASH				0x273D31A7
#define NtDuplicateToken_HASH				0x9581865F
#define NtEnumerateKey_HASH					0x58A5891
#define NtEnumerateValueKey_HASH			0xA4CB94AE
#define NtFlushBuffersFile_HASH				0x517D4070
//#define NtFlushBuffersFileEx_HASH			0x6B5E1B20
#define NtFlushKey_HASH						0x6DD96FD7
#define NtFlushVirtualMemory_HASH			0xBDC8C6E4
#define NtFreeVirtualMemory_HASH			0x43DCF024
#define NtGetContexThread_HASH				0x25676D79
#define NtLockFile_HASH						0x6A996756
#define NtLockVirtualMemory_HASH			0xBC9CC932
#define NtMapViewOfSection_HASH				0x9F2E4AE3
#define NtNotifyChangeKey_HASH				0xB0591DAE
#define NtOpenFile_HASH						0xD4F3DC78
#define NtOpenKey_HASH						0x558E2A9F
#define NtOpenProcess_HASH					0x60D7424C
#define NtOpenProcessTokenEx_HASH			0x2C64E8DD
#define NtOpenSection_HASH					0x4406AA64
#define NtOpenThread_HASH					0xDDB3C4DE
#define NtProtectVirtualMemory_HASH			0xAD0E0164
#define NtQueryDirectoryFile_HASH			0x7CE4D65D
#define NtQueryInformationFile_HASH			0xFE197C7D
#define NtQueryInformationProcess_HASH		0xDC5C081
#define NtQueryInformationThread_HASH		0x51C56116
#define NtQueryInformationToken_HASH		0x5F4224D8
#define NtQueryKey_HASH						0x79E980D0
#define NtQueryObject_HASH					0xB1C92F0D
#define NtQuerySection_HASH					0x98E44B0D
#define NtQuerySecurityObject_HASH			0x9F00FFF2
#define NtQuerySystemInformation_HASH		0xED11F5C8
#define NtQueryValueKey_HASH				0x8ED69DD8
#define NtQueryVirtualMemory_HASH			0xF112DD09
#define NtQueryVolumeInformationFile_HASH	0x3296B073
#define NtReadFile_HASH						0x4361B875
#define NtReadVirtualMemory_HASH			0x4ED666AA
#define NtResumeThread_HASH					0xAAC5D704
#define NtSetInformationToken_HASH			0xB6DC42C7
#define NtSetSecurityObject_HASH			0xD9F2325D
#define NtSuspendThread_HASH				0x492014C6
#define NtUnlockFile_HASH					0x7ECCED68
#define NtUnlockVirtualMemory_HASH			0x6332CA06
#define NtUnmapViewOfSection_HASH			0x3E903A11
#define NtWriteFile_HASH					0xF40DDF90
#define NtWriteVirtualMemory_HASH			0xE0C90A2A
#define RtlAllocateHeap_HASH				0x539BC3D1
#define RtlFreeHeap_HASH					0xCD09A2BB
#define RtlGetVersion_HASH					0xE59573B3
#define RtlNtStatusToDosError_HASH			0xD7F67361
//========================================================================================
#define AddVectoredExceptionHandler_HASH	0xEC115C21
#define GetCurrentProcess_HASH				0xD8CAA52D
//========================================================================================
#define RESOLVE_IAT 1
#define CHECKSUM_IAT 2
//========================================================================================
extern DWORD g_dwCoreDLLFunctions [ IMPORT_END ];
//========================================================================================
extern DWORD g_dwIATCRC;
//========================================================================================
DWORD __stdcall dwCRCBlock ( DWORD dwRemainder, PVOID pBlock, int iBlockSize );  // used to validate IAT, among other things
BOOL __stdcall IAT ( int iArg, Validation_Response_Packet_t* Validation_Response_Packet );
DLL_EXPORT BOOL __stdcall Unload ( void );
DLL_EXPORT BOOL __stdcall StartupFunction ( Validation_Response_Packet_t* Validation_Response_Packet );
//========================================================================================
BOOL __stdcall ResolveImports ( PDWORD pdwImportBlock, int iBlockSize, PDWORD pdwBufferOut );
//========================================================================================
BOOL __stdcall ResolveImportsEx ( HANDLE hProcess, PDWORD pdwImportBlock, int iBlockSize, PDWORD pdwBufferOut );
//========================================================================================
typedef HANDLE ( WINAPI* GetCurrentProcess_t )( void );
//========================================================================================
typedef void ( __stdcall* MD5Init_t )( MD5Context_t* context );
typedef void ( __stdcall* MD5Update_t )( MD5Context_t* context, unsigned char* buf, unsigned int len );
typedef void ( __stdcall* MD5Final_t )( MD5Context_t* context );
//========================================================================================
typedef NTSTATUS ( NTAPI* NtAllocateVirtualMemory_t )( HANDLE ProcessHandle, PVOID *BaseAddress, ULONG_PTR ZeroBits, PSIZE_T RegionSize, ULONG AllocationType, ULONG Protect );
typedef NTSTATUS ( NTAPI* NtCreateFile_t )( PHANDLE FileHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, PIO_STATUS_BLOCK IoStatusBlock, PLARGE_INTEGER AllocationSize, ULONG FileAttributes, ULONG ShareAccess, ULONG CreateDisposition, ULONG CreateOptions, PVOID EaBuffer, ULONG EaLength );
typedef NTSTATUS ( NTAPI* NtClose_t )( HANDLE Handle );
typedef NTSTATUS ( NTAPI* NtDuplicateObject_t )( HANDLE SourceProcessHandle, HANDLE SourceHandle, HANDLE TargetProcessHandle, HANDLE TargetHandle, ACCESS_MASK DesiredAccess, ULONG HandleAttributes, ULONG Options );
typedef NTSTATUS ( NTAPI* NtFreeVirtualMemory_t )( HANDLE ProcessHandle, PVOID *BaseAddress, PSIZE_T RegionSize, ULONG FreeType ); // NtCurrentProcess
typedef NTSTATUS ( NTAPI* NtGetContextThread_t )( HANDLE ThreadHandle, PCONTEXT pContext ); 
typedef NTSTATUS ( NTAPI* NtOpenProcess_t )( PHANDLE ProcessHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, PCLIENT_ID ClientId );
typedef NTSTATUS ( NTAPI* NtOpenThread_t )( PHANDLE ThreadHandle, ACCESS_MASK DesiredAccess, POBJECT_ATTRIBUTES ObjectAttributes, PCLIENT_ID ClientId );
typedef NTSTATUS ( NTAPI* NtProtectVirtualMemory_t )( HANDLE ProcessHandle, PVOID* BaseAddress, SIZE_T* NumberOfBytesToProtect, ULONG NewAccessProtection, PULONG OldAccessProtection );	 
typedef NTSTATUS ( NTAPI* NtQueryDirectoryFile_t )( HANDLE FileHandle, HANDLE Event, PIO_APC_ROUTINE ApcRoutine, PVOID ApcContext, PIO_STATUS_BLOCK IoStatusBlock, PVOID FileInformation, ULONG Length,FILE_INFORMATION_CLASS FileInformationClass, BOOLEAN ReturnSingleEntry, PUNICODE_STRING FileMask, BOOLEAN RestartScan );
typedef NTSTATUS ( NTAPI* NtQueryInformationFile_t )(  HANDLE FileHandle, PIO_STATUS_BLOCK IoStatusBlock, PVOID FileInformation, ULONG Length, FILE_INFORMATION_CLASS FileInformationClass );
typedef NTSTATUS ( NTAPI* NtQueryInformationProcess_t )( HANDLE ProcessHandle, PROCESSINFOCLASS ProcessInformationClass, PVOID ProcessInformation, ULONG ProcessInformationLength, PULONG ReturnLength );
typedef NTSTATUS ( NTAPI* NtQueryInformationThread_t )( HANDLE ThreadHandle, THREADINFOCLASS ThreadInformationClass, PVOID ThreadInformation, ULONG ThreadInformationLength, PULONG ReturnLength );
typedef NTSTATUS ( NTAPI* NtQueryObject_t )( HANDLE Handle, OBJECT_INFORMATION_CLASS ObjectInformationClass, PVOID ObjectInformation, ULONG ObjectInformationLength, PULONG ReturnLength );
typedef NTSTATUS ( NTAPI* NtQuerySystemInformation_t )( SYSTEM_INFORMATION_CLASS SystemInformationClass, PVOID SystemInformation, ULONG SystemInformationLength, PULONG ReturnLength );
typedef NTSTATUS ( NTAPI* NtQueryVirtualMemory_t )( HANDLE ProcessHandle, PVOID Address, MEMORY_INFORMATION_CLASS VirtualMemoryInformationClass, PVOID VirtualMemoryInformation, SIZE_T Length, PSIZE_T ResultLength );
typedef NTSTATUS ( NTAPI* NtQueryVolumeInformationFile_t )( HANDLE FileHandle, PIO_STATUS_BLOCK IoStatusBlock, PVOID FsInformation, ULONG Length, FS_INFORMATION_CLASS FsInformationClass );
typedef NTSTATUS ( NTAPI* NtReadFile_t )(  HANDLE FileHandle, HANDLE Event, PIO_APC_ROUTINE ApcRoutine, PVOID ApcContext, PIO_STATUS_BLOCK IoStatusBlock, PVOID Buffer, ULONG Length, PLARGE_INTEGER ByteOffset, PULONG Key );
typedef NTSTATUS ( NTAPI* NtReadVirtualMemory_t )( HANDLE ProcessHandle, PVOID BaseAddress, PVOID Buffer, ULONG NumberOfBytesToRead, PULONG NumberOfBytesReaded );
typedef NTSTATUS ( NTAPI* NtResumeThread_t )( HANDLE ThreadHandle, PULONG SuspendCount );
typedef NTSTATUS ( NTAPI* NtSuspendThread_t )( HANDLE ThreadHandle, PULONG PreviousSuspendCount );
typedef NTSTATUS ( NTAPI* NtWriteFile_t )( HANDLE FileHandle, HANDLE Event, PIO_APC_ROUTINE ApcRoutine, PVOID ApcContext, PIO_STATUS_BLOCK IoStatusBlock, PVOID Buffer, ULONG Length, PLARGE_INTEGER ByteOffset, PULONG Key ); 
typedef NTSTATUS ( NTAPI* NtWriteVirtualMemory_t )( HANDLE hProcess, LPVOID lpBaseAddress, LPCVOID lpBuffer, SIZE_T nSize, SIZE_T *lpNumberOfBytesWritten );
typedef PVOID ( NTAPI* RtlAllocateHeap_t )( PVOID HeapHandle, ULONG Flags, ULONG Size );
typedef BOOLEAN ( NTAPI* RtlFreeHeap_t )( PVOID HeapHandle, ULONG Flags, PVOID MemoryPointer );
typedef NTSTATUS ( NTAPI* RtlGetVersion_t )( PRTL_OSVERSIONINFOW lpVersionInformation );
typedef ULONG ( NTAPI* RtlNtStatusToDosError_t )( NTSTATUS );
//========================================================================================		
extern GetCurrentProcess_t GetCurrentProcess;
//========================================================================================
extern MD5Final_t MD5Final;
extern MD5Init_t MD5Init;
extern MD5Update_t MD5Update;
//========================================================================================
extern NtAllocateVirtualMemory_t NtAllocateVirtualMemory;
extern NtClose_t NtClose;
extern NtCreateFile_t NtCreateFile;
extern NtDuplicateObject_t NtDuplicateObject;
extern NtFreeVirtualMemory_t NtFreeVirtualMemory;
extern NtGetContextThread_t NtGetContextThread;
extern NtOpenProcess_t NtOpenProcess;
extern NtOpenThread_t NtOpenThread;
extern NtQueryInformationFile_t NtQueryInformationFile;
extern NtQueryInformationProcess_t NtQueryInformationProcess;
extern NtQueryInformationThread_t NtQueryInformationThread;
extern NtQueryObject_t NtQueryObject;
extern NtQuerySystemInformation_t NtQuerySystemInformation;
extern NtQueryVirtualMemory_t NtQueryVirtualMemory;
extern NtReadFile_t NtReadFile;
extern NtReadVirtualMemory_t NtReadVirtualMemory;
extern NtResumeThread_t NtResumeThread;
extern NtSuspendThread_t NtSuspendThread;
extern NtWriteFile_t NtWriteFile;
extern NtWriteVirtualMemory_t NtWriteVirtualMemory;
//========================================================================================
extern RtlAllocateHeap_t RtlAllocateHeap;
extern RtlFreeHeap_t RtlFreeHeap;
extern RtlGetVersion_t RtlGetVersion;
extern RtlNtStatusToDosError_t RtlNtStatusToDosError;
//========================================================================================
BOOL __stdcall GetProcAddress ( HMODULE hModule, PDWORD pdwHash, PDWORD pdwOut, int iNumberOfFunctions );
//========================================================================================
DWORD __stdcall ResolveSingleImport ( HANDLE hProcess, DWORD dwModuleNameHash, DWORD dwExportHash );
//========================================================================================
DLL_IMPORT HANDLE WINAPI CreateThread ( LPSECURITY_ATTRIBUTES lpThreadAttributes, SIZE_T dwStackSize, LPTHREAD_START_ROUTINE lpStartAddress, LPVOID lpParameter, DWORD dwCreationFlags, LPDWORD lpThreadId );
//========================================================================================