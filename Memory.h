//========================================================================================
#pragma once
//========================================================================================
#include "NT.h"
#include "macros.h"
//========================================================================================
HANDLE GetProcessHeap ( void );
//========================================================================================
extern "C" void * __cdecl memcpy ( void*, const void*, size_t );
extern "C" void * __cdecl memset ( void*, int, size_t );
//========================================================================================
void* __cdecl memcpy ( void* _Dest, const void* _Source, size_t _Size );
void* __cdecl memset ( void* _Dest, int _Val, size_t _Size );
//========================================================================================
PVOID __stdcall AllocateFromHeap ( DWORD dwSize );
void __stdcall DeallocateOffHeap ( PVOID pvMemory );
//========================================================================================
PVOID __stdcall NtAllocateMemory ( HANDLE hProcess, DWORD dwBase, DWORD dwSize, DWORD dwProtection, NTSTATUS& status );
BOOL __stdcall NtFreeMemory ( HANDLE hProcess, DWORD dwBase, DWORD dwSize, DWORD dwFreeType, NTSTATUS& status );
//========================================================================================
typedef struct
{
	DWORD m_dwBase;
	DWORD m_dwSize;
	DWORD m_dwType;
	DWORD m_dwPadding;
}Memory_Region_t;
//========================================================================================
extern HANDLE g_hProcessHeap;
//========================================================================================