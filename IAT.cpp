//========================================================================================
#include "IAT.h"
//========================================================================================
#include "FuncLen.h"
#include "hde32.h"
#include "Memory.h"
#include "Packet.h"
#include "Scanner.h"
#include "StatusCodes.h"
#include "Strings.h"
//========================================================================================
bool g_bSetupFnPtrs;
//========================================================================================		
GetCurrentProcess_t GetCurrentProcess;
//========================================================================================
MD5Final_t MD5Final;
MD5Init_t MD5Init;
MD5Update_t MD5Update;
//========================================================================================
NtAllocateVirtualMemory_t NtAllocateVirtualMemory;
NtClose_t NtClose;
NtCreateFile_t NtCreateFile;
NtDuplicateObject_t NtDuplicateObject;
NtFreeVirtualMemory_t NtFreeVirtualMemory;
NtGetContextThread_t NtGetContextThread;
NtOpenProcess_t NtOpenProcess;
NtOpenThread_t NtOpenThread;
NtQueryInformationFile_t NtQueryInformationFile;
NtQueryInformationProcess_t NtQueryInformationProcess;
NtQueryInformationThread_t NtQueryInformationThread;
NtQueryObject_t NtQueryObject;
NtQuerySystemInformation_t NtQuerySystemInformation;
NtQueryVirtualMemory_t NtQueryVirtualMemory;
NtReadFile_t NtReadFile;
NtReadVirtualMemory_t NtReadVirtualMemory;
NtResumeThread_t NtResumeThread;
NtSuspendThread_t NtSuspendThread;
NtWriteFile_t NtWriteFile;
NtWriteVirtualMemory_t NtWriteVirtualMemory;
//========================================================================================
RtlAllocateHeap_t RtlAllocateHeap;
RtlFreeHeap_t RtlFreeHeap;
RtlGetVersion_t RtlGetVersion;
RtlNtStatusToDosError_t RtlNtStatusToDosError;
//========================================================================================
DWORD g_dwCoreDLLFunctions [ IMPORT_END ];
//========================================================================================
DWORD g_dwIATCRC;
//========================================================================================
DWORD g_dwDLLFunctions[] =
{
	BLOCK_BEGIN,						// these 2 DWORDs signifies the beginning of an import block
	BLOCK_BEGIN_1,
	NTDLL_HASH,							// module name hash for ntdll
	MD5Final_HASH,
	MD5Init_HASH,
	MD5Update_HASH,
	NtAllocateVirtualMemory_HASH,
	NtClose_HASH,
	NtCreateDirectoryObject_HASH,
	NtCreateEvent_HASH,
	NtCreateFile_HASH,
	NtCreateKey_HASH,
	NtCreateSection_HASH,
	NtDeleteFile_HASH,
	NtDeleteKey_HASH,
	NtDuplicateObject_HASH,
	NtDuplicateToken_HASH,
	NtEnumerateKey_HASH,
	NtEnumerateValueKey_HASH,
	NtFlushBuffersFile_HASH,
//	NtFlushBuffersFileEx_HASH,
	NtFlushKey_HASH,
	NtFlushVirtualMemory_HASH,
	NtFreeVirtualMemory_HASH,
	NtGetContexThread_HASH,
	NtLockFile_HASH,
	NtLockVirtualMemory_HASH,
	NtMapViewOfSection_HASH,
	NtNotifyChangeKey_HASH,
	NtOpenFile_HASH,
	NtOpenKey_HASH,
	NtOpenProcess_HASH,
	NtOpenProcessTokenEx_HASH,
	NtOpenSection_HASH,
	NtOpenThread_HASH,
	NtProtectVirtualMemory_HASH,
	NtQueryDirectoryFile_HASH,
	NtQueryInformationFile_HASH,
	NtQueryInformationProcess_HASH,
	NtQueryInformationThread_HASH,
	NtQueryInformationToken_HASH,
	NtQueryKey_HASH,
	NtQueryObject_HASH,
	NtQuerySection_HASH,
	NtQuerySecurityObject_HASH,
	NtQuerySystemInformation_HASH,
	NtQueryValueKey_HASH,
	NtQueryVirtualMemory_HASH,
	NtQueryVolumeInformationFile_HASH,
	NtReadFile_HASH,
	NtReadVirtualMemory_HASH,
	NtResumeThread_HASH,
	NtSetInformationToken_HASH,
	NtSetSecurityObject_HASH,
	NtSuspendThread_HASH,
	NtUnlockFile_HASH,
	NtUnlockVirtualMemory_HASH,
	NtUnmapViewOfSection_HASH,
	NtWriteFile_HASH,
	NtWriteVirtualMemory_HASH,
	RtlAllocateHeap_HASH,			
	RtlFreeHeap_HASH,
	RtlGetVersion_HASH,
	RtlNtStatusToDosError_HASH,
	BLOCK_END,							// this ends an import block
	BLOCK_END_1,

	BLOCK_BEGIN,
	BLOCK_BEGIN_1,
	KERNEL32_HASH,
	GetCurrentProcess_HASH,
	BLOCK_END,
	BLOCK_END_1,
};
//========================================================================================
void CopyToFn ( void )
{
	GetCurrentProcess = ( GetCurrentProcess_t )g_dwCoreDLLFunctions [ IMPORT_GetCurrentProcess ];
		
	MD5Final = ( MD5Final_t )g_dwCoreDLLFunctions [ IMPORT_MD5Final ];
	MD5Init = ( MD5Init_t )g_dwCoreDLLFunctions [ IMPORT_MD5Init ];
	MD5Update = ( MD5Update_t )g_dwCoreDLLFunctions [ IMPORT_MD5Update ];

	NtAllocateVirtualMemory = ( NtAllocateVirtualMemory_t )g_dwCoreDLLFunctions [ IMPORT_NtAllocateVirtualMemory ];
	NtClose = ( NtClose_t )g_dwCoreDLLFunctions [ IMPORT_NtClose ];
	NtCreateFile = ( NtCreateFile_t )g_dwCoreDLLFunctions [ IMPORT_NtCreateFile ];
	NtDuplicateObject = ( NtDuplicateObject_t )g_dwCoreDLLFunctions [ IMPORT_NtDuplicateObject ];
	NtFreeVirtualMemory = ( NtFreeVirtualMemory_t )g_dwCoreDLLFunctions [ IMPORT_NtFreeVirtualMemory ];
	NtGetContextThread = ( NtGetContextThread_t )g_dwCoreDLLFunctions [ IMPORT_NtGetContextThread ];
	NtOpenProcess = ( NtOpenProcess_t )g_dwCoreDLLFunctions [ IMPORT_NtOpenProcess ];
	NtOpenThread = ( NtOpenThread_t )g_dwCoreDLLFunctions [ IMPORT_NtOpenThread ];
	NtQueryInformationFile = ( NtQueryInformationFile_t )g_dwCoreDLLFunctions [ IMPORT_NtQueryInformationFile ];
	NtQueryInformationProcess = ( NtQueryInformationProcess_t )g_dwCoreDLLFunctions [ IMPORT_NtQueryInformationProcess ];
	NtQueryInformationThread = ( NtQueryInformationThread_t )g_dwCoreDLLFunctions [ IMPORT_NtQueryInformationThread ];
	NtQueryObject = ( NtQueryObject_t )g_dwCoreDLLFunctions [ IMPORT_NtQueryObject ];
	NtQuerySystemInformation = ( NtQuerySystemInformation_t )g_dwCoreDLLFunctions [ IMPORT_NtQuerySystemInformation ];
	NtQueryVirtualMemory = ( NtQueryVirtualMemory_t )g_dwCoreDLLFunctions [ IMPORT_NtQueryVirtualMemory ];
	NtReadFile = ( NtReadFile_t )g_dwCoreDLLFunctions [ IMPORT_NtReadFile ];
	NtReadVirtualMemory = ( NtReadVirtualMemory_t )g_dwCoreDLLFunctions [ IMPORT_NtReadVirtualMemory ];
	NtResumeThread = ( NtResumeThread_t )g_dwCoreDLLFunctions [ IMPORT_NtResumeThread ];
	NtSuspendThread = ( NtSuspendThread_t )g_dwCoreDLLFunctions [ IMPORT_NtSuspendThread ];
	NtWriteFile = ( NtWriteFile_t )g_dwCoreDLLFunctions [ IMPORT_NtWriteFile ];
	NtWriteVirtualMemory = ( NtWriteVirtualMemory_t )g_dwCoreDLLFunctions [ IMPORT_NtWriteVirtualMemory ];

	RtlAllocateHeap = ( RtlAllocateHeap_t )g_dwCoreDLLFunctions [ IMPORT_RtlAllocateHeap ];
	RtlFreeHeap = ( RtlFreeHeap_t )g_dwCoreDLLFunctions [ IMPORT_RtlFreeHeap ];
	RtlGetVersion = ( RtlGetVersion_t )g_dwCoreDLLFunctions [ IMPORT_RtlGetVersion ];
	RtlNtStatusToDosError = ( RtlNtStatusToDosError_t )g_dwCoreDLLFunctions [ IMPORT_RtlNtStatusToDosError ];
		
	return;
}
//========================================================================================
void __stdcall ustrcpytolower ( PUNICODE_STRING dst, PUNICODE_STRING src )
{
	size_t i;
	WCHAR c;
	
	dst->Length = src->Length;
	
	for ( i = 0; i < ( src->Length / sizeof ( WCHAR ) ); i++ )
	{
		c = src->Buffer[i];
		
		if ( c >= 0x41 && c <= 0x5A )
			c |= 0x20;
			
		dst->Buffer[i] = c;
	}
	
	dst->Buffer[i] = 0x00;
}
//========================================================================================
HMODULE __stdcall GetModuleHandle ( DWORD dwHash )
{
	PPROCESS_ENVIRONMENT_BLOCK peb;
	PLDR_DATA_TABLE_ENTRY module;
	PLIST_ENTRY entry;
	PLIST_ENTRY InLoadOrderModuleList;
	UNICODE_STRING basedllname;
	WCHAR buffer [ MAX_PATH ];
	
	basedllname.MaximumLength = MAX_PATH;
	basedllname.Buffer = ( PWSTR )buffer;
    
	peb = GetPEB();
    
	if ( peb != NULL )
	{
		InLoadOrderModuleList = &peb->Ldr->InLoadOrderModuleList;

		for ( entry = InLoadOrderModuleList->Flink; entry != NULL, entry != InLoadOrderModuleList; entry = entry->Flink )
		{ 
			module = CONTAINING_RECORD ( entry, LDR_DATA_TABLE_ENTRY, InLoadOrderModuleList );
            
			ustrcpytolower ( &basedllname, &module->BaseDllName );
            
			if ( dwHash == dwCRCBlock ( 0x0, basedllname.Buffer, basedllname.Length ) )
			{
				return ( HMODULE )module->BaseAddress;
			}
		}
	} 
    
	return ( HMODULE )NULL;
}
//========================================================================================
BOOL __stdcall GetProcAddress ( HMODULE hModule, PDWORD pdwHash, PDWORD pdwOut, int iNumberOfFunctions ) 
{
	BOOL bGotAPI, bGotDLL, bReturn;

	BYTE bStringChar;

	int iCurCounter, iForwardedExports, iIterator, iLength, iStringCounterAPI, iStringCounterDLL;

	WORD i;
	DWORD dwCRC, dwCRCAPI, dwExport, dwModuleBase, dwHash, dwName;

	HMODULE hModuleForwarded;
	    
	PIMAGE_DOS_HEADER doshdr;
	PIMAGE_NT_HEADERS nthdr;
	PIMAGE_OPTIONAL_HEADER32 opthdr;
	PIMAGE_DATA_DIRECTORY datadir;
	PIMAGE_EXPORT_DIRECTORY exportdir;
    
	PWORD  ordtable;
	PDWORD addrtable;
	PDWORD nametable;
    
	PCHAR pszProcname;

	char szAPI [ 64 ];
	char szDLL [ 64 ];
	char szName [ 128 ];

	wchar_t wszModuleName [ 64 ];

	iForwardedExports = 0;

	iStringCounterAPI = 0;
	iStringCounterDLL = 0;

	bGotAPI = 0;
	bGotDLL = 0;

	bReturn = 0;
    
	dwModuleBase = ( DWORD )hModule;
	doshdr = ( PIMAGE_DOS_HEADER )dwModuleBase;
    
	if ( doshdr->e_magic != IMAGE_DOS_SIGNATURE )
	{
		return bReturn;
	}

	nthdr = ( PIMAGE_NT_HEADERS ) ( dwModuleBase + doshdr->e_lfanew );
    
	if ( nthdr->Signature != IMAGE_NT_SIGNATURE )
	{
		return bReturn;
	}
        
	opthdr = &nthdr->OptionalHeader;
	datadir = &opthdr->DataDirectory [ IMAGE_DIRECTORY_ENTRY_EXPORT ];
	exportdir = ( PIMAGE_EXPORT_DIRECTORY )( dwModuleBase + datadir->VirtualAddress );

	ordtable = ( PWORD )( dwModuleBase + ( DWORD )exportdir->AddressOfNameOrdinals );
	addrtable = ( PDWORD )( dwModuleBase + ( DWORD )exportdir->AddressOfFunctions ); 
	nametable = ( PDWORD )( dwModuleBase + ( DWORD )exportdir->AddressOfNames ); 

	i = 0;
	iCurCounter = 0;
	iLength = 0;
	dwHash = 0;
	dwName = 0;
	
	for ( ; i < exportdir->NumberOfNames; i++ )
	{
		dwName = nametable [ i ];

		pszProcname = ( PCHAR )( dwModuleBase + dwName );

		iLength = strlen ( pszProcname );

		dwHash = dwCRCBlock ( 0x0, pszProcname, iLength );

		if ( iCurCounter < iNumberOfFunctions && pdwHash [ iCurCounter ] == dwHash )
		{
			pdwOut [ iCurCounter ] = ( DWORD )( dwModuleBase + addrtable [ ordtable [ i ] ] );

			dwExport = dwModuleBase + addrtable [ ordtable [ i ] ];

			iForwardedExports = 0;
			
			iStringCounterAPI = 0;
			iStringCounterDLL = 0;

			bGotAPI = 0;
			bGotDLL = 0;

			memset ( ( void* )szAPI, 0, sizeof ( szAPI ) );
			memset ( ( void* )szDLL, 0, sizeof ( szDLL ) );

			memcpy ( szName, ( const PVOID )dwExport, sizeof ( szName ) );
					
			for ( iIterator = 0; iIterator < sizeof ( szName ); iIterator++ )
			{
				bStringChar = szName [ iIterator ];

				if ( CharIsPrintable [ bStringChar ] != 0 )
				{
					if ( bGotDLL == 0 )
					{
						if ( bStringChar != '.' )
						{
							szDLL [ iStringCounterDLL ] = bStringChar;

							iStringCounterDLL++;
						}
						else
						{
							bGotDLL = 1;
						}
					}
					else
					{
						szAPI [ iStringCounterAPI ] = bStringChar;

						iStringCounterAPI++;
					}
				}
				else
				{
					break;
				}
			}
			if ( iStringCounterDLL >= 4 && iStringCounterAPI >= 5 )
			{				
				// store dwCRC and recall GetProcAddressEx

				szDLL [ iStringCounterDLL ] = '.';
				szDLL [ iStringCounterDLL + 1 ] = 'd';
				szDLL [ iStringCounterDLL + 2 ] = 'l';
				szDLL [ iStringCounterDLL + 3 ] = 'l';

				iStringCounterDLL += 3;

				for ( iIterator = 0; iIterator < iStringCounterDLL; iIterator++ )
				{
					if ( szDLL [ iIterator ] >= 'A' && szDLL [ iIterator ] <= 'Z' ) // lowercase
					{
						szDLL [ iIterator ] |= ' ';
					}
				}

				if ( AsciiToUnicode ( szDLL, wszModuleName ) )
				{
					dwCRCAPI = dwCRCBlock ( 0, ( PVOID )szAPI, iStringCounterAPI );
						
					iStringCounterDLL = wstrlen ( wszModuleName );

					dwCRC = dwCRCBlock ( 0, ( PVOID )wszModuleName, iStringCounterDLL );

					hModuleForwarded = GetModuleHandle ( dwCRC );
																						
					if ( hModuleForwarded == 0 || !GetProcAddress ( hModuleForwarded, &dwCRCAPI, &pdwOut [ iCurCounter ], 1 ) )
					{
						bReturn = 0;
					}

					iForwardedExports++;
				}
			}
				
			iCurCounter++;
		}
		else
		{
			continue;
		}
	}

	if ( iCurCounter >= iNumberOfFunctions )
	{
		bReturn = 1;
	}

	return bReturn;
}
//========================================================================================
BOOL __stdcall GetProcAddressEx ( HANDLE hProcess, HMODULE hModule, PDWORD pdwHash, PDWORD pdwOut, int iNumberOfFunctions ) 
{
	BOOL bGotAPI, bGotDLL, bReturn;

	unsigned char bStringChar;

	unsigned int iCurCounter, iForwardedExports, iIterator, iImportIterator, iLength, iStringCounterAPI, iStringCounterDLL;

	WORD ordinal;
	DWORD dwAddress, dwBytesRead, dwCRC, dwCRCAPI, dwModuleBase, dwHash, dwExport, dwName;
	
	NTSTATUS status;

	HANDLE hCurrentProcess;

	HMODULE hModuleForwarded;
	    
	IMAGE_DOS_HEADER doshdr;
	IMAGE_NT_HEADERS nthdr;
	IMAGE_OPTIONAL_HEADER32 opthdr;
	IMAGE_DATA_DIRECTORY datadir;
	IMAGE_EXPORT_DIRECTORY exportdir;

	PIMAGE_DOS_HEADER pDosHdr;
	PIMAGE_NT_HEADERS pNthdr;
	PIMAGE_OPTIONAL_HEADER32 pOpthdr;
	PIMAGE_DATA_DIRECTORY pDatadir;
	PIMAGE_EXPORT_DIRECTORY pExportdir;
    
	PWORD  ordtable;
	PDWORD addrtable;
	PDWORD nametable;
    
	PCHAR pszProcname;

	char szAPI [ 64 ];
	char szDLL [ 64 ];
	char szName [ 128 ];

	wchar_t wszModuleName [ 64 ];
    
	dwModuleBase = ( DWORD )hModule;

	dwBytesRead = 0;
	
	bReturn = 0;

	iImportIterator = 0;

	hCurrentProcess = GetCurrentProcess();
		
	status = NtReadVirtualMemory ( hProcess, ( PVOID )dwModuleBase, &doshdr, sizeof ( IMAGE_DOS_HEADER ), &dwBytesRead );

	if ( NT_SUCCESS ( status ) )
	{    
		if ( doshdr.e_magic != IMAGE_DOS_SIGNATURE )
		{
			return bReturn;
		}
		
		pNthdr = ( PIMAGE_NT_HEADERS )( dwModuleBase + doshdr.e_lfanew );

		status = NtReadVirtualMemory ( hProcess, ( PVOID )pNthdr, &nthdr, sizeof ( IMAGE_NT_HEADERS ), &dwBytesRead );

		if ( NT_SUCCESS ( status ) )
		{      
			if ( nthdr.Signature != IMAGE_NT_SIGNATURE )
			{
				return bReturn;
			}

			pOpthdr = &pNthdr->OptionalHeader;

			status = NtReadVirtualMemory ( hProcess, ( PVOID )pOpthdr, &opthdr, sizeof ( IMAGE_OPTIONAL_HEADER32 ), &dwBytesRead );

			if ( !NT_SUCCESS ( status ) )
			{
				return bReturn;
			}
						
			pDatadir = ( PIMAGE_DATA_DIRECTORY )( dwModuleBase + opthdr.DataDirectory [ IMAGE_DIRECTORY_ENTRY_EXPORT ].VirtualAddress );

			status = NtReadVirtualMemory ( hProcess, ( PVOID )pDatadir, &exportdir, sizeof ( IMAGE_EXPORT_DIRECTORY ), &dwBytesRead );

			if ( !NT_SUCCESS ( status ) )
			{
				return bReturn;
			}

			ordtable = ( PWORD )( dwModuleBase + ( DWORD )exportdir.AddressOfNameOrdinals );
			addrtable = ( PDWORD )( dwModuleBase + ( DWORD )exportdir.AddressOfFunctions ); 
			nametable = ( PDWORD )( dwModuleBase + ( DWORD )exportdir.AddressOfNames ); 

			iCurCounter = 0;
			iLength = 0;
			dwHash = 0;
			dwName = 0;
				
			for ( iIterator = 0; iIterator < exportdir.NumberOfNames; iIterator++ )
			{
				status = NtReadVirtualMemory ( hProcess, ( PVOID )&nametable [ iIterator ], &dwName, sizeof ( DWORD ), &dwBytesRead );

				if ( !NT_SUCCESS ( status ) )
				{
					return bReturn;
				}
				
				status = NtReadVirtualMemory ( hProcess, ( PVOID )( dwModuleBase + dwName ), szName, sizeof ( szName ), &dwBytesRead );

				if ( !NT_SUCCESS ( status ) )
				{
					return bReturn;
				}

				pszProcname = szName;

				iLength = strlen ( pszProcname );

				dwHash = dwCRCBlock ( 0x0, pszProcname, iLength );
												
				if ( iCurCounter < iNumberOfFunctions && pdwHash [ iCurCounter ] == dwHash )
				{
					status = NtReadVirtualMemory ( hProcess, ( PVOID )&ordtable [ iIterator ], &ordinal, sizeof ( WORD ), &dwBytesRead );

					if ( !NT_SUCCESS ( status ) )
					{
						return bReturn;
					}

					status = NtReadVirtualMemory ( hProcess, ( PVOID )&addrtable [ ordinal ], &dwAddress, sizeof ( dwAddress ), &dwBytesRead );

					if ( !NT_SUCCESS ( status ) )
					{
						return bReturn;
					}

					dwExport = dwModuleBase + dwAddress;
					
					pdwOut [ iCurCounter ] = dwExport; 

					// check the dwExport to make sure it isn't forwarded

					memset ( ( void* )szName, 0, sizeof ( szName ) );

					status = NtReadVirtualMemory ( hProcess, ( PVOID )dwExport, &szName, sizeof ( szName ), &dwBytesRead );

					if ( !NT_SUCCESS ( status ) )
					{
						return bReturn;
					}

					iForwardedExports = 0;

					iStringCounterAPI = 0;
					iStringCounterDLL = 0;

					bGotAPI = 0;
					bGotDLL = 0;

					memset ( ( void* )szAPI, 0, sizeof ( szAPI ) );
					memset ( ( void* )szDLL, 0, sizeof ( szDLL ) );
					
					for ( iIterator = 0; iIterator < sizeof ( szName ); iIterator++ )
					{
						bStringChar = szName [ iIterator ];

						if ( CharIsPrintable [ bStringChar ] != 0 )
						{
							if ( bGotDLL == 0 )
							{
								if ( bStringChar != '.' )
								{
									szDLL [ iStringCounterDLL ] = bStringChar;

									iStringCounterDLL++;
								}
								else
								{
									bGotDLL = 1;
								}
							}
							else
							{
								szAPI [ iStringCounterAPI ] = bStringChar;

								iStringCounterAPI++;
							}
						}
						else
						{
							break;
						}
					}

					if ( iStringCounterDLL >= 4 && iStringCounterAPI >= 5 )
					{
						// store dwCRC and recall GetProcAddressEx

						szDLL [ iStringCounterDLL ] = '.';
						szDLL [ iStringCounterDLL + 1 ] = 'd';
						szDLL [ iStringCounterDLL + 2 ] = 'l';
						szDLL [ iStringCounterDLL + 3 ] = 'l';

						iStringCounterDLL += 3;

						for ( iIterator = 0; iIterator < iStringCounterDLL; iIterator++ )
						{
							if ( szDLL [ iIterator ] >= 'A' && szDLL [ iIterator ] <= 'Z' ) // lowercase
							{
								szDLL [ iIterator ] |= ' ';
							}
						}

						if ( AsciiToUnicode ( szDLL, wszModuleName ) )
						{
							dwCRCAPI = dwCRCBlock ( 0, ( PVOID )szAPI, iStringCounterAPI );
							
							iStringCounterDLL = wstrlen ( wszModuleName );

							dwCRC = dwCRCBlock ( 0, ( PVOID )wszModuleName, iStringCounterDLL );

							hModuleForwarded = GetModuleHandleEx ( hCurrentProcess, hProcess, dwCRC );
							
							if ( hModuleForwarded == 0 || !GetProcAddressEx ( hProcess, hModuleForwarded, &dwCRCAPI, &pdwOut [ iCurCounter ], 1 ) )
							{
								return bReturn;
							}

							iForwardedExports++;
						}
					}
					
					iCurCounter++;
				}
				else 
				{
					continue;
				}
			}
		}
	}

	if ( iCurCounter >= iNumberOfFunctions )
	{
		bReturn = 1;
	}

	return bReturn;
}
//========================================================================================
// A little information on this one, ResolveImports takes 3 arguments, an import block descriptor and the sizeof the block descriptor
// and the buffer to copy the resolved imports to.  For anti cheat reasons I try to limit the lookups to strictly ntdll so we can perform
// direct syscalls or copy the opcodes to our module and call that instead so we can avoid hooks.
// Of course the main reason is to slow analysis by needing only one iteration through the peb list of linked modules
// Ideally in the future we should defintely think of using the anti cheat process to provide us with the OS version
// so the server can construct a tailor made module for that OS, eg XP, so we don't have to do import lookups at all.
// For windows 64 bit we can check the thunk into kernel mode for hooks.
//========================================================================================
BOOL __stdcall ResolveImports ( PDWORD pdwImportBlock, int iBlockSize, PDWORD pdwBufferOut )
{
	BOOL bReturn;
	DWORD dwModuleBase, dwModuleHash;
	int iCounter, iIterator, iTotalImports;
	PDWORD pdwBlock;

	dwModuleBase = 0;
	iCounter = 0;
	iIterator = 0;
	iTotalImports = 0;
	bReturn = 0;

	pdwBlock = pdwImportBlock;

	for ( ; iIterator < iBlockSize; iIterator++, pdwBlock++ )
	{
		if ( pdwImportBlock [ iIterator ] == BLOCK_BEGIN && pdwImportBlock [ iIterator + 1 ] == BLOCK_BEGIN_1 )
		{
			pdwBlock++;
			pdwBlock++;

			dwModuleHash = *pdwBlock;

			dwModuleBase = ( DWORD )GetModuleHandle ( dwModuleHash );
			
			pdwBlock++;

			// now we need to determine the number of imports we're going to be looking up for this import block

			for ( iCounter = 0; pdwBlock [ iCounter ] != BLOCK_END && pdwBlock [ iCounter + 1 ] != BLOCK_END_1; iCounter++ )
			{				
				iTotalImports++;
			}
			if ( GetProcAddress ( ( HMODULE )dwModuleBase, pdwBlock, pdwBufferOut, iTotalImports ) ) 
			{
				bReturn = 1;
			}
			else
			{
				bReturn = 0;

				break;
			}

			pdwBufferOut += iTotalImports; // move the buffer forward

			iIterator += iTotalImports + 4; // skip over to the next import block entry
			pdwBlock += iTotalImports + 1;

			dwModuleHash = 0;

			iTotalImports = 0;
		}
	}
		
	memset ( pdwImportBlock, 0, iBlockSize ); // clear out the import block descriptor so a memory dump won't get it

	return bReturn;
}
//========================================================================================
DWORD __stdcall GetSingleImport ( HANDLE hProcess, HMODULE hModule, DWORD dwExportHash )
{
	BOOL bGotAPI, bGotDLL, bReturn;

	unsigned char bStringChar;

	unsigned int iCurCounter, iForwardedExports, iIterator, iImportIterator, iLength, iStringCounterAPI, iStringCounterDLL;

	WORD i;
	WORD ordinal;
	DWORD dwAddress, dwBytesRead, dwCRC, dwCRCAPI, dwModuleBase, dwHash, dwExport, dwName;
	
	NTSTATUS status;

	HANDLE hCurrentProcess;

	HMODULE hModuleForwarded;
	    
	IMAGE_DOS_HEADER doshdr;
	IMAGE_NT_HEADERS nthdr;
	IMAGE_OPTIONAL_HEADER32 opthdr;
	IMAGE_DATA_DIRECTORY datadir;
	IMAGE_EXPORT_DIRECTORY exportdir;

	PIMAGE_DOS_HEADER pDosHdr;
	PIMAGE_NT_HEADERS pNthdr;
	PIMAGE_OPTIONAL_HEADER32 pOpthdr;
	PIMAGE_DATA_DIRECTORY pDatadir;
	PIMAGE_EXPORT_DIRECTORY pExportdir;
    
	PWORD  ordtable;
	PDWORD addrtable;
	PDWORD nametable;
    
	PCHAR pszProcname;

	char szAPI [ 64 ];
	char szDLL [ 64 ];
	char szName [ 128 ];

	wchar_t wszModuleName [ 64 ];
    
	dwModuleBase = ( DWORD )hModule;

	dwBytesRead = 0;
	
	bReturn = 0;

	iImportIterator = 0;

	hCurrentProcess = GetCurrentProcess();
		
	status = NtReadVirtualMemory ( hProcess, ( PVOID )dwModuleBase, &doshdr, sizeof ( IMAGE_DOS_HEADER ), &dwBytesRead );

	if ( NT_SUCCESS ( status ) )
	{    
		if ( doshdr.e_magic != IMAGE_DOS_SIGNATURE )
		{
			return 0;
		}
		
		pNthdr = ( PIMAGE_NT_HEADERS )( dwModuleBase + doshdr.e_lfanew );

		status = NtReadVirtualMemory ( hProcess, ( PVOID )pNthdr, &nthdr, sizeof ( IMAGE_NT_HEADERS ), &dwBytesRead );

		if ( NT_SUCCESS ( status ) )
		{      
			if ( nthdr.Signature != IMAGE_NT_SIGNATURE )
			{
				return bReturn;
			}

			pOpthdr = &pNthdr->OptionalHeader;

			status = NtReadVirtualMemory ( hProcess, ( PVOID )pOpthdr, &opthdr, sizeof ( IMAGE_OPTIONAL_HEADER32 ), &dwBytesRead );

			if ( !NT_SUCCESS ( status ) )
			{
				return bReturn;
			}
						
			pDatadir = ( PIMAGE_DATA_DIRECTORY )( dwModuleBase + opthdr.DataDirectory [ IMAGE_DIRECTORY_ENTRY_EXPORT ].VirtualAddress );

			status = NtReadVirtualMemory ( hProcess, ( PVOID )pDatadir, &exportdir, sizeof ( IMAGE_EXPORT_DIRECTORY ), &dwBytesRead );

			if ( !NT_SUCCESS ( status ) )
			{
				return bReturn;
			}

			ordtable = ( PWORD )( dwModuleBase + ( DWORD )exportdir.AddressOfNameOrdinals );
			addrtable = ( PDWORD )( dwModuleBase + ( DWORD )exportdir.AddressOfFunctions ); 
			nametable = ( PDWORD )( dwModuleBase + ( DWORD )exportdir.AddressOfNames ); 

			i = 0;
			iCurCounter = 0;
			iLength = 0;
			dwHash = 0;
			dwName = 0;
				
			for ( ; i < exportdir.NumberOfNames; i++ )
			{
				status = NtReadVirtualMemory ( hProcess, ( PVOID )&nametable [ i ], &dwName, sizeof ( DWORD ), &dwBytesRead );

				if ( !NT_SUCCESS ( status ) )
				{
					return 0;
				}
				
				status = NtReadVirtualMemory ( hProcess, ( PVOID )( dwModuleBase + dwName ), szName, sizeof ( szName ), &dwBytesRead );

				if ( !NT_SUCCESS ( status ) )
				{
					return 0;
				}

				pszProcname = szName;

				iLength = strlen ( pszProcname );
								
				dwHash = dwCRCBlock ( 0x0, pszProcname, iLength );

				if ( dwExportHash == dwHash )
				{
					status = NtReadVirtualMemory ( hProcess, ( PVOID )&ordtable [ i ], &ordinal, sizeof ( WORD ), &dwBytesRead );

					if ( !NT_SUCCESS ( status ) )
					{
						return 0;
					}

					status = NtReadVirtualMemory ( hProcess, ( PVOID )&addrtable [ ordinal ], &dwAddress, sizeof ( dwAddress ), &dwBytesRead );

					if ( !NT_SUCCESS ( status ) )
					{
						return 0;
					}

					dwExport = dwModuleBase + dwAddress;
					
					// check the dwExport to make sure it isn't forwarded

					memset ( ( void* )szName, 0, sizeof ( szName ) );

					status = NtReadVirtualMemory ( hProcess, ( PVOID )dwExport, &szName, sizeof ( szName ), &dwBytesRead );

					if ( !NT_SUCCESS ( status ) )
					{
						return 0;
					}

					iForwardedExports = 0;

					iStringCounterAPI = 0;
					iStringCounterDLL = 0;

					bGotAPI = 0;
					bGotDLL = 0;

					memset ( ( void* )szAPI, 0, sizeof ( szAPI ) );
					memset ( ( void* )szDLL, 0, sizeof ( szDLL ) );
					
					for ( iIterator = 0; iIterator < sizeof ( szName ); iIterator++ )
					{
						bStringChar = szName [ iIterator ];

						if ( CharIsPrintable [ bStringChar ] != 0 )
						{
							if ( bGotDLL == 0 )
							{
								if ( bStringChar != '.' )
								{
									szDLL [ iStringCounterDLL ] = bStringChar;

									iStringCounterDLL++;
								}
								else
								{
									bGotDLL = 1;
								}
							}
							else
							{
								szAPI [ iStringCounterAPI ] = bStringChar;

								iStringCounterAPI++;
							}
						}
						else
						{
							break;
						}
					}

					if ( iStringCounterDLL >= 4 && iStringCounterAPI >= 5 )
					{
						// store dwCRC and recall GetProcAddressEx

						szDLL [ iStringCounterDLL ] = '.';
						szDLL [ iStringCounterDLL + 1 ] = 'd';
						szDLL [ iStringCounterDLL + 2 ] = 'l';
						szDLL [ iStringCounterDLL + 3 ] = 'l';

						iStringCounterDLL += 3;

						for ( iIterator = 0; iIterator < iStringCounterDLL; iIterator++ )
						{
							if ( szDLL [ iIterator ] >= 'A' && szDLL [ iIterator ] <= 'Z' ) // lowercase
							{
								szDLL [ iIterator ] |= ' ';
							}
						}

						if ( AsciiToUnicode ( szDLL, wszModuleName ) )
						{
							dwCRCAPI = dwCRCBlock ( 0, ( PVOID )szAPI, iStringCounterAPI );
							
							iStringCounterDLL = wstrlen ( wszModuleName );

							dwCRC = dwCRCBlock ( 0, ( PVOID )wszModuleName, iStringCounterDLL );

							hModuleForwarded = GetModuleHandleEx ( hCurrentProcess, hProcess, dwCRC );
							
							if ( hModuleForwarded == 0 )
							{
								return 0;
							}

							dwExport = 0;

							dwExport = GetSingleImport ( hProcess, hModuleForwarded, dwCRCAPI );
							
							if ( dwExport != 0 )
							{
								return dwExport;
							}

							iForwardedExports++;
						}
					}
					
					iCurCounter++;
				}
				else 
				{
					continue;
				}
			}
		}
	}

	return dwExport;
}
//========================================================================================
DWORD __stdcall ResolveSingleImport ( HANDLE hProcess, DWORD dwModuleNameHash, DWORD dwExportHash )
{
	HANDLE hCurrentProcess;

	DWORD dwModuleBase;

	hCurrentProcess = GetCurrentProcess();

	dwModuleBase = ( DWORD )GetModuleHandleEx ( hCurrentProcess, hProcess, dwModuleNameHash );

	return GetSingleImport ( hProcess, ( HMODULE )dwModuleBase, dwExportHash );
}
//========================================================================================
BOOL __stdcall ResolveImportsEx ( HANDLE hProcess, PDWORD pdwImportBlock, int iBlockSize, PDWORD pdwBufferOut )
{
	BOOL bReturn;
	DWORD dwModuleBase, dwModuleHash;
	HANDLE hCurrentProcess;
	int iCounter, iIterator, iTotalImports;
	PDWORD pdwBlock;

	dwModuleBase = 0;
	iCounter = 0;
	iIterator = 0;
	iTotalImports = 0;
	bReturn = 0;

	pdwBlock = pdwImportBlock;

	hCurrentProcess = GetCurrentProcess();

	for ( ; iIterator < iBlockSize; iIterator++, pdwBlock++ )
	{
		if ( pdwImportBlock [ iIterator ] == BLOCK_BEGIN && pdwImportBlock [ iIterator + 1 ] == BLOCK_BEGIN_1 )
		{
			pdwBlock++;
			pdwBlock++;

			dwModuleHash = *pdwBlock;

			dwModuleBase = ( DWORD )GetModuleHandleEx ( hCurrentProcess, hProcess, dwModuleHash );
			
			pdwBlock++;

			// now we need to determine the number of imports we're going to be looking up for this import block

			for ( iCounter = 0; pdwBlock [ iCounter ] != BLOCK_END && pdwBlock [ iCounter + 1 ] != BLOCK_END_1; iCounter++ )
			{				
				iTotalImports++;
			}
			if ( GetProcAddressEx ( hProcess, ( HMODULE )dwModuleBase, pdwBlock, pdwBufferOut, iTotalImports ) ) 
			{
				bReturn = 1;
			}
			else
			{
				bReturn = 0;

				break;
			}

			pdwBufferOut += iTotalImports; // move the buffer forward

			iIterator += iTotalImports + 4; // skip over to the next import block entry
			pdwBlock += iTotalImports + 1;

			dwModuleHash = 0;

			iTotalImports = 0;
		}
	}
	
	memset ( pdwImportBlock, 0, iBlockSize ); // clear out the import block descriptor so a memory dump won't get it

	return bReturn;
}
//========================================================================================
BOOL __stdcall IAT ( int iArg, Validation_Response_Packet_t* Validation_Response_Packet )
{
	BOOL bReturn;
	DWORD dwCRC;

	bReturn = 0;
	dwCRC = 0;
	
	if ( g_hProcessHeap == 0 )
		g_hProcessHeap = GetHeap();

	switch ( iArg )
	{
		case RESOLVE_IAT:
		{
			bReturn = ResolveImports ( g_dwDLLFunctions, sizeof ( g_dwDLLFunctions ), g_dwCoreDLLFunctions );
			
			CopyToFn();
		}
		break;

		case CHECKSUM_IAT:
		{
			dwCRC = dwCRCBlock ( dwCRC, g_dwCoreDLLFunctions, sizeof ( g_dwCoreDLLFunctions ) );

			g_dwIATCRC = dwCRC;

			Validation_Response_Packet->m_dwIATCRC = dwCRC;

			bReturn = 1;
		}
		break;

		default:
		{

		}
		break;
	}
				
	return bReturn;
}
//========================================================================================
#define WIDTH 32
#define POLYNOMIAL ( 0x488781ED )
#define TOPBIT ( 1 << ( WIDTH - 1 ) )
//========================================================================================
DWORD __stdcall dwCRCBlock ( DWORD dwRemainder, PVOID pvBlock, int iBlockSize ) // Miranda
{
	int iBit, iByte;
	PBYTE pbBlock;

	iBit = 0;
	iByte = 0;

	pbBlock = ( PBYTE )pvBlock;
	
	for ( iByte = 0; iByte < iBlockSize; iByte++ ) 
	{
		dwRemainder ^= ( pbBlock [ iByte ] << ( WIDTH - 8 ) );

		for ( iBit = 8; iBit > 0; --iBit) 
		{
			if ( dwRemainder & TOPBIT )
			{
				dwRemainder = ( dwRemainder << 1 ) ^ POLYNOMIAL;
			}
			else
			{
				dwRemainder = ( dwRemainder << 1 );
			}
		}
	}

	return dwRemainder;
}
/*
#pragma optimize( "gsty", off )

//#define HASH(str) HashString (str,strlen(str))

//========================================================================================
#define H1(s,i,x)   (x*65599u+(uint8_t)s[(i)<strlen(s)?strlen(s)-1-(i):strlen(s)])
#define H4(s,i,x)   H1(s,i,H1(s,i+1,H1(s,i+2,H1(s,i+3,x))))
#define H16(s,i,x)  H4(s,i,H4(s,i+4,H4(s,i+8,H4(s,i+12,x))))
#define H64(s,i,x)  H16(s,i,H16(s,i+16,H16(s,i+32,H16(s,i+48,x))))
#define H256(s,i,x) H64(s,i,H64(s,i+64,H64(s,i+128,H64(s,i+192,x))))

#define HASH(s)    ((uint32_t)(H256(s,0,0)^(H256(s,0,0)>>16)))

#pragma optimize( "gsty", on ) 
*/
//========================================================================================
BOOL __stdcall SetupNewSection ( void )
{
	BOOL bReturn;

	DWORD dwCurrentAddress, dwFunction, dwSectionEnd;

	int iFunctionLength, iIterator;
	
	bReturn = 0;
	dwCurrentAddress = 0;
	dwFunction = 0;
	iFunctionLength = 0;

	bReturn = 0;

	

//	dwCurrentAddress = HASH("THISSHOULDNOTAPPEAR");

	

	dwCurrentAddress = ( DWORD )Initialize;

	

	dwSectionEnd = ( DWORD )Initialize + 0x8000;

	// copy all ntdll stuff 

	for ( iIterator = 0; iIterator < IMPORT_END; iIterator++ )
	{
		iFunctionLength = GetFunctionLength ( ( PVOID )g_dwCoreDLLFunctions [ iIterator ], 1, 1, &g_dwCoreDLLFunctions [ iIterator ], 0, 0, dwCurrentAddress );

		if ( ( dwCurrentAddress + iFunctionLength ) > dwSectionEnd )
		{
			return bReturn;
		}

		dwCurrentAddress += iFunctionLength;
	}

	CopyToFn();
		
	bReturn = 1;

	return bReturn;
}
//========================================================================================
DWORD g_dwLen = 0;
//========================================================================================
//DWORD WINAPI dwThread ( LPVOID lpArgs )
//========================================================================================
DLL_EXPORT BOOL __stdcall StartupFunction ( Validation_Response_Packet_t* Validation_Response_Packet )
{
	BOOL bReturn;

//	PDWORD pdwBufferIn; 
//	PDWORD pdwBufferOut; 
	
	DWORD dwCRC, dwGlobalCRC;

	dwCRC = 0;
	dwGlobalCRC = 0;

	HANDLE hCurrentProcess;
	
	if ( 1 /*Initialize()*/ )
	{
		bReturn = IAT ( RESOLVE_IAT, Validation_Response_Packet );

		hCurrentProcess = GetCurrentProcess();

		if ( bReturn )
		{
			if ( g_pdwBranches == NULL )
			{
				g_pdwBranches = ( PDWORD )NtAllocateMemory ( hCurrentProcess, 0, 0x4000, PAGE_READWRITE, g_NtStatus );

				if ( g_pdwBranches == NULL )
				{
					SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus ); 

					return 0;
				}
			}
			if ( g_pdwInstructions == NULL )
			{
				g_pdwInstructions = ( PDWORD )NtAllocateMemory ( hCurrentProcess, 0, 0x8000, PAGE_READWRITE, g_NtStatus );

				if ( g_pdwInstructions == NULL )
				{
					SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus ); 

					return 0;
				}
			}
			if ( g_pdwFixedFunction == NULL )
			{
				g_pdwFixedFunction = ( PDWORD )NtAllocateMemory ( hCurrentProcess, 0, 0x8000, PAGE_READWRITE, g_NtStatus );

				if ( g_pdwFixedFunction == NULL )
				{
					SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus ); 

					return 0;
				}
			}
			if ( g_pdwCopiedInstructions == NULL )
			{
				g_pdwCopiedInstructions = ( PDWORD )NtAllocateMemory ( hCurrentProcess, 0, 0x8000, PAGE_READWRITE, g_NtStatus );

				if ( g_pdwCopiedInstructions == NULL )
				{
					SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus ); 

					return 0;
				}
			}
			if ( g_pJumpTable == NULL )
			{
				g_pJumpTable = ( JumpTable_t* )NtAllocateMemory ( hCurrentProcess, 0, 0x4000, PAGE_READWRITE, g_NtStatus );

				if ( g_pJumpTable == NULL )
				{
					SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus ); 

					return 0;
				}
			}
			if ( g_pBlock == NULL )
			{
				g_pBlock = ( Block_t* )NtAllocateMemory ( hCurrentProcess, 0, 0x4000, PAGE_READWRITE, g_NtStatus );

				if ( g_pBlock == NULL )
				{
					SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus ); 

					return 0;
				}
			}
			if ( g_pvMemoryAllocatedBase == NULL )
			{
				g_pvMemoryAllocatedBase = NtAllocateMemory ( hCurrentProcess, 0, 0x5000000, PAGE_READWRITE, g_NtStatus );

				if ( g_pvMemoryAllocatedBase == NULL )
				{
					SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus ); 

					return 0;
				}
			}
			if ( g_pvFileAllocatedBase == NULL )
			{
				g_pvFileAllocatedBase = NtAllocateMemory ( hCurrentProcess, 0, 0x5000000, PAGE_READWRITE, g_NtStatus );

				if ( g_pvFileAllocatedBase == NULL )
				{
					SetStatus ( MEMORY_ALLOCATE_ERROR, g_NtStatus ); 

					return 0;
				}
			}
			if ( SetupNewSection() != 1 )
			{
				return 0;
			}

			// report the MD5 of the new section serverside

			bReturn = IAT ( CHECKSUM_IAT, Validation_Response_Packet );

			if ( bReturn == 0 )
			{
				return 0;
			}

			ProcessBufferMD5 ( MD5Context, ( PBYTE )Initialize, 0x8000 );

			memcpy ( Validation_Response_Packet->m_dwMD5HashOfSection, MD5Context.buf, sizeof ( MD5Context.buf ) ); // send this serverside
		}
	}

	return 1;
}
//========================================================================================
BOOL __stdcall Unload ( void ) // unload all memory
{
	return 0;
}
//========================================================================================
/*
bool WINAPI DllMain ( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved ) // removed in actual release
{
	if ( fdwReason == DLL_PROCESS_ATTACH )
	{
//		CreateThread ( NULL, 0, ( LPTHREAD_START_ROUTINE )dwThread, NULL, 0, NULL );
	}

	return true;

}
*/
//========================================================================================