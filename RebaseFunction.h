//========================================================================================
#pragma once
//========================================================================================
#include "NT.h"
#include "macros.h"
//========================================================================================
void __stdcall ReprocessRelocations ( DWORD dwFunctionBuffer, DWORD dwRelocateFromBase, DWORD dwRebaseAddress, DWORD dwSize );
//========================================================================================
//PIMAGE_BASE_RELOCATION NTAPI ProcessDataSection ( MD5Context_t& MD5Context, VTable_Entry_t* pVTable, DWORD& dwBufferSize, int iVTablePointersFound, DWORD Address, DWORD Count, PUSHORT TypeOffset, LONG Delta );
//========================================================================================
PIMAGE_BASE_RELOCATION NTAPI LdrProcessRelocationBlockLongLong	( DWORD Address, DWORD Count, PUSHORT TypeOffset, LONG Delta );
//========================================================================================
PIMAGE_BASE_RELOCATION NTAPI ScanVTables ( MD5Context_t& MD5Context, DWORD dwBaseRData, DWORD dwEndRData, DWORD Address, DWORD Count, PUSHORT TypeOffset, LONG Delta );
//========================================================================================