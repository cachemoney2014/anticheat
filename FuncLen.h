//========================================================================================
#pragma once
//========================================================================================
#include "NT.h"
#include "macros.h"
//========================================================================================
#define INSTR_NEAR_PREFIX	0x0F
#define INSTR_CALL			0xE8
#define INSTR_SHORTJMP		0xEB
#define INSTR_RELJMP		0xE9
#define INSTR_RELJCX		0xE3
//========================================================================================
#define INSTR_NEARJCC_BEGIN 0x80
#define INSTR_NEARJCC_END	0x8F
//========================================================================================
#define INSTR_SHORTJCC_BEGIN	0x70
#define INSTR_SHORTJCC_END		0x7F
//========================================================================================
#define INSTR_CMP			0x83
//========================================================================================
#define INSTR_RET			0xC3
#define INSTR_RETN			0xC2
#define INSTR_RETFN			0xCA
#define INSTR_RETF			0xCB
//========================================================================================
#define INSTR_SHORT_JA		0x77
#define INSTR_NEAR_JA		0x87
//========================================================================================
#define INSTR_SHORT_JG		0x7F
#define INSTR_NEAR_JG		0x8F
//========================================================================================
#define INSTR_FAR_PREFIX	0xFF
//========================================================================================
#define ROUND_UP(p, align)   (((DWORD)(p) + (align)-1) & ~((align)-1))
#define ROUND_DOWN(p, align) ((DWORD)(p) & ~((align)-1))
//========================================================================================
DWORD __stdcall GetFunctionLength ( PVOID pvFunctionBegin, BOOL bRebaseRelocs, BOOL bFixupRelative, PDWORD pdwFunctionStartOut, DWORD dwRelocateFromBase, DWORD dwRebaseAddress, DWORD dwBufferIn );
//========================================================================================
PVOID __stdcall GetFunctionEnd ( PVOID pvFunc );
PVOID __stdcall GetBranchListFromBlock ( PVOID block );
PVOID __stdcall GetBranchAddress ( PBYTE instr );
BOOL __stdcall IsEndPoint ( PBYTE instr, PVOID curblock );
DWORD __stdcall dwGetLongConditionalJumpAddress ( PDWORD pdwAddress );
DWORD __stdcall dwGetCallAddress ( PDWORD pdwAddress );
BOOL __stdcall bIsUnConditionalJump ( PBYTE pbCurrentInstruction );
//========================================================================================
int __stdcall compare ( const void* a, const void* b );
//========================================================================================
typedef struct
{
	DWORD m_dwBlockStart;
	DWORD m_dwBlockEnd;
}Block_t;
//========================================================================================
typedef struct
{
	DWORD m_dwBlockStart;
	DWORD m_dwBlockEnd;

	DWORD m_dwJumpTable;
	DWORD m_dwPadding;
}JumpTable_t;
//========================================================================================
extern PDWORD g_pdwBranches;
extern PDWORD g_pdwInstructions;
extern PDWORD g_pdwFixedFunction;
extern PDWORD g_pdwCopiedInstructions;
//========================================================================================
extern Block_t* g_pBlock;
extern JumpTable_t* g_pJumpTable;
//========================================================================================
extern int g_iBranchesFound;
extern int g_iTotalInstructions;
//========================================================================================
int __stdcall GetFunctionLengthVAC3Method ( DWORD dwFunctionStart, DWORD dwSize );
//========================================================================================