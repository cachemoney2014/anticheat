//========================================================================================
#pragma once
//========================================================================================
typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned long DWORD;
typedef __int64 QWORD;
//========================================================================================
typedef BYTE* PBYTE;
typedef WORD* PWORD;
typedef DWORD* PDWORD;
//========================================================================================
typedef char CHAR;
typedef short SHORT;
typedef long LONG;
typedef void VOID;
//========================================================================================
typedef CHAR* PCHAR;
typedef SHORT* PSHORT;
typedef LONG* PLONG;
typedef void* PVOID;
//========================================================================================
typedef unsigned char UCHAR;
typedef unsigned short USHORT;
typedef unsigned long ULONG;
//========================================================================================
typedef UCHAR* PUCHAR;
typedef USHORT* PUSHORT;
typedef ULONG* PULONG;
//========================================================================================
typedef UCHAR* UCHAR_PTR;
typedef USHORT* USHORT_PTR;
typedef ULONG ULONG_PTR;
//========================================================================================
typedef wchar_t WCHAR;
typedef WCHAR* PWSTR;
//========================================================================================
typedef int BOOL;
typedef int INT;
typedef float FLOAT;
typedef double DOUBLE;
typedef BYTE BOOLEAN;
//========================================================================================
typedef BOOL* PBOOL;
typedef INT* PINT;
typedef FLOAT* PFLOAT;
typedef DOUBLE* PDOUBLE;
//========================================================================================
typedef signed char SBYTE;
typedef SBYTE *PSBYTE;
//========================================================================================
typedef signed short SWORD;
typedef SWORD *PSWORD;
//========================================================================================
typedef signed long SDWORD;
typedef SDWORD *PSDWORD;
//========================================================================================
typedef unsigned int UINT;
typedef UINT* PUINT;
//========================================================================================
typedef void* LPVOID;
typedef const void* LPCVOID;
//========================================================================================
typedef BOOLEAN* PBOOLEAN;
//========================================================================================
typedef unsigned int uint32;
//========================================================================================
typedef const CHAR* LPCSTR;
typedef LPCSTR LPCTSTR;
//========================================================================================
typedef DWORD* LPDWORD;
//========================================================================================
typedef CHAR* LPSTR;
//========================================================================================
typedef LPSTR LPTSTR;
//========================================================================================
typedef void* HANDLE;
//========================================================================================
typedef HANDLE* PHANDLE;
//========================================================================================
typedef INT SIZE_T;
typedef SIZE_T* PSIZE_T;
//========================================================================================
typedef LONG KPRIORITY;
//========================================================================================
typedef wchar_t* LPWSTR, *PWSTR; 
//========================================================================================
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned long uint32_t;
typedef unsigned __int64 uint64_t;
//========================================================================================
typedef unsigned __int64* PUINT64;
typedef unsigned __int64* PULONGLONG;
//========================================================================================
#define FALSE 0
#define TRUE (!FALSE)
//========================================================================================
#define NULL 0
//========================================================================================
#define DLL_PROCESS_DETACH 0
#define DLL_PROCESS_ATTACH 1
#define DLL_THREAD_ATTACH 2
#define DLL_THREAD_DETACH 3
//========================================================================================
#define WINAPI __stdcall
#define PASCAL __stdcall
#define RESTRICTED_POINTER __restrict
#define NTAPI __stdcall
//========================================================================================
#define far
#define near
//========================================================================================
#define FAR                 far
#define NEAR                near
//========================================================================================
typedef __int64 LONGLONG;
typedef unsigned __int64 ULONGLONG;
//========================================================================================
typedef ULONG_PTR DWORD_PTR; 
//========================================================================================
typedef LONG NTSTATUS;
//========================================================================================
#define INFINITE			0xFFFFFFFF
//========================================================================================
#define MAXIMUM_SUPPORTED_EXTENSION     512
#define SIZE_OF_80387_REGISTERS      80
//========================================================================================
#define DLL_EXPORT extern "C" __declspec ( dllexport )
#define DLL_IMPORT extern "C" __declspec ( dllimport )
//========================================================================================
#define ALLOC_CALL _CRTNOALIAS _CRTRESTRICT 
//========================================================================================
typedef int ( FAR WINAPI* FARPROC )();
typedef int ( NEAR WINAPI* NEARPROC )();
typedef int ( WINAPI* PROC )();
//========================================================================================
typedef enum _POOL_TYPE
{
    NonPagedPool,
    PagedPool,
    NonPagedPoolMustSucceed,
    DontUseThisType,
    NonPagedPoolCacheAligned,
    PagedPoolCacheAligned,
    NonPagedPoolCacheAlignedMustS
} POOL_TYPE, *PPOOL_TYPE;
//========================================================================================
typedef struct _FILETIME {
    DWORD dwLowDateTime;
    DWORD dwHighDateTime;
} FILETIME, *PFILETIME, *LPFILETIME;
//========================================================================================
typedef struct _WIN32_FIND_DATAA {
    DWORD dwFileAttributes;
    FILETIME ftCreationTime;
    FILETIME ftLastAccessTime;
    FILETIME ftLastWriteTime;
    DWORD nFileSizeHigh;
    DWORD nFileSizeLow;
    DWORD dwReserved0;
    DWORD dwReserved1;
    CHAR   cFileName[ 260 ];
    CHAR   cAlternateFileName[ 14 ];
} WIN32_FIND_DATAA, *PWIN32_FIND_DATAA, *LPWIN32_FIND_DATAA;
//========================================================================================
typedef WIN32_FIND_DATAA WIN32_FIND_DATA;
//========================================================================================
#define APIENTRY    WINAPI
//========================================================================================
#define MAX_COMPUTERNAME_LENGTH 15
//========================================================================================
#ifndef INVALID_HANDLE_VALUE
#define INVALID_HANDLE_VALUE ((HANDLE)-1)
#endif
//========================================================================================
#define HEAP_NO_SERIALIZE               0x00000001      
#define HEAP_GROWABLE                   0x00000002      
#define HEAP_GENERATE_EXCEPTIONS        0x00000004      
#define HEAP_ZERO_MEMORY                0x00000008      
#define HEAP_REALLOC_IN_PLACE_ONLY      0x00000010      
#define HEAP_TAIL_CHECKING_ENABLED      0x00000020      
#define HEAP_FREE_CHECKING_ENABLED      0x00000040      
#define HEAP_DISABLE_COALESCE_ON_FREE   0x00000080      
#define HEAP_CREATE_ALIGN_16            0x00010000      
#define HEAP_CREATE_ENABLE_TRACING      0x00020000      
#define HEAP_CREATE_ENABLE_EXECUTE      0x00040000      
#define HEAP_MAXIMUM_TAG                0x0FFF              
#define HEAP_PSEUDO_TAG_FLAG            0x8000              
#define HEAP_TAG_SHIFT                  18
//========================================================================================
#define VER_EQUAL                       1
#define VER_GREATER                     2
#define VER_GREATER_EQUAL               3
#define VER_LESS                        4
#define VER_LESS_EQUAL                  5
#define VER_AND                         6
#define VER_OR                          7
//========================================================================================
#define VER_CONDITION_MASK              7
#define VER_NUM_BITS_PER_CONDITION_MASK 3
//========================================================================================
//
// RtlVerifyVersionInfo() type mask bits
//
//========================================================================================
#define VER_MINORVERSION                0x0000001
#define VER_MAJORVERSION                0x0000002
#define VER_BUILDNUMBER                 0x0000004
#define VER_PLATFORMID                  0x0000008
#define VER_SERVICEPACKMINOR            0x0000010
#define VER_SERVICEPACKMAJOR            0x0000020
#define VER_SUITENAME                   0x0000040
#define VER_PRODUCT_TYPE                0x0000080
//========================================================================================
//
// RtlVerifyVersionInfo() os product type values
//
//========================================================================================
#define VER_NT_WORKSTATION              0x0000001
#define VER_NT_DOMAIN_CONTROLLER        0x0000002
#define VER_NT_SERVER                   0x0000003
//========================================================================================
//
// dwPlatformId defines:
//
//========================================================================================
#define VER_PLATFORM_WIN32s             0
#define VER_PLATFORM_WIN32_WINDOWS      1
#define VER_PLATFORM_WIN32_NT           2
//========================================================================================
#define ERROR_INVALID_PARAMETER 87
#define ERROR_NO_MORE_ITEMS 259L
//========================================================================================
#define NT_SUCCESS(Status) (((NTSTATUS)(Status)) >= 0 )
#define NT_INFORMATION(Status) ((((ULONG)(Status) >> 30) == 1 )
#define NT_WARNING(Status) ((((ULONG)(Status)) >> 30) == 2 )
#define NT_ERROR(Status) ((((ULONG)(Status)) >> 30) == 3 )
//========================================================================================
#define IMAGE_SIZEOF_SHORT_NAME              8
//========================================================================================
#define CONTEXT_i386    0x00010000    // this assumes that i386 and
#define CONTEXT_i486    0x00010000    // i486 have identical context records
//========================================================================================
#define CONTEXT_CONTROL         (CONTEXT_i386 | 0x00000001L) // SS:SP, CS:IP, FLAGS, BP
#define CONTEXT_INTEGER         (CONTEXT_i386 | 0x00000002L) // AX, BX, CX, DX, SI, DI
#define CONTEXT_SEGMENTS        (CONTEXT_i386 | 0x00000004L) // DS, ES, FS, GS
#define CONTEXT_FLOATING_POINT  (CONTEXT_i386 | 0x00000008L) // 387 state
#define CONTEXT_DEBUG_REGISTERS (CONTEXT_i386 | 0x00000010L) // DB 0-3,6,7
#define CONTEXT_EXTENDED_REGISTERS  (CONTEXT_i386 | 0x00000020L) // cpu specific extensions

#define CONTEXT_FULL (CONTEXT_CONTROL | CONTEXT_INTEGER |\
                      CONTEXT_SEGMENTS)

#define CONTEXT_ALL             (CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_SEGMENTS | \
                                 CONTEXT_FLOATING_POINT | CONTEXT_DEBUG_REGISTERS | \
                                 CONTEXT_EXTENDED_REGISTERS)

#define CONTEXT_XSTATE          (CONTEXT_i386 | 0x00000040L)
//========================================================================================
typedef struct _IMAGE_SECTION_HEADER {
    BYTE    Name[IMAGE_SIZEOF_SHORT_NAME];
    union {
            DWORD   PhysicalAddress;
            DWORD   VirtualSize;
    } Misc;
    DWORD   VirtualAddress;
    DWORD   SizeOfRawData;
    DWORD   PointerToRawData;
    DWORD   PointerToRelocations;
    DWORD   PointerToLinenumbers;
    WORD    NumberOfRelocations;
    WORD    NumberOfLinenumbers;
    DWORD   Characteristics;
} IMAGE_SECTION_HEADER, *PIMAGE_SECTION_HEADER;


#define STILL_ACTIVE 259
//========================================================================================
#define FIELD_OFFSET(type, field)    ((LONG)(long)&(((type *)0)->field))
//========================================================================================
#define IMAGE_FIRST_SECTION( ntheader ) ((PIMAGE_SECTION_HEADER)        \
    ((unsigned long)ntheader +                                              \
     FIELD_OFFSET( IMAGE_NT_HEADERS, OptionalHeader ) +                 \
     ((PIMAGE_NT_HEADERS)(ntheader))->FileHeader.SizeOfOptionalHeader   \
    ))
//========================================================================================