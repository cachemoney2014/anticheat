//========================================================================================
#include "Memory.h"
//========================================================================================
#include "Strings.h"
//========================================================================================
char EasterEgg[] = "\x1E\x16\x79\x17\x70\x2\x63\x17\x62\xE\x6F\x1B\x72\x1D\x73\x0\x21\x1\x21\x78\x17"
"\x62\x45\x33\x56\x76\x10\x7F\xA\x64\x0\x20\x54\x3C\x59\x79\xA\x63\xE\x7E\x12\x77"
"\x57\x33\x56\x34\x41\x26\x41\x24\x56\x76\x15\x7D\x18\x7B\x10\x3E\x1E\x3E\x76\x19"
"\x6E\x4E\x2A\x45\x65\x1C\x73\x6\x26\x40\x25\x40\x2C\x13";
//========================================================================================
__forceinline unsigned int __fastcall strlen ( const char* _Str )
{
	const char *s;
    for ( s = _Str; *s; ++s );
    return ( s  - _Str );
}
//========================================================================================
__forceinline unsigned int __fastcall wstrlen ( const wchar_t* _Str )
{
	const wchar_t *s;
    for ( s = _Str; *s; ++s );
	return ( ( s - _Str ) * sizeof ( wchar_t ) );
}
//========================================================================================
void __stdcall DecryptString ( PCHAR pszStringToDecrypt, PCHAR pszStringOut, int iSize ) // reversed from Valve Anti Cheat, use it to decrypt the easter egg for fun
{
	int iIterator;

	BYTE bKey, bLength, bOldKey;

	bOldKey = 0x55;
	iIterator = 0;

	memset ( pszStringOut, 0, ( size_t )iSize );

	bLength = ( BYTE )( pszStringToDecrypt[0] ^ bOldKey ) - 1;
 
    do
	{
        bKey = ( BYTE )pszStringToDecrypt [ iIterator + 1 ];   
        
        pszStringOut [ iIterator ] = bOldKey ^ pszStringToDecrypt [ iIterator + 1 ];
        
        bOldKey = bKey;

		iIterator++;

    }while ( bLength-- );
}
//========================================================================================