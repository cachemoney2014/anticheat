//========================================================================================
#include "Memory.h"
//========================================================================================
#include "IAT.h"
//========================================================================================
HANDLE g_hProcessHeap = 0x0;
//========================================================================================
#define ROUND_UP(p, align)   (((DWORD)(p) + (align)-1) & ~((align)-1))
#define ROUND_DOWN(p, align) ((DWORD)(p) & ~((align)-1))
//========================================================================================
extern "C" 
{
	int _fltused = 1;
}
//========================================================================================
__declspec(naked) long __cdecl _ftol2_sse(double x)
{
        __asm
        {
                fstp        qword ptr [esp-8]
                cvttsd2si   eax, mmword ptr [esp-8]
                ret
        }
}
//========================================================================================
#pragma function ( memcpy, memset )
//========================================================================================
void* __cdecl memcpy ( void* _Dest, const void* _Source, size_t _Size )
{
	unsigned int uiBufferSize, uiRemainderSource, uiRemainderDest;

	PBYTE pbDest = ( PBYTE )_Dest;
	PBYTE pbSource = ( PBYTE )_Source;

	PDWORD pdwDest = ( PDWORD )_Dest;
	PDWORD pdwSource = ( PDWORD )_Source;

	uiRemainderSource = ( sizeof ( DWORD ) - ( ( DWORD )_Source & ( sizeof ( DWORD ) -1 ) ) );

	uiRemainderDest = ( sizeof ( DWORD ) - ( ( DWORD )_Dest & ( sizeof ( DWORD ) -1 ) ) );

	if ( uiRemainderSource == uiRemainderDest
		&& uiRemainderSource != 0 && _Size >= uiRemainderSource  )
	{		
		_Size -= uiRemainderSource;

		while ( uiRemainderSource-- )
		{
			*pbDest++ = *pbSource++;
		}

		pdwDest = ( PDWORD )pbDest;
		pdwSource = ( PDWORD )pbSource;
	}

	// see how many dwords can fit in the space
		
	uiBufferSize = _Size >> 2;

	if ( uiBufferSize != 0 )
	{
		_Size -= ( uiBufferSize << 2 );

		while ( uiBufferSize-- )
		{
			*pdwDest++ = *pdwSource++;
		}
	}
	if ( _Size != 0 ) // we still have space left over
	{		
		pbDest = ( PBYTE )pdwDest;
		pbSource = ( PBYTE )pdwSource;

		while ( _Size-- )
		{
			*pbDest++ = *pbSource++;
		}
	}

	return _Dest;
}
//========================================================================================
void* __cdecl memset ( void* _Dest, int _Val, size_t _Size )
{
	unsigned int uiBufferSize, uiRemainder;
	
	PDWORD pdwDest = ( PDWORD )_Dest;

	PBYTE pbDest = ( PBYTE )_Dest;
	
	uiRemainder = ( sizeof ( DWORD ) - ( ( DWORD )_Dest & ( sizeof ( DWORD ) -1 ) ) );
		
	if ( uiRemainder != 0 && _Size >= uiRemainder ) // unaligned memory
	{		
		_Size -= uiRemainder;
			
		while ( uiRemainder-- ) // get us on aligned memory
		{
			*pbDest++ = _Val;
		}

		pdwDest = ( PDWORD )pbDest;
	}
	
	// see how many dwords can fit in the space
	
	uiBufferSize = _Size >> 2;

	if ( uiBufferSize != 0 )
	{
		_Size -= ( uiBufferSize << 2 );

		while ( uiBufferSize-- )
		{
			*pdwDest++ = _Val;
		}
	}
	
	if ( _Size != 0 ) // we still have space left over
	{
		pbDest = ( PBYTE )pdwDest;
				
		while ( _Size-- )
		{
			*pbDest++ = _Val;
		}
	}
		
	return _Dest;
}
//========================================================================================
__forceinline HANDLE GetProcessHeap ( void )
{
	return g_hProcessHeap;
}
//========================================================================================
PVOID __stdcall AllocateFromHeap ( DWORD dwSize )
{	
	PVOID pvMem = NULL;
		
	pvMem = RtlAllocateHeap ( GetProcessHeap(), 0, dwSize );
				
	return pvMem;
}
//========================================================================================
void __stdcall DeallocateOffHeap ( PVOID pvMemory )
{
	RtlFreeHeap ( GetProcessHeap(), 0, pvMemory );
}
//========================================================================================
PVOID __stdcall NtAllocateMemory ( HANDLE hProcess, DWORD dwBase, DWORD dwSize, DWORD dwProtection, NTSTATUS& status )
{
	int iIndex;

	DWORD dwAllocatedBase;
	
	PVOID pvAllocationBase;

	dwAllocatedBase = dwBase;
			
	pvAllocationBase = ( PVOID )dwAllocatedBase;

	status = NtAllocateVirtualMemory ( hProcess, &pvAllocationBase, 0, ( PSIZE_T )&dwSize, MEM_COMMIT | MEM_RESERVE, dwProtection );

	if ( NT_SUCCESS ( status ) )
	{
//		iIndex = 

		return pvAllocationBase;
	}
	
	return NULL;
}
//========================================================================================
BOOL __stdcall NtFreeMemory ( HANDLE hProcess, DWORD dwBase, DWORD dwSize, DWORD dwFreeType, NTSTATUS& status )
{
	BOOL bReturn;

	DWORD dwAllocatedBase;
	
	PVOID pvAllocationBase;

	if ( dwFreeType == MEM_RELEASE )
	{
		dwSize = 0;
	}

	bReturn = 0;
	dwAllocatedBase = dwBase;
			
	pvAllocationBase = ( PVOID )dwAllocatedBase;

	status = NtFreeVirtualMemory ( hProcess, &pvAllocationBase, ( PSIZE_T )&dwSize, dwFreeType );
		
	if ( NT_SUCCESS ( status ) )
	{
		bReturn = 1;
	}

	return bReturn;
}
//========================================================================================