//========================================================================================
#include "NT.h"
//========================================================================================
#include <intrin.h>
//========================================================================================
PROCESS_ENVIRONMENT_BLOCK* GetPEB ( void )
{
	return ( PROCESS_ENVIRONMENT_BLOCK* )__readfsdword ( 0x30 );
}
//========================================================================================
HANDLE GetHeap ( void )
{
	HANDLE hHeap;

	// REPLACE WITH COMPILER INTRINSICS
	
	// Fuck this assembly code is badly done, but I guess since it's only called
	// once we don't have to care we stall the fuck out of the pipeline
	// add nops or some shit to do some interleaving

	__asm
	{
		MOV EAX, DWORD PTR FS:[ 0x18 ]
		NOP
		NOP
		MOV EAX, DWORD PTR DS:[ EAX+0x30 ]
		NOP
		NOP
		MOV EAX, DWORD PTR DS:[ EAX+0x18 ]
		NOP
		NOP
		MOV hHeap, EAX
		
	}

	return hHeap;
}
//========================================================================================
