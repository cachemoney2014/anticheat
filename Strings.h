//========================================================================================
#pragma once
//========================================================================================
#include "typedefs.h"
//========================================================================================
extern __forceinline unsigned int __fastcall strlen ( const char* _Str );
extern __forceinline unsigned int __fastcall wstrlen ( const wchar_t* _Str );
//========================================================================================
void __stdcall DecryptString ( PCHAR pszStringToDecrypt, PCHAR pszStringOut, int iSize );
//========================================================================================
