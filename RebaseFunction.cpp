//========================================================================================
#include "IAT.h"
#include "RebaseFunction.h"
//========================================================================================
extern int g_iMemoryValues, g_iFileValues;
//========================================================================================
void __stdcall ReprocessRelocations ( DWORD dwFunctionBuffer, DWORD dwRelocateFromBase, DWORD dwRebaseAddress, DWORD dwSize )
{

}
//========================================================================================
PIMAGE_BASE_RELOCATION NTAPI LdrProcessRelocationBlockLongLong	( DWORD Address, DWORD Count, PUSHORT TypeOffset, LONG Delta )		
{
    SHORT Offset;
    USHORT Type;
    DWORD i;
    PUSHORT ShortPtr;
    PULONG LongPtr;
    PULONGLONG LongLongPtr;

	for ( i = 0; i < Count; i++ )
	{
		Offset = *TypeOffset & 0xFFF;
		Type = *TypeOffset >> 12;
		ShortPtr = ( PUSHORT )( Address + Offset );
        /*
        * Don't relocate within the relocation section itself.
        * GCC/LD generates sometimes relocation records for the relocation section.
        * This is a bug in GCC/LD.
        * Fix for it disabled, since it was only in ntoskrnl and not in ntdll
        */
        /*
        if ((ULONG_PTR)ShortPtr < (ULONG_PTR)RelocationDir ||
        (ULONG_PTR)ShortPtr >= (ULONG_PTR)RelocationEnd)
        {*/
		switch ( Type )
		{
            /* case IMAGE_REL_BASED_SECTION : */
            /* case IMAGE_REL_BASED_REL32 : */
			case IMAGE_REL_BASED_ABSOLUTE:
			{
				
			}
			break;

			case IMAGE_REL_BASED_HIGH:
			{
				*ShortPtr = HIWORD ( MAKELONG ( 0, *ShortPtr ) + ( Delta & 0xFFFFFFFF ) );
			}
			break;

			case IMAGE_REL_BASED_LOW:
			{
				*ShortPtr = *ShortPtr + LOWORD ( Delta & 0xFFFF );
			}
			break;

			case IMAGE_REL_BASED_HIGHLOW:
			{
				LongPtr = ( PULONG )( Address + Offset );
				*LongPtr = *LongPtr + ( Delta & 0xFFFFFFFF );
			}
			break;

			case IMAGE_REL_BASED_DIR64:
			{
				LongLongPtr = ( PUINT64 )( Address + Offset );
				*LongLongPtr = *LongLongPtr + Delta;
			}
			break;

			case IMAGE_REL_BASED_HIGHADJ:
			case IMAGE_REL_BASED_MIPS_JMPADDR:
			default:
			{
				return ( PIMAGE_BASE_RELOCATION )NULL;
			}
			break;
		}

        TypeOffset++;
    }

    return (PIMAGE_BASE_RELOCATION)TypeOffset;
}
/*
//========================================================================================
PIMAGE_BASE_RELOCATION NTAPI ProcessDataSection ( MD5Context_t& MD5Context, VTable_Entry_t* pVTable, DWORD& dwBufferSize, int iVTablePointersFound, DWORD Address, DWORD Count, PUSHORT TypeOffset, LONG Delta  )		
{
	DWORD dwAddress, dwVTablePointer;

    SHORT Offset;
    USHORT Type;
    DWORD i;
    PUSHORT ShortPtr;
    PULONG LongPtr;
    PULONGLONG LongLongPtr;

	NTSTATUS status;

	int iIndexIntoVTable;

	iIndexIntoVTable = 0;

	for ( i = 0; i < Count; i++ )
	{
		Offset = *TypeOffset & 0xFFF;
		Type = *TypeOffset >> 12;
		
		switch ( Type )
		{
			case IMAGE_REL_BASED_HIGHLOW:
			{
				dwAddress = ( DWORD )( Address + Offset );

				if ( pVTable [ iIndexIntoVTable ].m_dwVirtualAddress == dwAddress )
				{
					pVTable [ iIndexIntoVTable ].m_dwPointer += ( Delta & 0xFFFFFFFF );

					dwVTablePointer = 0;

					dwVTablePointer = pVTable [ iIndexIntoVTable ].m_dwPointer;

					MD5Update ( &MD5Context, ( PBYTE )&dwVTablePointer, sizeof ( DWORD ) );

					iIndexIntoVTable++;

					dwBufferSize += sizeof ( VTable_Entry_t );
				}
			}
			break;
		}

        TypeOffset++;
    }

    return (PIMAGE_BASE_RELOCATION)TypeOffset;
}
*/
//========================================================================================
PIMAGE_BASE_RELOCATION NTAPI ScanVTables ( MD5Context_t& MD5Context, DWORD dwBaseRData, DWORD dwEndRData, DWORD Address, DWORD Count, PUSHORT TypeOffset, LONG Delta )		
{
    SHORT Offset;
    USHORT Type;
    DWORD i;
    PUSHORT ShortPtr;
    PULONG LongPtr;
    PULONGLONG LongLongPtr;

	PDWORD pdwAddress;

	NTSTATUS status;

	DWORD dwAddress, dwBytesRead, dwFunction;

	for ( i = 0; i < Count; i++ )
	{
		Offset = *TypeOffset & 0xFFF;
		Type = *TypeOffset >> 12;
		
		switch ( Type )
		{
			case IMAGE_REL_BASED_HIGHLOW:
			{
				pdwAddress = ( PDWORD )( Address + Offset );

				dwFunction = *pdwAddress;

				if ( ( DWORD )pdwAddress >= dwBaseRData && ( DWORD )pdwAddress <= dwEndRData )
				{
					dwFunction = dwFunction + ( Delta & 0xFFFFFFFF );

					MD5Update ( &MD5Context, ( PBYTE )&dwFunction, sizeof ( DWORD ) );
				}
			}
			break;
		}
        /*
        * Don't relocate within the relocation section itself.
        * GCC/LD generates sometimes relocation records for the relocation section.
        * This is a bug in GCC/LD.
        * Fix for it disabled, since it was only in ntoskrnl and not in ntdll
        */
        /*
        if ((ULONG_PTR)ShortPtr < (ULONG_PTR)RelocationDir ||
        (ULONG_PTR)ShortPtr >= (ULONG_PTR)RelocationEnd)
        {*/
/*		switch ( Type )
		{
            /* case IMAGE_REL_BASED_SECTION : */
            /* case IMAGE_REL_BASED_REL32 : */
/*			case IMAGE_REL_BASED_ABSOLUTE:
			{
				
			}
			break;

			case IMAGE_REL_BASED_HIGH:
			{
				*ShortPtr = HIWORD ( MAKELONG ( 0, *ShortPtr ) + ( Delta & 0xFFFFFFFF ) );
			}
			break;

			case IMAGE_REL_BASED_LOW:
			{
				*ShortPtr = *ShortPtr + LOWORD ( Delta & 0xFFFF );
			}
			break;

			case IMAGE_REL_BASED_HIGHLOW:
			{
				LongPtr = ( PULONG )( Address + Offset );
				*LongPtr = *LongPtr + ( Delta & 0xFFFFFFFF );
			}
			break;

			case IMAGE_REL_BASED_DIR64:
			{
				LongLongPtr = ( PUINT64 )( Address + Offset );
				*LongLongPtr = *LongLongPtr + Delta;
			}
			break;

			case IMAGE_REL_BASED_HIGHADJ:
			case IMAGE_REL_BASED_MIPS_JMPADDR:
			default:
			{
				return ( PIMAGE_BASE_RELOCATION )NULL;
			}
			break;
		}
		*/
        TypeOffset++;
    }

    return (PIMAGE_BASE_RELOCATION)TypeOffset;
}
//========================================================================================